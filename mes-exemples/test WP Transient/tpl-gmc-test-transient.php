<?php
/**
 * Template Name: Test TRANSIENT
 * The template for test WP Transient
 *
 * @package WordPress
 */
$user = wp_get_current_user();

/**
 * Flush des transients expirés (l'idéal serait de supprimer les transients via un hook)
 */
if ($_REQUEST['test_delete'] && false === get_transient( 'mon_transient')) {
	delete_transient( 'mon_transient' );
	die("transient deleted");
}



/**
 * Création du nonce au début du fichier
 */
$bare_url = 'http://localhost/labo/test-wordpress-modules/test-transient';

/**
 * Comment créer un transient ?
 *
 */
if (false === get_transient( 'mon_transient')) {

	$ma_valeur_a_stocker = array("field1"=>"TOTO","field2"=>"TITI","field3"=>"TATA");
	set_transient( 'mon_transient', $ma_valeur_a_stocker, '10' );
}
else {
	echo "je ne recalcule pas mes données ";
		$transient_timeout = get_option ( '_transient_timeout_' . 'mon_transient' );
	var_dump($transient_timeout);

}


foreach ($ma_valeur_a_stocker as $key => $value) {
	echo '<p>' . $key . ' => ' . $value . '</p>';
}

?>
<h1>Test des WP Transient</h1>



<p>
	<a href="<?php echo $complete_url; ?>">Vérification du transient dans une page</a>
</p>

<p>
	<a href="<?php echo $complete_url . '?test_delete=1'; ?>">Flush du transient</a>
</p>

