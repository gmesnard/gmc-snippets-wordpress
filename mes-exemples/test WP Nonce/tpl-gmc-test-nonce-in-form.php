<?php
/**
 * Template Name: Test NONCE FORM
 * The template for test WP Nonce via form
 *
 * @package WordPress
 */
$user = wp_get_current_user();

/**
 * Création du nonce au début du fichier
 */
//$create_nonce = wp_create_nonce('gmc-nonce');

/**
 * Comment vérifier un nonce ?
 *
 * Si on utilise un champ de formulaire via 
 * 	>	wp_nonce_field($action, $nonce),
 * alors la vérification se fait via 
 * 	>	check_admin_referer($action, $nonce)
 */
if (isset($_POST) && $_POST) {
	$check_admin_referer = check_admin_referer('remove-comment_'.$user->ID, 'gmc-nonce');
	var_dump("Verification du nonce dans un formulaire");
	var_dump($check_admin_referer);

  var_dump($_POST['nom']);
}
?>
<h1>Test des WP Nonce dans une formulaire</h1>

<form action="" method="POST">
<fieldset>
	<p>
	<label for="id1">Nom</label>
	<input type="text" name="nom" id="id1">

	<input type="hidden" name="submit" value="1">
	<?php wp_nonce_field( 'remove-comment_'.$user->ID, 'gmc-nonce' ); ?>
	<input type="submit">
</fieldset>
</form>

<p>
	<a href="http://localhost/labo/test-wordpress-modules/test-nonce-url/">Tester le Nonce avec une url</a>
</p>
