<?php
/**
 * Template Name: Test NONCE URL
 * The template for test WP Nonce via url
 *
 * @package WordPress
 */
$user = wp_get_current_user();

/**
 * Création du nonce au début du fichier
 */
$bare_url = 'http://localhost/labo/test-wordpress-modules/test-nonce-url?delete-user_'.$user->ID;
$complete_url = wp_nonce_url( $bare_url, 'nom_de_l_action', 'nom_du_nonce' );

/**
 * Comment vérifier un nonce ?
 *
 * Si on utilise un nonce généré dans l'url
 * 	>	wp_nonce_url($url, $nom_de_l_action, $nom_du_nonce),
 * alors la vérification se fait via 
 * 	>	wp_verify_nonce($nom_du_nonce, $nom_de_l_action)
 */
if (isset($_REQUEST) && $_REQUEST) {
	$verify_nonce = wp_verify_nonce( $_REQUEST['nom_du_nonce'], 'nom_de_l_action' );
	if ( !$verify_nonce ) {
		die('Pb avec le nonce, rediriger la page...');
	}

	var_dump("Verification du nonce dans une page");
	var_dump($verify_nonce);
}

?>
<h1>Test des WP Nonce dans une page</h1>

<p>
	<a href="<?php echo $complete_url; ?>">Vérification du nonce dans une page</a>
</p>


<p>
	<a href="http://localhost/labo/test-wordpress-modules/test-nonce-form/">Tester le Nonce avec un formulaire</a>
</p>