/**
 * Changer l'auteur d'un média WP (impossible dans l'admin)
 */
UPDATE NOM_TABLE_posts SET post_author ='ID_DE_l'AUTEUR' WHERE post_type LIKE 'attachment'