<?php
function noo_menu_get_search_form(){
	ob_start();
?>
<div class="noo-menu-search noo-menu-search-show-<?php echo noo_menu_get_option('search_show','hover')?>">
	<form method="get" action="<?php echo esc_url( home_url( '/' ) ) ?>">
		<input type="submit" id="searchsubmit" value="" />
		<input type="text"  placeholder="<?php echo esc_attr__( 'Search', NOO_MENU ) ?>"  class="field" name="s" id="s" tabindex="1" />
		<i class="fa fa-search"></i>
	</form>
</div>
<?php /* if(class_exists('WooCommerce')):?>
<?php global $woocommerce; ?>
<div class="noo-menu-cart">
	<a href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="fa fa-shopping-cart"> 2</i></a>
</div>
<?php endif; */ ?>
<?php
	return apply_filters('noo_menu_search_form', ob_get_clean());
}
function noo_hex2rgba($hex='',$opacity=0){
	if(empty($hex))
		return $hex;
	
	$hex = str_replace("#", "", $hex);
	if(strlen($hex) == 3) {
		$r = hexdec(substr($hex,0,1).substr($hex,0,1));
		$g = hexdec(substr($hex,1,1).substr($hex,1,1));
		$b = hexdec(substr($hex,2,1).substr($hex,2,1));
	} else {
		$r = hexdec(substr($hex,0,2));
		$g = hexdec(substr($hex,2,2));
		$b = hexdec(substr($hex,4,2));
	}
	$rgb = array($r, $g, $b);
	
	if($opacity){
		if(abs($opacity) > 1)
			$opacity = 1.0;
		return 'rgba('.implode(",",$rgb).','.$opacity.')';
	}else{
		return 'rgb('.implode(",",$rgb).')';
	}
}

function noo_menu_google_font($key=''){
	static $font_string = null;
	if(empty($font_string))
		$font_string = @file_get_contents(NOO_MENU_DIR.'assets/fonts/google-web-fonts.txt');
	$fonts = json_decode($font_string);
	$items = $fonts->items;
	$selected = noo_menu_get_option($key.'_font','Open Sans');
	$i = 0;
?>
<label for="<?php echo $key.'_font' ?>"><?php echo __('Font',NOO_MENU) ?> </label>
<select class="google-font form-control" onchange="updatemenu()" id="<?php echo $key.'_font'?>" name="<?php echo noo_menu_get_option_name($key.'_font')?>">
<option  <?php selected($selected,$value)?> value="<?php echo 'inherit'?>"><?php echo __('Inherit',NOO_MENU) ?></option>
<?php
	foreach ($items as $item){
	if($i == 30)
		break;
	$value = esc_attr($item->family);
?>
	<option data-url="<?php echo esc_attr('family='.urlencode($item->family.':'.implode(',', $item->variants)).'&subset='.urlencode(implode(',',$item->subsets))) ?>" <?php selected($selected,$value)?> value="<?php echo $value?>"><?php echo esc_attr($item->family) ?></option>
	
<?php
	$i++;
	}
?>
</select>
<input type="hidden" value="<?php echo noo_menu_get_option($key.'_font_url')?>" id="<?php echo noo_menu_get_option_name($key.'_font_url')?>"  name="<?php echo noo_menu_get_option_name($key.'_font_url')?>" />
<?php
}

/**
 * Retrieve noo menu option value.
 * 
 * @param string $option
 * @param mixed $default
 * @param null|string $menu_id
 * @return mixed
 */
function noo_menu_get_option($option,$default=null,$menu_id = ''){
	global $noo_menu_id,$noo_menu_style_id;
	$option_id = $noo_menu_id;
	if(!empty($menu_id))
		$option_id = $menu_id;
	$options = get_option('noo_menu'.$option_id);
	
	$default = apply_filters('noo_menu_get_option_default', $default, $option, $noo_menu_id, $noo_menu_style_id);
	
	if($option != 'menu_config' && $option != 'dropdown' && $option != 'orientation' && $option != 'menu_logo'){
		if($noo_menu_style_id){
			$saved_style = get_option('noo_menu_style');
			if($saved_style && isset($saved_style[$noo_menu_style_id])){
				$style = $saved_style[$noo_menu_style_id]['style'];
				if(isset($style[$option])){
					return $style[$option];
				}
				
				return $default;
			}
		}
	}
	
	if (isset($options[$option])) {
		return $options[$option];
	}
	return $default;
}

function noo_menu_get_option_name($id){
	global $noo_menu_id;
	return 'noo_menu'.$noo_menu_id.'['.$id.']';
}


function _noo_sort_name_callback( $a, $b ) {
	return strnatcasecmp( $a['name'], $b['name'] );
}

function noo_menu_treerecurse($id, $list, &$children, $maxlevel = 9999, $level = 0){

	if (@$children[$id] && $level <= $maxlevel)
	{
		foreach ($children[$id] as $v)
		{
			$id = $v->ID;
			$list[$id] = $v;
			$children[$id] = isset($children[$id]) && !empty($children[$id]) ? $children[$id] : array();
			$list[$id]->children = $count = count(@$children[$id]);
			if ($count && !isset($list[$id]->frist_children)){
				$list[$id]->frist_children = $children[$id][0];
			}
			$list[$id]->level = $level;
			$list = noo_menu_treerecurse($id,$list, $children, $maxlevel, $level + 1);
		}
	}
	return $list;
}

function noo_menu_get_widget($base_id){
	global $wp_registered_widgets;
		
    $widget = null;

	if (isset($wp_registered_widgets[$base_id]) && ($data = $wp_registered_widgets[$base_id])) {
		$widget = new stdClass();
		foreach (array('id', 'name', 'classname', 'description') as $var) {
			$widget->$var = isset($data[$var]) ? $data[$var] : null;
		}
		if (isset($data['callback']) && is_array($data['callback']) && ($object = current($data['callback']))) {
			if (is_a($object, 'WP_Widget')) {
				$widget->class = get_class($object);
				$widget->type = $object->id_base;

				if (isset($data['params'][0]['number'])) {

					$number = $data['params'][0]['number'];
					$params = get_option($object->option_name);

					if (false === $params && isset($object->alt_option_name)) {
						$params = get_option($object->alt_option_name);
					}

					if (isset($params[$number])) {
						$widget->params = $params[$number];
					}
				}
			}
		}
		
		if (empty($widget->name)) {
			$widget->name = ucfirst($widget->type);
		}

		if (empty($widget->params)) {
			$widget->params = array();
		}
	}
	return $widget;
}

function noo_menu_paddings($key='',$top=true,$right=true,$bottom=true,$left=true,$dtop=0,$dright=0,$dbottom=0,$dleft=0,$boxname='Padding'){
	?>
	<?php if($boxname):?>
	<h3><?php echo __($boxname,NOO_MENU) ?></h3>
	<?php endif;?>
	<?php
	if($top)
		noo_menu_element_slider($key.'_padding_top','Padding Top','','',$dtop);
	if($right)
		noo_menu_element_slider($key.'_padding_right','Padding Right','','',$dright);
	if($bottom)
		noo_menu_element_slider($key.'_padding_bottom','Padding Bottom','','',$dbottom);
	if($left)
		noo_menu_element_slider($key.'_padding_left','Padding Left','','',$dleft);
	
}

function noo_menu_margins($key='',$top=true,$right=true,$bottom=true,$left=true,$dtop=0,$dright=0,$dbottom=0,$dleft=0){
	?>
	<h3><?php echo __('Margins',NOO_MENU) ?></h3>
	<?php 
	if($top)
		noo_menu_element_slider($key.'_margin_top','Margin Top','','',$dtop);
	if($right)
		noo_menu_element_slider($key.'_margin_right','Margin Right','','',$dright);
	if($bottom)
		noo_menu_element_slider($key.'_margin_bottom','Margin Bottom','','',$dbottom);
	if($left)
		noo_menu_element_slider($key.'_margin_left','Margin Left','','',$dleft);
}

function noo_menu_shadow($key='',$inset=true,$size=true){
	noo_menu_element_slider($key.'_h_shadow','Horizontal Length','-20','20');
	noo_menu_element_slider($key.'_v_shadow','Vertical Length','-20','20');
	noo_menu_element_slider($key.'_b_shadow','Blur Distance');
	if ($size)
		noo_menu_element_slider($key.'_s_shadow','Size of Shadow');
	noo_menu_element_color($key.'_shadow_color','Shadow Color','',true);
if($inset):
?>
<p>
	<input type="checkbox" <?php if(noo_menu_get_option($key.'_shadow_inset') == 'inset'):?> checked="checked"<?php endif;?> name="<?php echo noo_menu_get_option_name($key.'_shadow_inset') ?>" value="inset" onchange="updatemenu()" id="<?php echo $key.'_shadow_inset' ?>">
	<label for="<?php echo $key.'_shadow_inset' ?>"><?php echo __('Shadow Inset')?></label>
</p>
<?php endif;?>
<?php
}

function noo_menu_border_style($key=''){
	$options = array('solid','dashed','dotted','double','groove','inset','outset','ridge');
	?>
	<select class="form-control" id="<?php echo $key.'_border_style'?>" name="<?php echo noo_menu_get_option_name($key.'_border_style')?>" onchange="updatemenu()">
	<?php
	foreach ($options as $option){
		$seleced = noo_menu_get_option($key.'_border_style');
		?>
		<option value="<?php echo $option?>" <?php if($option == $seleced):?>selected="selected"<?php endif;?>><?php echo ucfirst($option)?></option>
		<?php
	}
	?>
	</select>
	<?php
}

function noo_menu_element_slider($element='',$label='',$min='',$max='',$default='',$unit='px',$run = 0){
if(empty($element) || empty($label))
	return '';
?>
<p>
	<label for="<?php $element?>"><?php echo __($label,NOO_MENU) ?></label>
	<strong class="text-info"><span id="<?php echo $element?>"><?php echo (int) noo_menu_get_option($element,$default)?></span><?php echo $unit?></strong>
</p>
<div data-run="<?php echo $run ?>" id="<?php echo $element ?>" class="slider" data-min="<?php echo $min?>" data-max="<?php echo $max?>"></div>
<input type="hidden" id="<?php echo $element?>" name="<?php echo noo_menu_get_option_name($element)?>" value="<?php echo (int) noo_menu_get_option($element,$default)?>" />
<?php
}

function noo_menu_element_color($element='',$label='',$default='',$opacity=false){
if(empty($element) || empty($label))
	return '';
?>
<p>
	<label for="<?php echo $element?>"><?php echo __($label,NOO_MENU) ?> </label>
	<input class="form-control  color" id="<?php echo $element?>" <?php if($opacity):?>data-rgbastring="<?php echo noo_hex2rgba(noo_menu_get_option($element,$default),noo_menu_get_option($element.'_opacity','1')); ?>" data-opacity="<?php echo noo_menu_get_option($element.'_opacity','1')?>"<?php endif;?> value="<?php echo noo_menu_get_option($element,$default)?>" name="<?php echo noo_menu_get_option_name($element)?>" type="text" onchange="updatemenu()" />
	<?php if($opacity):?>
	<input id="<?php echo $element.'_opacity'?>" value="<?php echo noo_menu_get_option($element.'_opacity','1')?>" name="<?php echo noo_menu_get_option_name($element.'_opacity')?>" type="hidden" />
	<?php endif;?>
</p>
<?php
}

function noo_meu_border_radius($key='',$boxname='Corner Radius'){
?>
<?php if($boxname):?>
<h3><?php echo __($boxname,NOO_MENU) ?></h3>
<?php endif;?>
<?php 
 noo_menu_element_slider($key.'_radius_top_left','Radius Top Left');
 noo_menu_element_slider($key.'_radius_top_right','Radius Top Right');
 noo_menu_element_slider($key.'_radius_bottom_right','Radius Bottom Right');
 noo_menu_element_slider($key.'_radius_bottom_left','Radius Bottom Left');
}

function noo_menu_background($key='',$gradien=true,$solid=true,$boxname=true,$solid_name="Background Color",$radio=true){
if(empty($key))
	return '';
?>
<?php if($boxname):?>
<?php if($boxname === true ) $boxname = 'Background';?>
<h3><?php echo __($boxname,NOO_MENU) ?></h3>
<?php endif;?>

<?php if($solid):?>
<?php if($radio):?>
<input id="<?php echo $key?>_solid_background" type="radio" name="<?php echo noo_menu_get_option_name($key.'_background')?>" <?php if(noo_menu_get_option($key.'_background','solid') == 'solid'):?>checked="checked"<?php endif;?> value="solid" onchange="updatemenu()" />
<label for="<?php echo $key?>_solid_background">&nbsp;<?php echo __('Solid Background',NOO_MENU) ?></label>
<?php endif;?>
<?php noo_menu_element_color($key.'_solid_background_color',$solid_name,'',true)?>
<?php endif;?>

<?php if($gradien):?>
<input type="<?php if($radio):?>radio<?php else: ?>checkbox<?php endif;?>" id="<?php echo $key?>_gradient_background" name="<?php echo noo_menu_get_option_name($key.'_background')?>" <?php if(noo_menu_get_option($key.'_background','solid') == 'gradient'):?>checked="checked"<?php endif;?>  value="gradient"  onchange="updatemenu()" />
<label for="<?php echo $key?>_gradient_background">&nbsp;<?php echo __('Gradient Background',NOO_MENU) ?></label>
<?php 
noo_menu_element_color($key.'_gradient_background_start_color','Start Color','',true);
noo_menu_element_color($key.'_gradient_background_end_color','End Color','',true);

endif;
?>

<?php
}

function noo_menu_border_position($key=''){
$positions = array('top','right','bottom','left');
?>
<div id="<?php echo $key.'_border_position' ?>" class="clearfix">
<label style="display: block;"><?php echo __('Border Position',NOO_MENU)?></label>
<?php foreach ($positions as $position):?>
  <p class="pull-left">
  	<input id="<?php echo $key.'_border_position_'.$position ?>"  <?php if($position == noo_menu_get_option($key.'_border_position_'.$position,'all')):?>checked="checked"<?php endif?> type="checkbox" onchange="updatemenu()" value="<?php echo $position?>" name="<?php echo noo_menu_get_option_name($key.'_border_position_'.$position)?>" style="margin: 0 0 0 10px;">
	<label for="<?php echo $key.'_border_position_'.$position ?>"><?php echo __(ucfirst($position),NOO_MENU)?></label>
  </p>
<?php endforeach;?>
</div>
<?php 
}



