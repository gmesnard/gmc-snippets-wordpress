<?php


class Noo_Menu_Admin {
	
	protected $_sidebar_id = 'noo_megamenu';
	
	protected $_menu=0;
	
	public function __construct($menu = 0){
		if(!empty($menu)){
			global $noo_menu_id;
			$this->_menu = $noo_menu_id = $menu;
		}
		add_action("admin_print_scripts-nav-menus.php",array(&$this,'enqueue_scripts_menus'));
		
		//add_action('admin_menu', array(&$this, 'admin_menu'),10);
		add_action('admin_init', array(&$this,'init'));
		add_action('wp_delete_nav_menu', array(&$this,'delete_option'),10);
		
		add_action('wp_ajax_noo_menu_save_style', array(&$this,'save_style'));
		add_action('wp_ajax_noo_menu_delete_style', array(&$this,'delete_style'));
		add_action('wp_ajax_noo_menu_get_style', array(&$this,'get_style'));
		
		add_action('wp_ajax_noo_menu_list_widgets',array(&$this,'get_list_widgets'));
		add_action('wp_ajax_noo_menu_add_widget',array(&$this,'add_widget'));
		add_action('wp_ajax_noo_menu_get_option_widget',array(&$this,'get_option_widget'));
		add_action('wp_ajax_noo_menu_get_page', array(&$this,'get_page'));
		
		add_action('wp_ajax_noo_menu_update_option', array(&$this,'update_option'));
		add_action('wp_ajax_noo_menu_delete_option', array(&$this,'delete_option'));
		
		add_action('wp_ajax_noo_menu_is_enable', array(&$this,'is_enable'));

	}
	
	public function init(){
		
		register_setting('noo_menu'.$this->_menu,'noo_menu'.$this->_menu);
		
		register_sidebar ( array (
				'name' => 'Noo MegaMenu',
				'id' => $this->_sidebar_id,
				'description' => 'This is sidebar widget that will use in Noo Megamenu',
		));
	}
	
	public function save_style(){
		
		$style_name = isset($_POST['style_name'] )  ? $_POST['style_name'] : '';
		$menu = isset($_POST['menu']) ? $_POST['menu'] : "";
		if (empty($style_name) || empty($menu)) {
			die('0');
		}
		
		$option = 'noo_menu'.$menu;
		$style = isset($_POST[$option]) ? $_POST[$option] : array();
		
		$option_name = 'noo_menu_style';
		$style_arr = array('name'=>stripslashes($style_name),'style'=>$style);
		$style_id = sanitize_title($style_name).'_'.rand();
		$saved_style = get_option($option_name);
		
		if($saved_style){
			$saved_style[$style_id] = $style_arr;
			update_option($option_name, $saved_style);
		}else{
			$new_style = array();
			$new_style[$style_id] = $style_arr;
			add_option($option_name,$new_style,'','no');
		}
		die('1');
	}
	
	public function get_style(){
		ob_start();
		$this->_preset_style_list();
		echo ob_get_clean();
		die;
	}
	
	protected function _preset_style_list(){
		?>
		<ol class="noo-menu-preset-style-list">
		<?php
		if($styles = get_option('noo_menu_style')){
			
			foreach ($styles as $id=>$style){
			?>
			<li>
				<a class="noo-menu-apply-preset-style" href="#" data-id="<?php echo $id?>"><?php echo $style['name']?></a>
				<a class="noo-menu-del-preset-style" data-id="<?php echo $id?>">x</a>
			</li>
			<?php
			}
		}
		?>
		</ol>
		<?php
	}
	
	public function delete_style(){
		$style_id = $_POST['style_id'];
		if (!isset($style_id) || $style_id == "") {
			die('0');
		}
		$option_name = 'noo_menu_style';
		$saved_style = get_option($option_name);
		unset($saved_style[$style_id]);
		if (count($saved_style) > 0) {
			update_option($option_name, $saved_style);
		} else {
			delete_option($option_name);
		}
		die('1');
	}
	
	public function delete_nav_menu_listener($menu_id){
		$this->_delete_option($menu_id);
	}
	
	private function _delete_option($menu_id){
		delete_option('noo_menu'.$menu_id);
	}
	
	public function is_enable(){
		$menu = isset($_POST['menu']) ? $_POST['menu'] : 0;
		global $noo_menu_id;
		if($menu){
			$this->_menu = $noo_menu_id = $menu;
		}
		echo noo_menu_get_option('enable',1);
		die;
	}
	
	
	public function delete_option(){

		check_ajax_referer( 'delete-option', 'security' );
		
		$menu = isset($_POST['menu']) ? $_POST['menu'] : 0;
		
		if($menu){
			$this->_delete_option($menu);
		}
		die;
	}
	
	public function update_option(){
		
		check_ajax_referer( 'update-option', 'security' );
		
		$this->_menu = $_POST['menu'];
		$option = 'noo_menu'.$this->_menu;
		$value = null;
		
		if ( isset( $_POST[ $option ] ) ) {
			$value = $_POST[ $option ];
			if ( ! is_array( $value ) )
				$value = trim( $value );
			$value = wp_unslash( $value );
		}
		
		update_option( $option, $value );
		
		die('1');
	}
	
	public function get_list_widgets(){
		check_ajax_referer( 'get-list-widgets', 'security' );
		
		echo json_encode(array(
			'list_widgets'=>$this->_list_widgets()
		)); 
		
		die();
	}
	
	public function get_option_widget(){
		check_ajax_referer( 'get-option-wiget', 'security' );
		
		$seleced = $_POST['seleced'];
		
		$sidebars = wp_get_sidebars_widgets();
		$sidebar = isset($sidebars[$this->_sidebar_id]) ? $sidebars[$this->_sidebar_id] : array();
		
		?>
        <option value=""></option>
		<?php
			foreach ($sidebar as $id){

				$widget = noo_menu_get_widget($id);
				
				if (!empty($widget)){
				?>
				<?php 
				$title = $widget->name;
				if(property_exists($widget,'params')){
					if(isset($widget->params['title']) && trim($widget->params['title']) != ''){
						$title .= ': '.$widget->params['title'].'';
					}
				}
				?>
				<option value="<?php echo $widget->id ?>" <?php if ($widget->id == $seleced):?>selected="selected"<?php endif;?>><?php echo $title ?></option>
				<?php 
			 }
		 }
		  ?>
		<?php
		die();
	}
	
	public function get_page(){
		check_ajax_referer( 'get-page', 'security' );
		$menu = $_POST['menu'];
		$style_id = isset($_POST['style_id']) ? $_POST['style_id'] : 0;
		global $noo_menu_id,$noo_menu_style_id;
		if($menu){
			$this->_menu = $noo_menu_id = $menu;
			$noo_menu_id = $menu;
		}
		if($style_id){
			$noo_menu_style_id = $style_id;
		}
		ob_start();
		$this->admin_page();
		echo ob_get_clean();
		die();
	}
	
	public function add_widget(){
		
		check_ajax_referer( 'add-widget', 'security' );
		
		global $wp_registered_widget_controls,$wp_registered_widgets;
		
		$widget_id = $_POST['widget_id'];
		
		$control = isset($wp_registered_widget_controls[$widget_id]) ? $wp_registered_widget_controls[$widget_id] : array();
		$widget = $wp_registered_widgets[$widget_id];
		
		
		if ( ! isset( $widget['params'][0] ) )
			$widget['params'][0] = array();
		
		
		$widget_number = isset($control['params'][0]['number']) ? $control['params'][0]['number'] : '';
		$id_base = isset($control['id_base']) ? $control['id_base'] : $widget_id;
		
		if( !function_exists('next_widget_id_number') )
			require_once(ABSPATH . 'wp-admin/includes/widgets.php');

		$multi_number = next_widget_id_number($id_base);
		
		$id_format = $id_base.'-'.$multi_number;
		
		

		$sidebars = wp_get_sidebars_widgets();
		$sidebar = isset($sidebars[$this->_sidebar_id]) ? $sidebars[$this->_sidebar_id] : array();
		$sidebarArr = array();
		$i=0;
		foreach ($sidebar as $k=>$v){
			$sidebarArr[$k]= "widget-{$i}_{$v}";
			$i++;
		}
		$c = $i++;
		$sidebarArr[$c] = "widget-{$c}_{$id_format}";
		
		$sidebar_order = '';
		foreach ($sidebars as $key=>$values){
			if($key == $this->_sidebar_id)
				continue;
			
			$sidebar_order_values = array();
			if(is_array($values) && !empty($values)){
				$j=0;
				foreach ($values as $v1=>$v2){
					$sidebar_order_values[$v1] =  "widget-{$j}_{$v2}";
					$j++;
				}
			}
			$sidebar_order .='<input class="noo-menu-widget-order" type="hidden" data-sidebar-id="'.$key.'" value="'.implode(',', $sidebar_order_values).'">';
		}
		
		if ( isset($wp_registered_widget_controls[$widget['id']]['id_base']) && isset($widget['params'][0]['number']) ) {
			$add_new = 'multi';
		} else {
			$add_new = 'single';
		}
		
		ob_start();
		?>
		<form action="" method="post" id="nooWidgetForm">
			<div class="widget-content">
				<?php
				if (isset($control['callback']) ):
					call_user_func_array($control['callback'],array('number'=>-1));
				?>
				<input type="hidden" name="action" value="save-widget" />
				<input type="hidden" name="widget-id" class="widget-id" value="<?php echo esc_attr($id_format); ?>" />
				<input type="hidden" name="id_base" class="id_base" value="<?php echo esc_attr($id_base); ?>" />
				<input type="hidden" name="widget-width" class="widget-width" value="<?php if (isset( $control['width'] )) echo esc_attr($control['width']); ?>" />
				<input type="hidden" name="widget-height" class="widget-height" value="<?php if (isset( $control['height'] )) echo esc_attr($control['height']); ?>" />
				<input type="hidden" name="widget_number" class="widget_number" value="<?php echo esc_attr($widget_number); ?>" />
				<input type="hidden" name="multi_number" class="multi_number" value="<?php echo esc_attr($multi_number); ?>" />
				<input type="hidden" name="sidebar" value="<?php echo $this->_sidebar_id?>" />
				<?php wp_nonce_field( 'save-sidebar-widgets', 'savewidgets', false ); ?>
				<input type="hidden" name="add_new" class="add_new" value="<?php echo esc_attr($add_new); ?>" />
				<input type="hidden" id="sidebars_noo_megamenu_list_widgets" data-sidebar-id = "<?php echo $this->_sidebar_id?>" value="<?php echo implode(',', $sidebarArr)?>">
				<?php echo $sidebar_order?>
				<?php
				else:
						echo "\t\t<p>" . __('There are no options for this widget.') . "</p>\n";
				endif;
				?>
			</div>
		</form>
		<?php 
		$control_form  = ob_get_clean();
		
		echo json_encode(array(
			'widget_form'=>$control_form
		));
		
		die();
	}
	
	protected function _list_widgets(){

		global $wp_registered_widgets;
		$sort = $wp_registered_widgets;
		usort( $sort, '_noo_sort_name_callback' );
		$done = array();
		
		ob_start();
		$i = 0;
		$widget_count = count($sort);
		foreach($sort as $widget){
			
			if ( in_array( $widget['callback'], $done, true ) ) // We already showed this multi-widget
				continue;
			
			if($i++ % 3 == 0):
			?>
			<div class="noo-row">
			<?php
			endif;
			$done[] = $widget['callback'];
			
			$widget_title = esc_html( strip_tags( $widget['name'] ) );
			$widget_id = $widget['id'];
			
			
			static $c = 0;
			$c++;
			
			$_temp_id = $widget_id;
			if ( isset($wp_registered_widget_controls[$widget['id']]['id_base']) && isset($widget['params'][0]['number']) ) {
				$id_base = $wp_registered_widget_controls[$widget['id']]['id_base'];
				$_temp_id = "$id_base-__i__";
			}

		?>
			<div id="<?php echo "widget-{$c}_{$_temp_id}"?>" class="widget noo-span4">
				<a class="widget-title" data-widget-title="<?php echo $widget_title ?>" href="#" data-widget-id = "<?php echo $widget_id ?>">
					<span><?php echo $widget_title; ?></span>
				</a>
				<p>
				<?php echo ( $widget_description = wp_widget_description($widget_id))  ? "$widget_description\n" : "$widget_title\n"; ?>
				</p>
			</div>
		<?php
		if ($i % 3 == 0 || $i == $widget_count) :
		?>
		</div>
		<?php
		endif;
		?>
		<?php
		}
		return ob_get_clean();
		
	}
	
	public function enqueue_styles(){
		wp_register_style('bootstrap-popover',NOO_MENU_URL.'assets/css/bootstrap-popover.css');
		wp_register_style('fontawesome-iconpicker',NOO_MENU_URL.'assets/css/fontawesome-iconpicker.css', array( 'bootstrap-popover' ));

		wp_register_style('noo-menu-minicolors',NOO_MENU_URL.'assets/css/jquery.minicolors.css');
		wp_register_style('noo-menu-build',NOO_MENU_URL.'assets/css/build.css');
		wp_register_style('noo-menu-admin',NOO_MENU_URL.'assets/css/admin.css', array('noo-font-awesome', 'fontawesome-iconpicker'));

		wp_enqueue_style('noo-menu-build');
		wp_enqueue_style('noo-menu-admin');
		wp_enqueue_style('noo-menu-minicolors');
		
	}
	
	public function enqueue_scripts(){
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-slider');
		wp_enqueue_script('jquery-ui-droppable');
		wp_enqueue_script('jquery-ui-sortable');
		wp_enqueue_script('postbox');
		wp_enqueue_media();
		
		wp_register_script('noo-font-awesome', NOO_MENU_URL.'assets/js/font-awesome.js',array(),NOO_MENU_VERSION);
		wp_register_script('fontawesome-iconpicker', NOO_MENU_URL.'assets/js/fontawesome-iconpicker.js',array(),NOO_MENU_VERSION);
		wp_register_script('noo-menu-block-ui', NOO_MENU_URL.'assets/js/jquery.blockUI.min.js',array(),NOO_MENU_VERSION);
		wp_register_script('noo-menu-smart-fixed', NOO_MENU_URL.'assets/js/jquery.smartfixedobject.js',array(),NOO_MENU_VERSION);
		wp_register_script('noo-menu-minicolors',NOO_MENU_URL.'assets/js/jquery.minicolors.custom.js',array(),NOO_MENU_VERSION);
		wp_register_script('noo-menu-bootstrap-tab',NOO_MENU_URL.'assets/js/bootstrap-tab.js',array(),'2.3.2');
		wp_register_script('noo-menu-bootstrap-modal', NOO_MENU_URL.'assets/js/bootstrap-modal.js',array(),'2.3.2');
		wp_register_script('noo-menu-updatemenu', NOO_MENU_URL.'assets/js/updatemenu.js', array(),NOO_MENU_VERSION);
		wp_register_script('noo-menu-admin', NOO_MENU_URL.'assets/js/admin.js', array('fontawesome-iconpicker','noo-menu-block-ui','noo-menu-smart-fixed','noo-menu-minicolors','noo-menu-bootstrap-modal' ,'noo-menu-bootstrap-tab','noo-menu-updatemenu'),NOO_MENU_VERSION);
		
		$AdminMegamenu = array(
			'plugin_page'=>NOO_MENU,
			'plugin_url'=>NOO_MENU_URL,
			'switchMsg'=>__('Swith and Save Builder?',NOO_MENU),
			'navSwitchText'=>__('Switch to NOO Menu',NOO_MENU),
			'navEnableText'=>__('Enable NOO Menu',NOO_MENU),
			'ajax_url'=>admin_url( 'admin-ajax.php', 'relative' ),
			'get_list_widgets_security'=>wp_create_nonce('get-list-widgets'),
			'add_widget_security'=>wp_create_nonce('add-widget'),
			'get_option_widget'=>wp_create_nonce('get-option-wiget'),
			'sidebar_id'=>$this->_sidebar_id,
			'get_page_security'=>wp_create_nonce('get-page'),
			'update_option_security'=>wp_create_nonce('update-option'),
			'delete_option_security'=>wp_create_nonce('delete-option'),
			'noo_menu_css'=>NOO_MENU_URL.'assets/css/style.css',
			'noo_menu_default_css'=>NOO_MENU_URL.'assets/css/default.css',
			'noo_menu_pc_css'=>NOO_MENU_URL.'assets/css/pc.css',
			'deleteMenuSetting'=>__("You are about to delete settings of this menu. \n 'Cancel' to stop, 'OK' to delete.",NOO_MENU),
			'disableMegamenu'=>__("You are about to disable NOO Menu on this menu, this menu will now function as normal Wordpress menu.\n 'Cancel' to stop, 'OK' to disable.",NOO_MENU),
		);
		wp_localize_script('noo-menu-admin', 'NooMegamenuOptions', $AdminMegamenu);
		wp_enqueue_script('noo-menu-admin');
	}
	
	public function enqueue_scripts_menus(){

		if( is_admin() ) {
			$this->enqueue_styles();
			$this->enqueue_scripts();
		}
	}
	
	public function render_option($id,$params){
		$params = wp_parse_args((array)$params,array(
				'type'=>'',
				'label'=>'',
				'default'=>'',
				'max'=>'',
				'options'=>array()
		));
		
		extract($params,EXTR_SKIP);
		
		$name = noo_menu_get_option_name($id);//'noo_menu'.$this->_menu.'['.$id.']';
		
		//echo '<td scope="row" '.(!empty($label)? '':'colspan="4"').'>';
		switch ($type) {
			case 'text' :
				if($label){
					echo '<label for="'.$id.'">'.$label.'</label>';
				}
				echo '<input class="form-control" type="text" id="' . $id . '" value="' . noo_menu_get_option( $id ) . '" name="' . $name . '" />';
				break;
			case 'color' :
				if($label){
					echo '<label for="'.$id.'">'.$label.'</label>';
				}
				echo '<input data-default-color="#336CA6" type="text" id="' . $id . '" value="' . noo_menu_get_option ( $id ) . '" name="' . $name . '" />';
				echo '<script type="text/javascript">
						jQuery(document).ready(function($){
						    $("#' . $id . '").wpColorPicker();
						});
					 </script>
					 ';
				break;
			case 'select' :
				if($label){
					echo '<label for="'.$id.'">'.$label.'</label>';
				}
				echo '<select class="form-control" id="' . $id . '" name="' . $name . '">';
				foreach ( $options as $key => $value ) {
					$selected = noo_menu_get_option ( $id ) == $key ? ' selected="selected"' : '';
					echo '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
				}
				echo '</select>';
				break;
			case 'menu' :
				$menus = get_terms ( 'nav_menu', array (
						'hide_empty' => false 
				) );
				if (is_wp_error ( $menus )) {
					echo __ ( 'Menu does not exist', 'dhsm' );
				}
				echo '<select id="' . $id . '" name="' . $name . '">';
				echo '<option value="0" >' . sprintf ( '&mdash; %s &mdash;', esc_html__ ( 'Select a Menu' ) ) . '</option>';
				foreach ( $menus as $menu ) {
					$selected = noo_menu_get_option ( $id ) == $menu->term_id ? ' selected="selected"' : '';
					echo '<option value="' . $menu->term_id . '" ' . $selected . '>' . $menu->name . '</option>';
				}
				echo '</select>';
				break;
			case 'build':
				$menu_build_output = '';
				if($this->_menu){
					
					$menu_build = new Noo_Menu_Build($this->_menu);
					$menu_build_options = array('settings'=>json_decode(noo_menu_get_option('menu_config'),true),'params'=>get_option('noo_menu'.$this->_menu));
					$menu_build_output = $menu_build->output($menu_build_options);
				}
				echo $this->build($menu_build_output);
			break;
			case 'text_slider':
				//echo '<p>';
				noo_menu_element_slider($id,$label,'',$max,$default);
				//echo '</p>';
			break;
			default :
				break;
		}
		//echo '</td>';
		
	}
	
	public function admin_menu(){
		add_menu_page( __('Noo Menu',NOO_MENU), __('Noo Menu',NOO_MENU), 'manage_options','noo-menu', array(&$this, 'admin_page'));
	}
	
	public function admin_page($frontend = false){
		?>
<div class="noo-menu-frame">
	<div class="noo-menu-frame-header clearfix">
		<?php //echo __('Mega Builder',NOO_MENU)?>
		<?php echo __('Shortcode',NOO_MENU)?>: <input value="<?php echo esc_attr('[noomenu menu="'.$this->_menu.'"]') ?>" class="wp-ui-text-highlight code" type="text" readonly="readonly" onfocus="this.select();">
		<button type="button" class="button button-primary noo-menu-save-change"><?php echo (noo_menu_get_option('enable',1) > 0 ? __('Save Menu',NOO_MENU) : __('Save to Enable MegaMenu',NOO_MENU) ) ?></button>
		<button type="button" class="button noo-menu-switch"><?php echo __('Switch to WP Menu',NOO_MENU) ?></button>
		
	</div>
	<form action="" method="post" id="nooMenuForm">
		<input type="hidden" id="menu" value="<?php echo $this->_menu ?>"/>
		<input type="hidden" id="noo-menu-enable" value="<?php echo noo_menu_get_option('enable',1)?>" name="<?php echo noo_menu_get_option_name('enable')?>" />
		<?php 
		wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', false );
		wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', false );
		?>
		<table class="form-table">
			<tbody>
					<tr>
						<td>
						<?php 
						$menu_build_output = '';
						if($this->_menu){
							$menu_build = new Noo_Menu_Build($this->_menu);
							$menu_build_options = array('settings'=>json_decode(noo_menu_get_option('menu_config'),true),'params'=>get_option('noo_menu'.$this->_menu));
							$menu_build_output = $menu_build->output($menu_build_options,$frontend);
						}
						echo $this->build($menu_build_output);
						?>
						</td>
					</tr>
					<tr>
						<td scope="row">
						<?php 
						ob_start();
						require_once NOO_MENU_DIR.'includes/noo-menu-admin-tabs.php';
						echo ob_get_clean();
						?>
						</td>
					</tr>
				</tbody>
		</table>
	</form>
	<div class="modal hide fade" id="nooWidgetControlModal" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			<h3 id="nooWidgetControlModalLabel"><?php echo __('List Widgets',NOO_MENU) ?></h3>
		</div>
		<div class="modal-body">
		</div>
		<div class="modal-footer">
			<button class="button" data-dismiss="modal" aria-hidden="true"><?php echo __('Close',NOO_MENU) ?></button>
			<button id="nooWidgetControlModalSubmit" class="button button-primary" type="button" style="display: none;"><?php echo __('Save changes',NOO_MENU) ?></button>
			<span style="" class="spinner"></span>
		</div>
	</div>
	<div id="idSwitchDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-body">
			<p><?php echo __('It looks like you have been editing something -- if you leave before saving your changes will be lost.',NOO_MENU) ?></p>
		</div>
		<div class="modal-footer">
			<button id="popup-switch-save-button" data-value="1" class="button button-primary" type="button"><?php echo __('Save Changes',NOO_MENU) ?></button>
			<button id="popup-switch-no-save-button" data-value="0" class="button button-primary" type="button"><?php echo __('Don\'t Save',NOO_MENU) ?></button>
			<button class="button" data-dismiss="modal" data-value="-1" type="button" aria-hidden="true"><?php echo __('Cancel',NOO_MENU) ?></button>
			
		</div>
	</div>
	<div id="idIconsDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-body">
			<div class="divDialogElements">
				<div class="tabbable">
					<ul id="idTabTitles" class="nav nav-tabs" data-tabs="tabs">
						<li class="active"><a href="#one" data-toggle="tab">A</a></li>
						<li><a href="#two" data-toggle="tab">B</a></li>
						<li><a href="#three" data-toggle="tab">C</a></li>
						<li><a href="#four" data-toggle="tab">D</a></li>
						<li><a href="#five" data-toggle="tab">E</a></li>
						<li><a href="#six" data-toggle="tab">F</a></li>
						<li><a href="#seven" data-toggle="tab">G</a></li>
						<li><a href="#eight" data-toggle="tab">H</a></li>
						<li><a href="#nine" data-toggle="tab">I</a></li>
					</ul>
					<div id="idTabsContent" class="tab-content" style="margin-top: 15px;">
						<div class="active tab-pane" id="one"></div>
						<div class="tab-pane" id="two"></div>
						<div class="tab-pane" id="three"></div>
						<div class="tab-pane" id="four"></div>
						<div class="tab-pane" id="five"></div>
						<div class="tab-pane" id="six"></div>
						<div class="tab-pane" id="seven"></div>
						<div class="tab-pane" id="eight"></div>
						<div class="tab-pane" id="nine"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="noo-menu-frame-header clearfix">
		<a href="#" id="noo-menu-clear-config" class="noo-menu-action-no"><?php echo __('Clear All Options',NOO_MENU)?></a>
		<?php if(noo_menu_get_option('enable',1)>0):?>
		<a href="#" id="noo-menu-disable-nav" class="noo-menu-action-no"><?php echo __('Disable NOO Menu',NOO_MENU)?></a>
		<?php endif;?>
		<button type="button" class="button button-primary noo-menu-save-change"><?php echo (noo_menu_get_option('enable',1) > 0 ? __('Save Menu',NOO_MENU) : __('Save to Enable MegaMenu',NOO_MENU) ) ?></button>
		<button type="button" class="button noo-menu-switch"><?php echo __('Switch to WP Menu',NOO_MENU) ?></button>
	</div>
</div>
<?php
	}
	
	public function build($menu=null){
		$html = '
<div id="noo-admin-megamenu" class="noo-admin-megamenu noo-menu-item-options">
  <div class="admin-inline-toolbox clearfix">
    <div class="noo-admin-mm-row clearfix">
	  
      <div id="noo-admin-preview-intro" class="pull-left hide">
        <h3>'.__('Style Preview',NOO_MENU) .'</h3>
        <p>'.__('Style preview is the live preview which shows how your menu looks like in real-time. Feel free to change the style settings to see your menu in action.',NOO_MENU) .'</p>
      </div>
				
      <div id="noo-admin-mm-intro" class="pull-left">
        <h3>'.__('Mega Menu Toolbox',NOO_MENU) .'</h3>
        <p>'.__('This toolbox includes all settings of megamenu, just select menu then configure. There are 3 level of configuration: sub-megamenu setting, column setting and menu item setting.',NOO_MENU) .'</p>
      </div>
	
      <div id="noo-admin-mm-tb">
        <div id="noo-admin-mm-toolitem" class="admin-toolbox">
          <h3>'.__('Item Configuration',NOO_MENU) .'</h3>
         
          <ul>
            <li>
              <label class="hasTip" title="'.__('Enable or disable submenus',NOO_MENU) .'">'.__('Submenu',NOO_MENU) .'</label>
              <fieldset class="radio btn-group toolitem-sub">
                <input type="radio" id="toggleSub0" class="toolbox-toggle" data-action="toggleSub" name="toggleSub" value="0"/>
                <label for="toggleSub0">'.__('No',NOO_MENU) .'</label>
                <input type="radio" id="toggleSub1" class="toolbox-toggle" data-action="toggleSub" name="toggleSub" value="1" checked="checked"/>
                <label for="toggleSub1">'.__('Yes',NOO_MENU) .'</label>
              </fieldset>
            </li>
          </ul>
          <ul>
            <li>
              <label class="hasTip" title="'.__('Group submenus or not.',NOO_MENU) .'">'.__('Group',NOO_MENU) .'</label>
              <fieldset class="radio btn-group toolitem-group">
                <input type="radio" id="toggleGroup0" class="toolbox-toggle" data-action="toggleGroup" name="toggleGroup" value="0"/>
                <label for="toggleGroup0">'.__('No',NOO_MENU) .'</label>
                <input type="radio" id="toggleGroup1" class="toolbox-toggle" data-action="toggleGroup" name="toggleGroup" value="1" checked="checked"/>
                <label for="toggleGroup1">'.__('Yes',NOO_MENU) .'</label>
              </fieldset>
            </li>
          </ul>
          <ul>
            <li>
              <label class="hasTip" title="'.__('Move menu item to right or left column.',NOO_MENU) .'">'.__('Positions',NOO_MENU) .'</label>
              <fieldset class="btn-group">
                <a href="" class="btn toolitem-moveleft toolbox-action" data-action="moveItemsLeft" title="'.__('Move to Left Column',NOO_MENU) .'"><i class="icon-arrow-left"></i></a>
                <a href="" class="btn toolitem-moveright toolbox-action" data-action="moveItemsRight" title="'.__('Move to Right Column',NOO_MENU) .'"><i class="icon-arrow-right"></i></a>
              </fieldset>
            </li>
          </ul>
          <ul>
            <li>
              <label class="hasTip" title="'.__('Add extra class to style megamenu.',NOO_MENU) .'">'.__('Extra Class',NOO_MENU) .'</label>
              <fieldset class="">
                <input type="text" class="input-medium toolitem-exclass toolbox-input" name="toolitem-exclass" data-name="class" value="" />
              </fieldset>
            </li>
          </ul>
          <ul>
            <li>
              <label class="hasTip" title="'.__('Add Icon for Menu Item. Click Icon label to visit bootstrap icons page and get Icon Class. E.g.: icon-search',NOO_MENU) .'">'.__('Icon',NOO_MENU) .'</label>
              <fieldset class="">
                <input type="text" class="input-medium toolitem-xicon toolbox-input fontawesome-iconpicker" name="toolitem-xicon" data-name="xicon" value="" />
             	<span style="display: block;" id="nooFontAwesomeDialog">( '.__('Font Awesome',NOO_MENU).' )</span>
              </fieldset>
            </li>
          </ul>
          <ul>
            <li>
              <label class="hasTip" title="'.__('Item caption',NOO_MENU) .'">'.__('Item caption',NOO_MENU) .'</label>
              <fieldset class="">
                <input type="text" class="input-medium toolitem-caption toolbox-input" name="toolitem-caption" data-name="caption" value="" />
              </fieldset>
            </li>
          </ul>
        </div>
	
        <div id="noo-admin-mm-toolsub" class="admin-toolbox">
          <h3>'.__('Submenu Configuration',NOO_MENU) .'</h3>
          <ul>
            <li>
              <label class="hasTip" title="'.__('Add a new row to the selected submenu',NOO_MENU) .'">'.__('Add row',NOO_MENU) .'</label>
              <fieldset class="btn-group">
                <a href="" class="btn toolsub-addrow toolbox-action" data-action="addRow"><i class="icon-plus"></i></a>
              </fieldset>
            </li>
          </ul>
          <ul>
            <li>
              <label class="hasTip" title="'. __('Hide this column when the menu is collapsed on small screen',NOO_MENU) .'">'.__('Hide when collapse',NOO_MENU) .'</label>
              <fieldset class="radio btn-group toolsub-hidewhencollapse">
                <input type="radio" id="togglesubHideWhenCollapse0" class="toolbox-toggle" data-action="hideWhenCollapse" name="togglesubHideWhenCollapse" value="0" checked="checked"/>
                <label for="togglesubHideWhenCollapse0">'.__('No',NOO_MENU) .'</label>
                <input type="radio" id="togglesubHideWhenCollapse1" class="toolbox-toggle" data-action="hideWhenCollapse" name="togglesubHideWhenCollapse" value="1"/>
                <label for="togglesubHideWhenCollapse1">'.__('Yes',NOO_MENU) .'</label>
              </fieldset>
            </li>
          </ul>
          <ul>
            <li>
              <label class="hasTip" title="'. __('Set submenu width(in pixel)',NOO_MENU) .'">'.__('Submenu Width (px)',NOO_MENU) .'</label>
              <fieldset class="">
                <input type="text" class="toolsub-width toolbox-input input-small" name="toolsub-width" data-name="width" value="" />
              </fieldset>
            </li>
          </ul>
          <ul>
            <li>
              <label class="hasTip" title="'.__('Align submenu',NOO_MENU) .'">'.__('Alignment',NOO_MENU) .'</label>
              <fieldset class="toolsub-alignment">
                <div class="btn-group">
                <a class="btn toolsub-align-left toolbox-action" href="#" data-action="alignment" data-align="left" title="'.__('Left',NOO_MENU) .'"><i class="icon-align-left"></i></a>
                <a class="btn toolsub-align-right toolbox-action" href="#" data-action="alignment" data-align="right" title="'.__('Right',NOO_MENU) .'"><i class="icon-align-right"></i></a>
                <a class="btn toolsub-align-center toolbox-action" href="#" data-action="alignment" data-align="center" title="'.__('Center',NOO_MENU) .'"><i class="icon-align-center"></i></a>
                <a class="btn toolsub-align-justify toolbox-action" href="#" data-action="alignment" data-align="justify" title="'.__('Justify',NOO_MENU) .'"><i class="icon-align-justify"></i></a>
                </div>
              </fieldset>
            </li>
          </ul>
          <ul>
            <li>
              <label class="hasTip" title="'.__('Add extra class to style megamenu.',NOO_MENU) .'">'.__('Extra Class',NOO_MENU) .'</label>
              <fieldset class="">
                <input type="text" class="toolsub-exclass toolbox-input input-medium" name="toolsub-exclass" data-name="class" value="" />
              </fieldset>
            </li>
          </ul>
        </div>
	
        <div id="noo-admin-mm-toolcol" class="admin-toolbox">
          <h3>'.__('Column Configuration',NOO_MENU) .'</h3>
          <ul>
            <li>
              <label class="hasTip" title="'. sprintf("Click <i class='icon-plus-sign'></i> to add a new column right after the column selection<br />Click <i class='icon-minus-sign'></i> to remove the selected column").'">'.__('Add/remove Column',NOO_MENU) .'</label>
              <fieldset class="btn-group">
                <a href="" class="btn toolcol-addcol toolbox-action" data-action="addColumn"><i class="icon-plus"></i></a>
                <a href="" class="btn toolcol-removecol toolbox-action" data-action="removeColumn"><i class="icon-minus"></i></a>
              </fieldset>
            </li>
          </ul>
          <ul>
            <li>
              <label class="hasTip" title="'. __('Hide this column when the menu is collapsed on small screen',NOO_MENU) .'">'.__('Hide when collapse	',NOO_MENU) .'</label>
              <fieldset class="radio btn-group toolcol-hidewhencollapse">
                <input type="radio" id="toggleHideWhenCollapse0" class="toolbox-toggle" data-action="hideWhenCollapse" name="toggleHideWhenCollapse" value="0" checked="checked"/>
                <label for="toggleHideWhenCollapse0">'.__('No',NOO_MENU) .'</label>
                <input type="radio" id="toggleHideWhenCollapse1" class="toolbox-toggle" data-action="hideWhenCollapse" name="toggleHideWhenCollapse" value="1"/>
                <label for="toggleHideWhenCollapse1">'.__('Yes',NOO_MENU) .'</label>
              </fieldset>
            </li>
          </ul>
          <ul>
            <li>
              <label class="hasTip" title="'.__('Add the appropriate number of span columns',NOO_MENU) .'">'.__('Width (1-12)',NOO_MENU) .'</label>
              <fieldset class="">
                <select class="toolcol-width toolbox-input toolbox-select input-mini" name="toolcol-width" data-name="width">
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                  <option value="11">11</option>
                  <option value="12">12</option>
                </select>
              </fieldset>
            </li>
          </ul>
          <ul>
            <li>
              <label class="hasTip" title="'.__('Select widget to place in MegaMenu',NOO_MENU) .'">'.__('Select Widget',NOO_MENU) .'</label>
              <fieldset class="">
                <select class="toolcol-widget toolbox-input toolbox-select input-medium" name="toolcol-position" data-name="widget" data-placeholder="'.__('Select Widget',NOO_MENU) .'">
                  <option value=""></option>
			        ';?>
			        <?php
						$sidebars = wp_get_sidebars_widgets();
						$sidebar = isset($sidebars[$this->_sidebar_id]) ? $sidebars[$this->_sidebar_id] : array();
							foreach ($sidebar as $id){
								
								$widget = noo_menu_get_widget($id);
								
								if (!empty($widget)){
								?>
								<?php 
								$title = $widget->name;
								if(property_exists($widget,'params')){
									if(isset($widget->params['title']) && trim($widget->params['title']) != ''){
										$title .= ': '.$widget->params['title'].'';
									}
								}
								$html .= "<option value=\"{$widget->id}\">{$title}</option>\n";
							 }
						 }
					  ?>
	                  <?php 
	                  $html .= '
	                </select>
					<a style="display: block;" role="button" class="toolbox-action" data-action="listWidgets" href="#"><i class="icon-plus"></i> '.__('Add Widget to List',NOO_MENU).'</a>
	              </fieldset>
	            </li>
	          </ul>
	          <ul>
	            <li>
	              <label class="hasTip" title="'.__('Add extra class to style megamenu.',NOO_MENU) .'">'.__('Extra Class',NOO_MENU) .'</label>
	              <fieldset class="">
	                <input type="text" class="input-medium toolcol-exclass toolbox-input" name="toolcol-exclass" data-name="class" value="" />
	              </fieldset>
	            </li>
	          </ul>
	        </div>    
	      </div> 
	      
	      <div class="toolbox-actions-group">
			<span class="spinner" style="float: left;margin-top:5px"></span>
			<ul class="action">
				<li>
					<button type="button" class="btn" style="background:#eee">'.__('Preset Styles',NOO_MENU).'</button>
					<ul>
						<li>
							<a id="noo_menu_save_preset_style_button" class="button" href="#">'.__('Save as Preset Style',NOO_MENU).'</a>
						</li>
						<li id="noo_menu_perset">';
					?>
					<?php 
					ob_start();
					$this->_preset_style_list();
					$html .= ob_get_clean();
					?>
			<?php 
			$html .= '</li></ul>';
			$html .= '</li>
			</ul>
			<ul class="action">
				<li class="">
					<fieldset class="noo-switch">
						<span class="off active" data-trigger="noo_menu_item_options">Structure</span>
						<span class="on" data-trigger="noo_menu_style_options">Preview</span>
					</fieldset>
				</li>
				<li class="action-group btn-group" style="display:none">
					
					<button class="btn active" id="noo_menu_item_options">'.__('Menu Item Options',NOO_MENU).'</button>
					<button class="btn" id="noo_menu_style_options">'.__('Menu Style Options',NOO_MENU).'</button>
				</li>
			</ul>
				
	        <button type="button" class="btn btn-success toolbox-action toolbox-saveConfig hide" data-action="saveConfig"><i class="icon-save"></i>'.__('Save',NOO_MENU) .'</button>
	      </div>
	
	    </div>
	  </div>';
	   if(!is_null($menu)){
			$dropdown = noo_menu_get_option('horizontal_dropdown','down');
			if(noo_menu_get_option('orientation','horizontal') == 'vertical'){
				$dropdown = noo_menu_get_option('vertical_dropdown','down');
			}
		 $html .='
		  <div id="noo-admin-mm-container" class="navbar clearfix">
			<div id="noo_menu_'.$this->_menu.'" class="noonav noo_menu_'.$this->_menu.' '.noo_menu_get_option('orientation','horizontal').'-'.$dropdown.'">
		  	'.$menu.'
			</div>
		  </div>
		   ';
		  }
	 $html .= '
	<input type="hidden" id="noo_magemenu_config" name="noo_menu'.$this->_menu.'[menu_config]" value="'.esc_attr(noo_menu_get_option('menu_config')).'" />
	<input type="hidden" id="noo_magemenu_style" name="noo_menu'.$this->_menu.'[menu_style]" value="'.esc_attr(noo_menu_get_option('menu_style')).'" />
	</div>	
								
	';
	return $html;
	}
}