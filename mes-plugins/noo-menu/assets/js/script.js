

(function(a){a.fn.noomenuscrolluphidden=function(){var d=a(window),e=a(document),f=this,c=f.outerHeight(),b=d.scrollTop(),h=/(iPad|iPhone|iPod)/g.test(navigator.userAgent),g;if(!h){d.scroll(function(){var j=d.scrollTop(),k=f.offset().top+c,i=k<=j&&j>=c;if(j<0||j>(e.height()-d.height())){return}if(j<b||j<c){if(f.hasClass("noo-sticking")){f.addClass("noo-menu-hidden");  var wpadminbar = a('#wpadminbar'); var t=0; if(wpadminbar.length){t = parseFloat(a('html').css('margin-top'));} f.find('.noonav').css('top',t + f.find('.noonav').data('offset'));  }}else{if(j>b){if(f.hasClass("noo-menu-hidden")){f.removeClass("noo-menu-hidden");f.find('.noonav').css('top','-200px')}}}b=j})}else{a(document).on("touchstart",function(){b=d.scrollTop()});a(document).on("touchend",function(){var i=d.scrollTop();if(i<b||i<c){if(f.hasClass("noo-sticking")){f.slideUp()}}else{if(i>b){f.slideDown()}}b=i})}return this}})(jQuery);


(function ($) {
	$.fn.nooTransitionEnd = function (duration) {
		var called = false, $el = this
		$(this).one($.support.transition.end, function () { called = true })
		var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
		setTimeout(callback, duration)
		return this
	}
	 
	$.fn.NooMobileMenu = function(options){

		return this.each(function(){
			var $this = $(this),
			parent = $this.closest('.noonav'),
			button = parent.find('.noo-menu-collapse'),
			toggle = $this.find('li.noo-nav-item.mega'),
			$window = $(window);

			$this.find( 'ul, ul li.menu-item, ul li.menu-item > a' ).removeAttr('style').unbind().off();
			var transition = (function() {
				var transitionEnd = (function() {
					var el = document.createElement('bootstrap'), transEndEventNames = {
						'WebkitTransition' : 'webkitTransitionEnd',
						'MozTransition' : 'transitionend',
						'OTransition' : 'oTransitionEnd otransitionend',
						'transition' : 'transitionend'
					}, name

					for (name in transEndEventNames) {
						if (el.style[name] !== undefined) {
							return transEndEventNames[name]
						}
					}

				}());

				return transitionEnd && {
					end : transitionEnd
				}
			})();

			if(button.length){
				button.click(function(e){
					var scroll = $.camelCase(['scroll', 'height'].join('-'));

					if($this.hasClass('noo-collapse-open')){
						//$this['height']($this['height']())[0].offsetHeight;
						$this.addClass('noocollapsing').removeClass('noocollapse').removeClass('noo-collapse-open');
						$this.css('max-height', 0);
						//$this.css('max-height', 0).one(transition.end,function(){
							$this.removeClass('noocollapsing').addClass('noocollapse');
						 //}).nooTransitionEnd(350);
					}else{
						$this.removeClass('noocollapse').addClass('noocollapsing');
						// $this.css('max-height', 0);
						$this.addClass('noo-collapse-open');

						//$this.one(transition.end,function(){
							$this.removeClass('noocollapsing').addClass('noocollapse').addClass('noo-collapse-open').css('height', 'auto').css('max-height', '999px');
						//}).nooTransitionEnd(350).css('max-height', $this[0][scroll]);
					}
					e.stopPropagation();
					e.preventDefault();
				});
			}
			$this.closest('.noonav').find('.noo-menu-collapse-bar').off('click').on('click',function(e){
				e.stopPropagation();
				e.preventDefault();
				button.trigger('click');
			});

			function clearMenus(e) {
				$(toggle).each(function () {
					var $parent = $(this);
					if (!$parent.hasClass('open')) return;
					$parent.removeClass('open');
				})
			}

			function getViewportW() {
				var client = window.document.documentElement['clientWidth'],
				inner = window['innerWidth'];

				return (client < inner) ? inner : client;
			}

			function noowaypointsticky(a){
				var b,c;b={wrapper:'<div class="sticky-wrapper" />',stuckClass:"stuck",direction:"down right"};c=function(e,f){var d;e.wrap(f.wrapper);d=e.parent();return d.data("isWaypointStickyWrapper",true)};a.waypoints("extendFn","sticky",function(e){var f,g,d;g=a.extend({},a.fn.waypoint.defaults,b,e);f=c(this,g);d=g.handler;g.handler=function(h){var i,j;i=a(this).children(":first");j=g.direction.indexOf(h)!==-1;a(this).toggleClass("noo-sticking",j).addClass('noo-menu-hidden');if(a(this).find(".noonav").data("hide-sticky")){a(this).noomenuscrolluphidden()}i.toggleClass(g.stuckClass,j);f.height(j?i.outerHeight():"");if(d!=null){return d.call(this,h)}};f.waypoint(g);return this.data("stuckClass",g.stuckClass)});return a.waypoints("extendFn","unsticky",function(){var d;d=this.parent();if(!d.data("isWaypointStickyWrapper")){return this}d.waypoint("destroy");this.unwrap();return this.removeClass(this.data("stuckClass"))});
			}
			noowaypointsticky($);

			if (getViewportW() > 767 && $this.hasClass('horizontal')) {
				var wpadminbar = $('#wpadminbar');
				var t;
				if(wpadminbar.length){
					t = parseFloat($('html').css('margin-top'));
				}

				var wrapper = '<div id="noo-sticky-wrapper" class="noo-sticky-wrapper">';
				$this.closest('.noonav').each(function(){
					if($(this).data('sticky')){
						var $nav = $(this);
						$nav.waypoint( 'sticky' , {
							wrapper: wrapper,
							context: window,
							offset: 10,
							handler: function(direction){
								var p = $this.closest('.noonav');
								p.css('top',0);
								if(p.hasClass('noosticky')){
									p.css('top',t + $this.closest('.noonav').data('offset'));
								}
							},
							stuckClass: 'noosticky'
						});
					}
				});
				
			}
			$(window).one('resize',function(){
				if (getViewportW() > 767 && $this.hasClass('horizontal')) {
					$this.closest('.noonav').waypoint('unsticky');
					var wpadminbar = $('#wpadminbar');
					var t;
					if(wpadminbar.length){
						t = parseFloat($('html').css('margin-top'));
					}

					var wrapper = '<div id="noo-sticky-wrapper" class="noo-sticky-wrapper">';
					$this.closest('.noonav').each(function(){
						if($(this).data('sticky')){
							$(this).waypoint( 'sticky' , {
								wrapper: wrapper,
								context: window,
								offset: 0,
								handler: function(direction){
									var p = $this.closest('.noonav');
									p.css('top',0);
									if(p.hasClass('noosticky')){
										p.css('top',t + $this.closest('.noonav').data('offset'));
									}
									//$this.width($this.closest('#noo-sticky-wrapper').css('float','none').outerWidth());
								},
								stuckClass: 'noosticky'
							}); 
						}
					})
				}else{
					$this.closest('.noonav').waypoint('unsticky');
				}
			});

			// $this.find('li.noo-nav-item.mega').hover(function(){return false},function(){return false});
			if ('ontouchstart' in document.documentElement) {
				$this.find('li.noo-nav-item.mega').find(' > a').click(function(e){
					var _this = $(this);
					e.preventDefault();
					e.stopPropagation();
					var $parent = _this.parent();
					var isActive = $parent.hasClass('open');

					if (!isActive) {
						clearMenus();
						$parent.addClass('open').parentsUntil('.noo-nav').filter('li.noo-nav-item.mega').addClass('open');
						window.scrollTo(0,_this.offset().top-50);
					}else{
						if(_this.attr('href')){
							window.location.href = _this.attr('href');
						}
					}
					return false;
				});


				$(document.body).children().on('click', function(){
					clearMenus();
					$this.addClass('noocollapsing').removeClass('noocollapse').removeClass('noo-collapse-open');
					$this['height'](0);
				});

				$this.closest('.noonav').click(function(e){
					e.stopPropagation();
				});
			}
			return false;
		});
	};
	$(document).ready(function(){
		$('.noo-megamenu').each(function(){$(this).NooMobileMenu()});
		
		$( '.noo-menu-select' ).change(function() {
			var loc = $(this).find( 'option:selected' ).val();
			if( loc != '' && loc != '#' ) window.location = loc;
		});
		
		$('.noo-menu-back-button').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			$(this).closest('li').addClass("noo-menu-li-open");
		});
	});

})(window.jQuery);

