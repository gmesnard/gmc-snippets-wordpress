<?php
/*
  Plugin Name: Mobi - Better Wordpress Mobile Menu
  Plugin URI: http://mobi.phpbitsplugins.com
  Description: Create Better Wordpress Mobile Navigation for your responsive websites.
  Author: phpbits
  Version: 2.1
  Author URI: http://codecanyon.net/user/phpbits?ref=phpbits
 */

//avoid direct calls to this file

if (!function_exists('add_action')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

$mobiwp_fonts = array(
          'fontawesome'  =>  __('Font Awesome', 'mobiwp'),
          'ionicons'     =>  __('Ionicons', 'mobiwp')
);
$mobiwp_socials = array(
    'facebook'      =>    __('Facebook', 'mobiwp'),
    'twitter'       =>    __('Twitter', 'mobiwp'),
    'instagram'     =>    __('Instagram', 'mobiwp'),
    'pinterest'     =>    __('Pinterest', 'mobiwp'),
    'googleplus'    =>    __('Google+', 'mobiwp'),
    'linkedin'      =>    __('Linkedin', 'mobiwp'),
    'delicious'     =>    __('Delicious', 'mobiwp'),
    'youtube'       =>    __('Youtube', 'mobiwp'),
    'vimeo'         =>    __('Vimeo', 'mobiwp'),
    'dribbble'      =>    __('Dribbble', 'mobiwp'),
);

/*##################################
  REQUIRE
################################## */
require_once( dirname( __FILE__ ) . '/admin/defaults.setting.php');
require_once( dirname( __FILE__ ) . '/lib/includes/fontawesome.array.php');
require_once( dirname( __FILE__ ) . '/lib/includes/ionicons.array.php');
require_once( dirname( __FILE__ ) . '/core/functions.menu.walker.php');
require_once( dirname( __FILE__ ) . '/admin/functions.ajax.php');
require_once( dirname( __FILE__ ) . '/admin/functions.options.php');
require_once( dirname( __FILE__ ) . '/core/functions.enqueue.php');
require_once( dirname( __FILE__ ) . '/core/functions.display.php');

register_activation_hook( __FILE__, 'mobiwp_defaults' );

/*##################################
  SETTINGS OPTION
################################## */
function mobiwp_generalOption(){
  return (object) get_option( 'mobiwp_general_settings' );
}
function mobiwp_popupOption(){
  return (object) get_option( 'mobiwp_popup_settings' );
}
function mobiwp_appearanceOption(){
  return (object) get_option( 'mobiwp_appearance_settings' );
}
function mobiwp_socialOption(){
  return (object) get_option( 'mobiwp_social_settings' );
}
function mobiwp_fontOption(){
  return (object) get_option( 'mobiwp_font_settings' );
}
function mobiwp_otherOption(){
  return (object) get_option( 'mobiwp_other_settings' );
}
function mobiwp_logoOption(){
  return (object) get_option( 'mobiwp_logo_settings' );
}
?>