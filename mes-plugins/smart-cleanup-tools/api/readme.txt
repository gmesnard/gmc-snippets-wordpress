=== Smart Envato API Library ===
Version: 4.1
Build:   3720
Author:  Milan Petrovic
Email:   plugins@millan.rs
Website: http://www.millan.rs/

== Files ==
* envato.api.php
* envato.data.php
* envato.functions.php
* readme.txt

== Legacy Files ==
* envato.v4.php
* envato.core.php

== Storage ==
* store.transient.php
* store.site_transient.php

== Changelog ==
= 4.1 / 2015.08.23. =
* Few minor updates to the new Envato API code
* Fixed problem with assigning username for private calls in legacy API
* Fixed missing vitals() method for compatibility purpose

= 4.0 / 2015.07.12. =
* Old API now moved to Legacy
* Added New API object
* New API uses Personal Token Authentication
