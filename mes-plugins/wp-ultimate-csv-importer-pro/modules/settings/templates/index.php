<?php
/******************************************************************************************
 * Copyright (C) Smackcoders 2014 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You can contact Smackcoders at email address info@smackcoders.com.
 *******************************************************************************************/

global $wp_version, $wpdb;
$impCE = new WPImporter_includes_helper();
/*$setvalue = get_option('wpcsvprosettings');
$seovalue = $setvalue['rseooption'];*/
$setvalue = get_option('wpcsvprosettings');
if(isset($setvalue['rseooption'])){
	$seovalue = $setvalue['rseooption'];
}
if (isset($skinnyData['savesettings']) && $skinnyData['savesettings'] == 'done') { ?>
	<div id="ShowMsg" class="alert alert-warning" style="display:none"></div>
	<div id="deletesuccess"><p class="alert alert-success"><?php echo __("Settings Saved"); ?></p></div>
	<?php
	$skinnyData['savesettings'] == 'notdone';
	?>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#ShowMsg').delay(5000).fadeOut();
			$('#ShowMsg').css("display", "none");
			$('#deletesuccess').delay(5000).fadeOut();
		});
	</script>
<?php
} ?>
<div style ='text-align:center;margin:0;color:red;font-size:smaller;' id='securitymsg'> <?php echo __('Go to Security and Performance tab for your environment configuration details');?> </div></br>

<input type='hidden' id='tmpLoc' name='tmpLoc' value='<?php echo WP_CONST_ULTIMATE_CSV_IMP_DIR; ?>'/>
<div class="uiplus-settings">
<form class="add:the-list: validate" name="importerSettings" method="post" enctype="multipart/form-data">
<div id="settingheader">
        <span class="corner-icon"> <img src="<?php echo WP_CONTENT_URL;?>/plugins/<?php echo WP_CONST_ULTIMATE_CSV_IMP_SLUG;?>/images/lSettingsCorner.png" width="24" height="24" /> </span>
        <span><label id="activemenu"><?php echo __('General Settings')?></label></span>
<!--        <button class="action btnn btn-primary" onclick="saveSettings();" style="float:right;position:relative; margin: 7px 15px 5px;" value="Save" name="savesettings" type="submit">Save Changes </button>-->
</div>
<div id="settingsholder">
        <div id="sidebar">
        <ul>
                <li id="1" class="bg-sidebar selected" onclick="showsettingsoption(this.id);">
                        <span class="settings-icon"> <img src="<?php echo WP_CONTENT_URL;?>/plugins/<?php echo WP_CONST_ULTIMATE_CSV_IMP_SLUG;?>/images/settings.png" width="24" height="24" /> </span>
                        <span id="settingmenu1" >General Settings</span>
                        <span id="arrow1" class="list-arrow"></span>
                </li>
                <li id="2" class="bg-sidebar" onclick="showsettingsoption(this.id);">
                        <span class="settings-icon"> <img src="<?php echo WP_CONTENT_URL;?>/plugins/<?php echo WP_CONST_ULTIMATE_CSV_IMP_SLUG;?>/images/lcustomposts.png" width="24" height="24" /> </span>
                        <span id="settingmenu2" >Custom Posts & Taxonomy</span>
                        <span id="arrow2" class="list-arrow" style="display:none;" ></span>
                </li>
                <li id="3" class="bg-sidebar" onclick="showsettingsoption(this.id);">
                        <span class="settings-icon"> <img src="<?php echo WP_CONTENT_URL;?>/plugins/<?php echo WP_CONST_ULTIMATE_CSV_IMP_SLUG;?>/images/lcustomfields.png" width="24" height="24" /> </span>
                        <span id="settingmenu3" >Custom Fields</span>
                        <span id="arrow3" class="list-arrow" style="display:none;" ></span>
                </li>
                <li id="4" class="bg-sidebar" onclick="showsettingsoption(this.id);">
                        <span class="settings-icon"> <img src="<?php echo WP_CONTENT_URL;?>/plugins/<?php echo WP_CONST_ULTIMATE_CSV_IMP_SLUG;?>/images/lcart.png" width="24" height="24" /> </span>
                        <span id="settingmenu4" >Ecommerce Settings</span>
                        <span id="arrow4" class="list-arrow" style="display:none;" ></span>
                </li>
                <li id="5" class="bg-sidebar" onclick="showsettingsoption(this.id);">
                        <span class="settings-icon"> <img src="<?php echo WP_CONTENT_URL;?>/plugins/<?php echo WP_CONST_ULTIMATE_CSV_IMP_SLUG;?>/images/seo24.png" width="24" height="24" /> </span>
                        <span id="settingmenu5" >SEO Settings</span>
			<span id="arrow5" class="list-arrow" style="display:none;" ></span>
                </li>
                <li id="6" class="bg-sidebar" onclick="showsettingsoption(this.id);">
                        <span class="settings-icon"> <img src="<?php echo WP_CONTENT_URL;?>/plugins/<?php echo WP_CONST_ULTIMATE_CSV_IMP_SLUG;?>/images/additionalfeatures.png" width="24" height="24" /> </span>
                        <span id="settingmenu6" >Additional Features</span>
                        <span id="arrow6" class="list-arrow" style="display:none;" ></span>
                </li>
                <li id="7" class="bg-sidebar" onclick="showsettingsoption(this.id);">
                        <span class="settings-icon"> <img src="<?php echo WP_CONTENT_URL;?>/plugins/<?php echo WP_CONST_ULTIMATE_CSV_IMP_SLUG;?>/images/DBOptimize.png" width="24" height="24" /> </span>
                        <span id="settingmenu7" >Database Optimization</span>
                        <span id="arrow7" class="list-arrow" style="display:none;" ></span>
                </li>
                <li id="8" class="bg-sidebar" onclick="showsettingsoption(this.id);">
                        <span class="settings-icon"> <img src="<?php echo WP_CONTENT_URL;?>/plugins/<?php echo WP_CONST_ULTIMATE_CSV_IMP_SLUG;?>/images/security.png" width="24" height="24" /> </span>
                        <span id="settingmenu8" >Security and Performance</span>
                        <span id="arrow8" class="list-arrow" style="display:none;" ></span>
                </li>
                <li id="9" class="bg-sidebar" onclick="showsettingsoption(this.id);">
                        <span class="settings-icon"> <img src="<?php echo WP_CONTENT_URL;?>/plugins/<?php echo WP_CONST_ULTIMATE_CSV_IMP_SLUG;?>/images/ldocs24.png" width="24" height="24" /> </span>
                        <span id="settingmenu9" >Documentation</span>
                        <span id="arrow9" class="list-arrow" style="display:none;" ></span>
                </li>
	 </ul>
        </div>
	<div id="contentbar">
<!-- div-1-->
                <div id="section1" class="generalsettings">
                        <div class="title">
                                        <h3>Enabled Modules</h3>
                                        <span style="float:right;margin-right:92px;margin-top:-34px;">
                                                <a id="checkallModules" onclick="checkallOption(this.id);" name="checkallModules" value="Check All" href="#">Check All</a>
                                        </span>
                                        <span style="float:right;margin-right:5px;margin-top:-34px;">
                                                <a id="uncheckallModules" onclick="checkallOption(this.id);" value="Un Check All " name="checkallModules" href="#"> / Uncheck All
                                                </a>
                                        </span>
                        </div>
                        <div id="data">
				<table><tbody> 
				<tr><td>
					<h3 id="innertitle">Post</h3>
		                        <label><div>Enables to import posts with custompost and customfields.</div>        
					<div>Enable to import posts with attributes from csv.</div>
</label></td><td>
				</td><td style="width:119px">
                                <label id="postlabel" class="<?php echo $skinnyData['post']; ?>"><input type='checkbox' name='post' value='post' id ='post' <?php echo $skinnyData['post']; ?> style="display:none" onclick="postsetting(this.id, this.name);">Enable</label>
				<label id="nopostlabel" class="<?php echo $skinnyData['nopost']; ?>"><input type='checkbox' name='post' style="display:none" onclick="postsetting(this.id, this.name);" >Disable</label>
				</td></tr>
				<tr><td>
				<h3 id="innertitle">Page</h3>
                                        <label><div>Enables to import pages with custompost and customfields.</div>        
					<div>Enable to import pages with attributes from csv.</div></label></td><td>
                                </td><td style="width:119px">
                                <label id="pagelabel" class="<?php echo $skinnyData['page']; ?>"><input type='checkbox' name='page_module' value='page' id ='page' <?php echo $skinnyData['page']; ?> style="display:none" onclick="pagesetting(this.id, this.name);">Enable</label>
				<label id="nopagelabel" class="<?php echo $skinnyData['nopage']; ?>"><input type='checkbox' name='page_module' style="display:none" onclick="pagesetting(this.id, this.name);"> Disable </label>
				</td></tr>
                                <tr><td>
                                <h3 id="innertitle">Users</h3>
                                        <label>Enable to import users with attributes from csv.</label></td><td>
                                </td><td style="width:119px">
                                <label id="userlabel" class="<?php echo $skinnyData['users']; ?>"><input type='checkbox' name='users' value='users' id = 'users' <?php echo $skinnyData['users']; ?> style="display:none" onclick="usersetting(this.id, this.name);" >Enable </label>
				<label id="nouserlabel" class="<?php echo $skinnyData['nousers']; ?>"><input type='checkbox' name='users' style="display:none" onclick="usersetting(this.id, this.name);">Disable</label>
				</td></tr>
                                <tr><td>
                                <h3 id="innertitle">Comments</h3>
                                        <label><div>Enables to import posts with custompost and customfields.</div>        
<div>Enable to import comments for post ids from csv.</div></label></td><td>
                                </td><td style="width:119px">
                                <label id="commentslabel" class="<?php echo $skinnyData['comments']; ?>"><input type='checkbox' name='comments' value='comments' id= 'comments' <?php echo $skinnyData['comments']; ?> style="display:none" onclick="commentsetting(this.id, this.name);">Enable</label>
				<label id="nocommentslabel" class="<?php echo $skinnyData['nocomments']; ?>"><input type='checkbox' name='comments' style="display:none" onclick="commentsetting(this.id, this.name);">Disable </label>
				</td></tr>
                                <tr><td>
                                <h3 id="innertitle">Custom Post</h3>
                                        <label><div>Enables to import Customposts.</div>        
<div>Enable to import custom posts with attributes from csv</div></label></td><td>
                                </td><td style="width:119px">
                                <label id="cplabel" class="<?php echo $skinnyData['custompost']; ?>"><input type='checkbox' name='custompost' value='custompost' id = 'custompost' <?php echo $skinnyData['custompost']; ?> style="display:none" onclick="cpsetting(this.id, this.name);">Enable</label>
				<label id="nocplabel" class="<?php echo $skinnyData['nocustompost']; ?>"><input type='checkbox' name='custompost'style="display:none" onclick="cpsetting(this.id, this.name);">Disable </label>
				</td></tr>
                                <tr><td>
                                <h3 id="innertitle">Custom Taxonomy</h3>
                                        <label><div>Enables to import Custom taxonomy.</div>        
<div>Enable to import nested custom taxonomies with description and slug for each from csv</div></label></td><td>
                                </td><td style="width:119px">
				<?php 
				 $skinny_customtaxonomy = '';
				if(isset($skinnyData['customtaxonomy']))
				{
					$skinny_customtaxonomy = $skinnyData['customtaxonomy'];
				}
				else
				{

					$skinny_customtaxonomy = '';
				}


				?>
                                <label id="custaxlabel" class="<?php echo $skinny_customtaxonomy; ?>"><input type='checkbox' name='customtaxonomy' value='customtaxonomy' id = 'customtaxonomy' <?php echo $skinny_customtaxonomy; ?> style="display:none" onclick="custaxsetting(this.id, this.name);" >Enable</label><input type='hidden'id="customtaxonomystatus" value="<?php echo $skinny_customtaxonomy; ?>" />
				<label id="nocustaxlabel" class="<?php echo $skinnyData['nocustomtaxonomy']; ?>"><input type='checkbox' name='customtaxonomy' style="display:none" onclick="custaxsetting(this.id, this.name);">Disable</label>
				</td></tr>
                                <tr><td>
                                <h3 id="innertitle">Categories/Tags</h3>
                                        <label><div>Enables to import Categories.</div>    
<div>Enable to import nested categories with description and slug for each from csv.</div></label></td><td>
                                </td><td style="width:119px">
				 <?php
                                 $skinny_categories = '';
                                if(isset($skinnyData['categories']))
                                {
                                        $skinny_categories = $skinnyData['categories'];
                                }
                                else
                                {

                                        $skinny_categories = '';
                                }
                                
				$categoryvalue = '';
				$customerreviewsvalue = '';
				?>
                                <label id="catlabel" class="<?php echo $skinny_categories; ?>"><input type='checkbox' name='categories' value='categories' id = 'categories' <?php echo $skinnyData['categories']; ?> style="display:none" onclick="catsetting(this.id, this.name);" >Enable</label><input type='hidden'id="categorystatus" value="<?php echo $categoryvalue; ?>" />
				<?php if(isset($skinnyData['nocategories'])) { ?>
                                <label id="nocatlabel" class="<?php echo $skinnyData['nocategories']; ?>"><input type='checkbox' name='categories' style="display:none" onclick="catsetting(this.id, this.name);" >Disable</label>
		                <?php } ?>
                   		</td></tr>
                                <tr><td>
                                <h3 id="innertitle">Customer Reviews</h3>
                                        <label><div>Enables to import Customer reviews.</div>
<div>Enable to import customer reviews with attributes from csv.</div></label></td><td>
                                </td><td style="width:119px">
                                <label id="custrevlabel" class="<?php echo $skinnyData['customerreviews']; ?>" <?php if (($skinnyData['customerreviewstd'] == 'pluginAbsent') || ($skinnyData['customerreviewstd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='checkbox' name='rcustomerreviews' id='rcustomerreviews' value='customerreviews' <?php echo $skinnyData['customerreviews']; ?> style="display:none" onclick="cusrevsetting(this.id, this.name);" <?php if (($skinnyData['customerreviewstd'] == 'pluginAbsent') || ($skinnyData['customerreviewstd'] == 'pluginPresent')) { ?> disabled <?php } ?>>Enable</label>
                                <input type='hidden'id="customerreviewstatus" value="<?php echo $customerreviewsvalue; ?>">
				<label id="nocustrevlabel" class="<?php echo $skinnyData['nocustomerreviews']; ?>"<?php if (($skinnyData['customerreviewstd'] == 'pluginAbsent') || ($skinnyData['customerreviewstd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;"<?php } ?>><input type='checkbox' name='rcustomerreviews' style="display:none" onclick="cusrevsetting(this.id, this.name);" <?php if (($skinnyData['customerreviewstd'] == 'pluginAbsent') || ($skinnyData['customerreviewstd'] == 'pluginPresent')) { ?> disabled <?php } ?>>Disable</label>
                                </td></tr>
				</tbody></table><br />
                                <label style='color:red;'><?php echo __("Note: Supports WordPress Custom Post by default. For Custom Post Type UI plugin, please enable it under Custom Posts & Taxonomy"); ?></label>
                        </div>
                </div>
<!--div-2 -->
                <div id="section2" class="custompost" style="display:none;">
                        <div class="title" class="databorder" >
                                <h3>Custom Posts & Taxonomy </h3>
                        </div>
                        <div id="data">
                               <table>
                               <tbody>
                               <tr><td>
                               <h3 id="innertitle">Custom Post Type UI</h3>
                               <label>Import support for Custom Post Type UI data.</label></td><td>
                               <label id="cptulabel" class="<?php echo $skinnyData['custompostuitype']; ?>" <?php if (($skinnyData['cptutd'] == 'pluginAbsent') || ($skinnyData['cptutd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='checkbox' name='cptuicustompost' id="cptui" value='custompostuitype' <?php echo $skinnyData['custompostuitype']; ?> style="display:none" onclick="cptuicustompostsetting(this.id, this.name);" <?php if (($skinnyData['cptutd'] == 'pluginAbsent') || ($skinnyData['cptutd'] == 'pluginPresent')) { ?> disabled <?php } ?>>Enable</label>	
				<label id="nocptulabel" class="<?php echo $skinnyData['nocustompostuitype']; ?>" <?php if (($skinnyData['cptutd'] == 'pluginAbsent') || ($skinnyData['cptutd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" <?php } ?>><input type='checkbox' name='cptuicustompost' style="display:none" onclick="cptuicustompostsetting(this.id, this.name);" <?php if (($skinnyData['cptutd'] == 'pluginAbsent') || ($skinnyData['cptutd'] == 'pluginPresent')) { ?> disabled <?php } ?>>Disable</label>
                               
                               </td></tr>
                               <tr><td>
                               <h3 id="innertitle">Types Custom Posts & Taxonomy</h3>
                               <label>Import support for Types Custom Post Type and taxonomies data.</label> </td><td>
                               <label id="typescustompost" class="<?php echo $skinnyData['wptypes']; ?>" <?php if (($skinnyData['wptypestd'] == 'pluginAbsent') || ($skinnyData['wptypestd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='checkbox'  name='typescustompost' id='wptypespost' value='wptypes' <?php echo $skinnyData['wptypes']; ?> style="display:none" onclick="typescustompostsetting(this.id, this.name);" <?php if (($skinnyData['wptypestd'] == 'pluginAbsent') || ($skinnyData['wptypestd'] == 'pluginPresent')) { ?> disabled <?php } ?>>Enable</label>
				<label id="notypescustompost" class="<?php echo $skinnyData['nowptypes']; ?>" <?php if (($skinnyData['wptypestd'] == 'pluginAbsent') || ($skinnyData['wptypestd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" <?php } ?>><input type='checkbox'  name='typescustompost' style="display:none" onclick="typescustompostsetting(this.id, this.name);" <?php if (($skinnyData['wptypestd'] == 'pluginAbsent') || ($skinnyData['wptypestd'] == 'pluginPresent')) { ?> disabled <?php } ?>>Disable</label>
                                
                                </td></tr>
                                <tr><td>
                                <h3 id="innertitle">CCTM Custom Posts</h3>
                                <label>Import support for CCTM Custom Posts from csv.</label> </td><td>
                                <label id="cctmcustompost" class="<?php echo $skinnyData['cctm']; ?>" <?php if (($skinnyData['cctmtd'] == 'pluginAbsent') || ($skinnyData['cctmtd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='checkbox' name='cctmcustompost' id='cctm' value='cctm' <?php echo $skinnyData['cctm']; ?> style="display:none" onclick="cctmcustompostsetting(this.id, this.name);" <?php if (($skinnyData['cctmtd'] == 'pluginAbsent') || ($skinnyData['cctmtd'] == 'pluginPresent')) { ?> disabled <?php } ?>>Enable</label>
				<label id="nocctmcustompost" class="<?php echo $skinnyData['nocctm']; ?>" <?php if (($skinnyData['cctmtd'] == 'pluginAbsent') || ($skinnyData['cctmtd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" <?php } ?>><input type='checkbox' name='cctmcustompost' style="display:none" onclick="cctmcustompostsetting(this.id, this.name);" <?php if (($skinnyData['cctmtd'] == 'pluginAbsent') || ($skinnyData['cctmtd'] == 'pluginPresent')) { ?> disabled <?php } ?>>Disable</label>
                                
                                </td></tr>
                                <tr><td>
                                <h3 id="innertitle">PODS Custom Posts & Taxonomy</h3>
                                <label>Import support for PODS Custom Posts.</label> </td><td>
                                <label id="podscustompost" class="<?php echo $skinnyData['podspost']; ?>" <?php if (($skinnyData['podstd'] == 'pluginAbsent') || ($skinnyData['podstd'] == 'pluginPresent')) { ?> style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='checkbox' name='podscustompost' id='podspost' value='podspost' <?php echo $skinnyData['podspost']; ?> style="display:none" onclick="podscustompostsetting(this.id, this.name);" <?php if (($skinnyData['podstd'] == 'pluginAbsent') || ($skinnyData['podstd'] == 'pluginPresent')) { ?> disabled  <?php } ?>>Enable</label>		
		              <label id="nopodscustompost" class="<?php echo $skinnyData['nopodspost']; ?>" <?php if (($skinnyData['podstd'] == 'pluginAbsent') || ($skinnyData['podstd'] == 'pluginPresent')) { ?> style="border-color: #ccc;color: #999;box-shadow: none;background-color:#e6e6e6;background-image:none;" <?php } ?>><input type='checkbox' name='podscustompost' style="display:none" onclick="podscustompostsetting(this.id, this.name);" <?php if (($skinnyData['podstd'] == 'pluginAbsent') || ($skinnyData['podstd'] == 'pluginPresent')) { ?> disabled <?php } ?>>Disable</label>
				</td></tr>
                                </tbody>
                                </table>
                        </div>
                </div>
                <!--div-3-->
                <div id="section3" class="Customfields" style="display:none;">
                        <div class="title">
                                        <h3>Custom Fields</h3>
                                        <span id="resetcustfield"><a id="resetopt" href="#" value="reset" name="resetcustfield" onclick="resetOption(this.id);">Reset Custom Field</a> </span>
                        </div>
                        <div id="data" class="databorder custom-fields" >
                                <table>
                                <tbody>
                                <tr><td>
                                <h3 id="innertitle">WP-Members for Users</h3>
                                <label>Enable to add import support WP-Members user fields.</label> </td><td>
                                <label id="wpusercheck" class="<?php echo $skinnyData['checkuser']; ?>" <?php if(($skinnyData['wpmemberstd'] == 'pluginAbsent') || ($skinnyData['wpmemberstd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='radio' name='rwpmembers' id='rwpmembers' value='wpmembers' <?php echo $skinnyData['wpmembers']; ?> style="display:none" onclick="wpmembersetting(this.id, this.name);" <?php if(($skinnyData['wpmemberstd'] == 'pluginAbsent') || ($skinnyData['wpmemberstd'] == 'pluginPresent')) { ?> disabled <?php } ?> ><span id="checkuser">Enable</span> </label>
                                <label id="wpuseruncheck" class="<?php echo $skinnyData['uncheckuser']; ?>" <?php if(($skinnyData['wpmemberstd'] == 'pluginAbsent') || ($skinnyData['wpmemberstd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;"<?php } ?>><input type='radio' name='rwpmembers' id="runcheckmember" style="display:none" onclick="wpmembersetting(this.id, this.name);" <?php if(($skinnyData['wpmemberstd'] == 'pluginAbsent') || ($skinnyData['wpmemberstd'] == 'pluginPresent')) { ?> disabled <?php } ?>>Disable </label>
                                </td></tr>
				<tr><td>
                                <h3 id="innertitle">WP e-Commerce Custom Fields </h3>
                                <label>Enable to add import support for WP e-Commerce custom fields.</label> </td><td>
                                <label id="ecomlabel" class="<?php echo $skinnyData['ecomfield']; ?>" <?php if(($skinnyData['wecftd'] == 'pluginAbsent') || ($skinnyData['wecftd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='radio' name='wpcustomfields' id='wpcustomfields' <?php #echo $skinnyData['wpcustomfields']; ?> value="wpcustomfields" style="display:none" onclick="wpfieldsetting(this.id, this.name);" <?php if(($skinnyData['wecftd'] == 'pluginAbsent') || ($skinnyData['wecftd'] == 'pluginPresent')) { ?>disabled <?php } ?>/>Enable</label>
				<label id="noecomlabel" class="<?php echo $skinnyData['noecomfield']; ?>" <?php if(($skinnyData['wecftd'] == 'pluginAbsent') || ($skinnyData['wecftd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;"<?php } ?>><input type='radio' name='wpcustomfields' style="display:none" onclick="wpfieldsetting(this.id, this.name);" <?php if(($skinnyData['wecftd'] == 'pluginAbsent') || ($skinnyData['wecftd'] == 'pluginPresent')) { ?>disabled <?php } ?>/>Disable</label>
                                </td></tr>
                                <tr><td>
                                <h3 id="innertitle">ACF Custom Fields</h3>
                                <label>Enable to add import support for ACF Custom Fields.</label> </br>
				</td><td>
                                <label id="acffieldlabel" class="<?php echo $skinnyData['acf']; ?>" <?php if(($skinnyData['acftd'] == 'pluginAbsent') || ($skinnyData['acftd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='checkbox' name='acfcustomfield' id='acfcustomfield' value='acf' <?php echo $skinnyData['acf']; ?> style="display:none" onclick="acfcustomfieldsetting(this.id, this.name);" <?php if(($skinnyData['acftd'] == 'pluginAbsent') || ($skinnyData['acftd'] == 'pluginPresent')) { ?>disabled<?php } ?>/>Enable</label>
				<label id="noacffieldlabel" class="<?php echo $skinnyData['noacf']; ?>" <?php if(($skinnyData['acftd'] == 'pluginAbsent') || ($skinnyData['acftd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;"<?php } ?>><input type='checkbox' name='acfcustomfield' style="display:none" onclick="acfcustomfieldsetting(this.id, this.name);" <?php if(($skinnyData['acftd'] == 'pluginAbsent') || ($skinnyData['acftd'] == 'pluginPresent')) { ?>disabled<?php } ?>/>Disable</label>
                                </td></tr>
                                <tr><td>
                                <h3 id="innertitle">CCTM Custom Fields</h3>
				<label>Enable to add import support for CCTM Custom Fields.</label> </td><td>
                                <label id="cctmfieldlabel" class="<?php echo $skinnyData['cctmcustfields']; ?>" <?php if(($skinnyData['cctmtd'] == 'pluginAbsent') || ($skinnyData['cctmtd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='checkbox' name='cctmcustomfield' id='cctmcustomfield' value='cctmcustfields' <?php echo $skinnyData['cctm']; ?> style="display:none" onclick="cctmcustomfieldsetting(this.id, this.name);" <?php if(($skinnyData['cctmtd'] == 'pluginAbsent') || ($skinnyData['cctmtd'] == 'pluginPresent')) { ?>disabled<?php } ?>/>Enable </label>
				<label id="nocctmfieldlabel" class="<?php echo $skinnyData['nocctmcustfields']; ?>" <?php if(($skinnyData['cctmtd'] == 'pluginAbsent') || ($skinnyData['cctmtd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;"<?php } ?>><input type='checkbox' name='cctmcustomfield' style="display:none" onclick="cctmcustomfieldsetting(this.id, this.name);" <?php if(($skinnyData['cctmtd'] == 'pluginAbsent') || ($skinnyData['cctmtd'] == 'pluginPresent')) { ?>disabled<?php } ?>/>Disable </label>
                                
                                </td></tr>
                                <tr><td>
                                <h3 id="innertitle">Types Custom Fields</h3>
                                <label>Enable to add import support for Types custom fields.</label> </td><td>
                                <label id="typesfieldlabel" class="<?php echo $skinnyData['wptypescustfields']; ?>" <?php if(($skinnyData['wptypestd'] == 'pluginAbsent') || ($skinnyData['wptypestd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='checkbox' name='typescustomfield' id='typescustomfield' value='wptypescustfields' <?php echo $skinnyData['wptypes']; ?> style="display:none" onclick="typescustomfieldsetting(this.id, this.name);" <?php if(($skinnyData['wptypestd'] == 'pluginAbsent') || ($skinnyData['wptypestd'] == 'pluginPresent')) { ?>disabled<?php } ?>/>Enable</label>
				<label id="notypesfieldlabel" class="<?php echo $skinnyData['nowptypescustfields']; ?>" <?php if(($skinnyData['wptypestd'] == 'pluginAbsent') || ($skinnyData['wptypestd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;"<?php } ?>><input type='checkbox' name='typescustomfield' style="display:none" onclick="typescustomfieldsetting(this.id, this.name);" <?php if(($skinnyData['wptypestd'] == 'pluginAbsent') || ($skinnyData['wptypestd'] == 'pluginPresent')) { ?>disabled<?php } ?>/>Disable</label>
                                
                                </td></tr>
                                <tr><td>
                                <h3 id="innertitle">PODS Custom Fields </h3>
                                <label>Enable to add import support for PODS custom fields.</label> </td><td>
				<label id="podsfieldlabel" class="<?php echo $skinnyData['podscustomfields']; ?>" <?php if(($skinnyData['podstd'] == 'pluginAbsent') || ($skinnyData['podstd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='checkbox' name='podscustomfield' id='podscustomfield' value='podscustomfields' <?php echo $skinnyData['podscustomfields']; ?> style="display:none" onclick="podscustomfieldsetting(this.id, this.name);" <?php if(($skinnyData['podstd'] == 'pluginAbsent') || ($skinnyData['podstd'] == 'pluginPresent')) { ?>disabled<?php } ?>/>Enable </span></label>
				<label id="nopodsfieldlabel" class="<?php echo $skinnyData['nopodscustomfields']; ?>" <?php if(($skinnyData['podstd'] == 'pluginAbsent') || ($skinnyData['podstd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;"<?php } ?>><input type='checkbox' name='podscustomfield' style="display:none" onclick="podscustomfieldsetting(this.id, this.name);" <?php if(($skinnyData['podstd'] == 'pluginAbsent') || ($skinnyData['podstd'] == 'pluginPresent')) { ?>disabled<?php } ?>/>Disable </span></label>
                                </td></tr>
                                </tbody>
                                </table>
                        </div>
                </div>
	<!--div-4 -->
                <div id="section4" class="ecommercesettings" style="display:none;">
                        <div class="title">
                        <h3>Ecommerce Settings</h3>
                        </div>
                        <div id="data" class="databorder" >
                                <table>
                                <tbody>
                                <tr><td>
                                <h3 id="innertitle">None</h3>
                                <label>Ecommerce import is disabled.</label> </td><td>
                                <label id="ecommercesetting1" class="<?php echo $skinnyData['nonerecommerce']; ?>"><input type='radio' id='ecommercenone' name='recommerce'  value='nonerecommerce' <?php echo $skinnyData['nonerecommerce']; ?>  class='ecommerce' onclick='ecommercesetting(this.id, this.name)' style="display:none" ><span id="ecommerce1text"> <?php echo $skinnyData['ecomnone_status']; ?> </span></label>
                                </td></tr>
                                <tr><td>
                                <h3 id="innertitle">Eshop </h3>
                                <label>Enable ecommerce import for Eshop.</label> </td><td>
                                <label id="ecommercesetting2" class="<?php echo $skinnyData['eshop']; ?>" <?php if(($skinnyData['eshoptd'] == 'pluginAbsent') || ($skinnyData['eshoptd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>> <input type='radio' id = 'eshop' name='recommerce' value='eshop' <?php echo $skinnyData['eshop']; ?> class='ecommerce'  onclick='ecommercesetting(this.id, this.name)' style="display:none" <?php if(($skinnyData['eshoptd'] == 'pluginAbsent') || ($skinnyData['eshoptd'] == 'pluginPresent')) { ?>disabled <?php } ?>><span id="ecommerce2text"><?php if(($skinnyData['eshoptd'] == 'pluginAbsent') || ($skinnyData['eshoptd'] == 'pluginPresent')) { ?>Disabled<?php } else { echo $skinnyData['eshop_status']; } ?></span></label>
                                </td></tr>
                                <tr><td>
                                <h3 id="innertitle">Marketpress Lite</h3>
                                <label>Enable ecommerce import for marketpress Lite.</label> </td><td>
				<label id="ecommercesetting3" class="<?php echo $skinnyData['marketpress']; ?>" <?php if(($skinnyData['marketpresslitetd'] == 'pluginAbsent') || ($skinnyData['marketpresslitetd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='radio' name='recommerce' id = 'marketpress' id='recommerce' value='marketpress' <?php echo $skinnyData['marketpress']; ?> class='marketpress' onclick='ecommercesetting(this.id, this.name)' style="display:none"<?php if(($skinnyData['marketpresslitetd'] == 'pluginAbsent') || ($skinnyData['marketpresslitetd'] == 'pluginPresent')) { ?>disabled <?php } ?>><span id="ecommerce3text"><?php if(($skinnyData['marketpresslitetd'] == 'pluginAbsent') || ($skinnyData['marketpresslitetd'] == 'pluginPresent')) { ?>Disabled<?php } else { echo $skinnyData['marketpress_status']; } ?></span></label>
                                </td></tr>
                                <tr><td>
                                <h3 id="innertitle">Woocommerce </h3>
                                <label>Enable ecommerce import for Woocommerce.</label> </td><td>
                                <label id="ecommercesetting4" class="<?php echo $skinnyData['woocommerce']; ?>" <?php if(($skinnyData['woocomtd'] == 'pluginAbsent') || ($skinnyData['woocomtd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='radio' name='recommerce' id = 'woocommerce' value='woocommerce' <?php echo $skinnyData['woocommerce']; ?> class='woocommerce' onclick='ecommercesetting(this.id, this.name)' style="display:none" <?php if(($skinnyData['woocomtd'] == 'pluginAbsent') || ($skinnyData['woocomtd'] == 'pluginPresent')) { ?>disabled <?php }?>><span id="ecommerce4text"><?php if(($skinnyData['woocomtd'] == 'pluginAbsent') || ($skinnyData['woocomtd'] == 'pluginPresent')) { ?>Disabled<?php } else { echo $skinnyData['woocommerce_status']; } ?></span></label>
                                </td></tr>
                                <tr><td>
				<h3 id="innertitle"> WP e-Commerce</h3>
                                <label>Enable ecommerce import for WP e-Commerce.</label> </td><td>
                                <label id="ecommercesetting5" class="<?php echo $skinnyData['wpcommerce']; ?>" <?php if(($skinnyData['wpcomtd'] == 'pluginAbsent') || ($skinnyData['wpcomtd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='radio' name='recommerce' id = 'wpcommerce' value='wpcommerce' <?php echo $skinnyData['wpcommerce']; ?> class='ecommerce' onclick='ecommercesetting(this.id, this.name)' style="display:none" <?php if(($skinnyData['wpcomtd'] == 'pluginAbsent') || ($skinnyData['wpcomtd'] == 'pluginPresent')) { ?>disabled <?php }?>><span id="ecommerce5text"><?php if(($skinnyData['wpcomtd'] == 'pluginAbsent') || ($skinnyData['wpcomtd'] == 'pluginPresent')) { ?>Disabled<?php } else { echo $skinnyData['wpcommerce_status']; } ?></span></label>
                                
				</td></tr>
                                </tbody>
                                </table>
                        </div>
                </div>
		<!--div-5-->
                <div id="section5" class="seosettings" style="display:none;">
                        <div class="title">
                                <h3>SEO Settings</h3>
                        </div>
                        <div id="data" class="databorder" >
                                <table>
                                <tbody>
                                <tr><td>
                                 <h3 id="innertitle">None</h3>
                                <label>SEO Meta import is disabled.</label> </td><td>
                                <label id="seosetting1" class="<?php echo $skinnyData['nonerseooption']; ?> " ><input type='radio' name='rseooption'id="none" value='nonerseooption' <?php echo $skinnyData['nonerseooption']; ?> style="display:none" class='ecommerce' onclick="seosetting(this.id,this.name);" ><span id="seosetting1text"> <?php echo $skinnyData['none_status']; ?> </span></label>
                                </td></tr>
				<tr><td>
                                <h3 id="innertitle">All-in-one SEO </h3>
                                <label>Enable All-in-one SEO import.</label> </td><td>
                                <label id="seosetting2" class="<?php echo $skinnyData['aioseo']; ?>" <?php if(($skinnyData['aioseotd'] == 'pluginAbsent') || ($skinnyData['aioseotd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='radio' name='rseooption' id="aioseo" value='aioseo' <?php echo $skinnyData['aioseo']; ?> style="display:none" onclick="seosetting(this.id,this.name);" <?php if(($skinnyData['aioseotd'] == 'pluginAbsent') || ($skinnyData['aioseotd'] == 'pluginPresent')) { ?>disabled <?php }?>><span id="seosetting2text"> <?php echo $skinnyData['aioseo_status']; ?> </span></label> 
                                </td></tr>
                                <tr><td>
                                <h3 id="innertitle"> Yoast SEO</h3>
                                <label>Enable Wordpress SEO by  Yoast support.</label> </td><td>
<label id="seosetting3" class="<?php echo $skinnyData['yoastseo']; ?>" <?php if(($skinnyData['yoasttd'] == 'pluginAbsent') || ($skinnyData['yoasttd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='radio' name='rseooption' id="yoastseo" value='yoastseo' <?php echo $skinnyData['yoastseo']; ?> style="display:none" onclick="seosetting(this.id,this.name);" <?php if(($skinnyData['yoasttd'] == 'pluginAbsent') || ($skinnyData['yoasttd'] == 'pluginPresent')) { ?>disabled<?php }?>><span id="seosetting3text"><?php echo $skinnyData['yoastseo_status']; ?> </span></label>

                                </td></tr>
                                </tbody>
                                </table>
                        </div>
                </div>

		 <!--div-6-->
                <div id="section6" class="additionalfeatures" style="display:none;">
                        <div class="title">
                                <h3>Additional Features</h3>
                        </div>
                        <div id="data">
                                <table class="enablefeatures">
                                <tbody>
				<tr class="databorder"><td>
				<h3 id="innertitle">Debug Mode</h3>
				<label>You can enable/disable the debug mode.</label> </td><td>
				<label id="debugmode_enable" class="<?php echo $skinnyData['debugmode_enable']; ?>"><input type='radio' name='debug_mode' value='enable_debug' <?php echo $skinnyData['debugmode_enable']; ?> id="enabled" style="display:none" onclick="debugmode_check(this.id, this.name);" > On </label>
				<label id="debugmode_disable" class="<?php echo $skinnyData['debugmode_disable']; ?>"><input type='radio' name='debug_mode' value='disable_debug' <?php echo $skinnyData['debugmode_disable']; ?> id="disabled" style="display:none" onclick="debugmode_check(this.id, this.name);" > Off </label>
				</td></tr>
				<tr class="databorder"><td>
				<h3 id="innertitle">Scheduled log mails</h3>
                                <label>Enable to get scheduled log mails.</label> </td><td>
                                <label id="schedulecheck" class="<?php echo $skinnyData['schedulelog']; ?>"><input type='radio' name='send_log_email' value='send_log_email' <?php echo $skinnyData['send_log_email']; ?> id="scheduled" style="display:none" onclick="schedulesetting(this.id, this.name);" > Yes </label>
                                <label id="scheduleuncheck" class="<?php echo $skinnyData['schedulenolog']; ?>"><input type='radio' name='send_log_email' id="noscheduled" style="display:none" onclick="schedulesetting(this.id, this.name);" > No </label>
                                </td></tr>
                                <tr><td>
                                <h3 id="innertitle">Drop Table</h3>
                                <label>If enabled plugin deactivation will remove plugin data, this cannot be restored.</label></td>
				<td><label id="dropon" class="<?php echo $skinnyData['drop_on'] ; ?>" ><input type='radio' name='drop_table' id='drop_table' value='on' <?php echo $skinnyData['dropon_status']; ?> style="display:none" onclick="dropsetting(this.id, this.name);" > On </label>
                                <label id="dropoff" class="<?php echo $skinnyData['drop_off'] ; ?>" ><input type='radio' name='drop_table' id='drop_tab' value='off' <?php echo $skinnyData['dropoff_status']; ?> style="display:none" onclick="dropsetting(this.id, this.name);" > Off</label>
                                </td></tr>
                                <tr><td>
                                <h3 id="innertitle" >Category Icons</h3>
                                <label>Enable to import category icons for category.</label>
                                </td>
                                <td>
				<label id="catenable" class="<?php echo $skinnyData['catyenable'] ; ?>"  <?php if(($skinnyData['cateicontd'] == 'pluginAbsent') || ($skinnyData['cateicontd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?>><input type='radio'  name='rcateicons' id='caticonenable' value='enable' <?php echo $skinnyData['catyenablestatus']; ?> style="display:none" onclick="categoryiconsetting(this.id, this.name);" style="display:none" <?php if(($skinnyData['cateicontd'] == 'pluginAbsent') || ($skinnyData['cateicontd'] == 'pluginPresent')) { ?> disabled <?php } ?>>Enable</label>
                                <label id="catdisable" class="<?php echo $skinnyData['catydisable']; ?>" <?php if(($skinnyData['cateicontd'] == 'pluginAbsent') || ($skinnyData['cateicontd'] == 'pluginPresent')) { ?>style="background-color:#e6e6e6;border-color: #ccc;color: #999;box-shadow: none;background-image:none;" title="Enable the plugin to activate."<?php } ?> ><input type='radio' name='rcateicons' id='caticondisable' value='disable' <?php echo $skinnyData['catydisablestatus']; ?>  style="display:none" onclick="categoryiconsetting(this.id, this.name);" style="display:none" <?php if(($skinnyData['cateicontd'] == 'pluginAbsent') || ($skinnyData['cateicontd'] == 'pluginPresent')) { ?> disabled <?php } ?>>Disable</label>                               
                                </td>
                                </tr>
				<tr><td>
                                <h3 id="innertitle" >Woocommerce Custom attribute</h3>
                                <label>Enables to register woocommrce custom attribute.</label>
                                </td>
                                <td>
                                <label id="onwoocomreg" class="<?php echo $skinnyData['onwoocomreg'] ; ?>"><input type='radio'  name='woocomattr' id='woocomattr' value='on' <?php echo $skinnyData['onwoocomreg_status']; ?> onclick="woocomregattr(this.id, this.name);" style="display:none" >On</label>
                                <label id="offwoocomreg" class="<?php echo $skinnyData['offwoocomreg'] ; ?>"><input type='radio' name='woocomattr' id='disablewoocomattr' value='off' <?php echo $skinnyData['offwoocomreg_status']; ?>  onclick="woocomregattr(this.id, this.name);" style="display:none">Off</label>
                                </td></tr>
                                </tbody>
                                </table>
                        </div>
                </div>
		<!--div-7-->
                <div id="section7" class="databaseoptimization" style="display:none;">
                        <div class="title">
                                <h3>Database Optimization </h3>
                                <span style="float:right;margin-right:168px;margin-top:-35px;">
                                        <a id="checkOpt" onclick="selectOptimizer(this.id);" href="#"> Check All </a>
                                </span>
                                <span style="float:right;margin-right:81px;margin-top:-35px;">
                                        <a id="uncheckOpt" onclick="selectOptimizer(this.id);" href="#"> / Uncheck All </a>
                                </span>
                        </div>
                        <div id="data" class="database">
                        <table class="databaseoptimization">
                        <tbody>
                        <tr>
                        <td>
			<?php
			if(isset($skinnyData['delete_all_orphaned_post_page_meta']))
			{
				$skinny_delete_all_post_page = $skinnyData['delete_all_orphaned_post_page_meta'];
			}
			else
			{
				$skinny_delete_all_post_page = '';
			}
			if(isset($skinnyData['delete_all_unassigned_tags']))
                        {
                                $skinny_delete_all_unassigned_tag = $skinnyData['delete_all_unassigned_tags'];
                        }
                        else
                        {
                                $skinny_delete_all_unassigned_tag = '';
                        }
			if(isset($skinnyData['delete_all_post_page_revisions']))
                        {
                                $skinny_delete_all_page_revisions = $skinnyData['delete_all_post_page_revisions'];
                        }
                        else
                        {
                                $skinny_delete_all_page_revisions = '';
                        }
			if(isset($skinnyData['delete_all_auto_draft_post_page']))
                        {
                                $skinny_delete_all_auto_draft_page = $skinnyData['delete_all_auto_draft_post_page'];
                        }
                        else
                        {
                                $skinny_delete_all_auto_draft_page = '';
                        }
			if(isset($skinnyData['delete_all_post_page_in_trash']))
                        {
                                $skinny_delete_all_post_page_trash = $skinnyData['delete_all_post_page_in_trash'];
                        }
                        else
                        {
                                $skinny_delete_all_post_page_trash = '';
                        }

			if(isset($skinnyData['delete_all_spam_comments']))
                        {
                                $skinny_delete_all_spam_comments = $skinnyData['delete_all_spam_comments'];
                        }
                        else
                        {
                                $skinny_delete_all_spam_comments = '';
                        }
			if(isset($skinnyData['delete_all_comments_in_trash']))
                        {
                                $skinny_delete_all_comments_trash = $skinnyData['delete_all_comments_in_trash'];
                        }
                        else
                        {
                                $skinny_delete_all_comments_trash = '';
                        }
			if(isset($skinnyData['delete_all_unapproved_comments']))
                        {
                                $skinny_delete_all_unapproved_comments = $skinnyData['delete_all_unapproved_comments'];
                        }
                        else
                        {
                                $skinny_delete_all_unapproved_comments = '';
                        }
			if(isset($skinnyData['delete_all_pingback_commments']))
                        {
                                $skinny_delete_all_pingback_comments = $skinnyData['delete_all_pingback_commments'];
                        }
                        else
                        {
                                $skinny_delete_all_pingback_comments = '';
                        }
			if(isset($skinnyData['delete_all_trackback_comments']))
                        {
                                $skinny_delete_all_trackback_comments = $skinnyData['delete_all_trackback_comments'];
                        }
                        else
                        {
                                $skinny_delete_all_trackback_comments = '';
                        }


			?>
			<label id="dblabel"><input type='checkbox' name='delete_all_orphaned_post_page_meta' id='delete_all_orphaned_post_page_meta' value='delete_all_orphaned_post_page_meta' <?php echo $skinny_delete_all_post_page; ?> onclick='database_optimization_settings(this.id);' /><span id="align"> Delete all orphaned Post/Page Meta</span></label>
                        </td>
                        <td>
			<label id="dblabel"><input type='checkbox' name='delete_all_unassigned_tags' id='delete_all_unassigned_tags' value='delete_all_unassigned_tags' <?php echo $skinny_delete_all_unassigned_tag; ?> onclick='database_optimization_settings(this.id);' /><span id="align"> Delete all unassigned tags</span></label>
                        </td>
                        </tr>
                        <tr>
                        <td>
			<label id="dblabel"><input type='checkbox' name='delete_all_post_page_revisions' id='delete_all_post_page_revisions' value='delete_all_post_page_revisions' <?php echo $skinny_delete_all_page_revisions; ?> onclick='database_optimization_settings(this.id);'  /><span id="align"> Delete all Post/Page revisions</span></label>
                        </td>
                        <td>
			<label id="dblabel"><input type='checkbox' name='delete_all_auto_draft_post_page' id='delete_all_auto_draft_post_page' value='delete_all_auto_draft_post_page' <?php echo $skinny_delete_all_auto_draft_page; ?> onclick='database_optimization_settings(this.id);' /><span id="align"> Delete all auto drafted Post/Page</span></label>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <label id="dblabel"><input type='checkbox' name='delete_all_post_page_in_trash' id='delete_all_post_page_in_trash' value='delete_all_post_page_in_trash' <?php echo $skinny_delete_all_post_page_trash; ?> onclick='database_optimization_settings(this.id);' /><span id="align"> Delete all Post/Page in trash</span></label>
                        </td>
                        <td>
			<label id="dblabel"><input type='checkbox' name='delete_all_spam_comments' id='delete_all_spam_comments' value='delete_all_spam_comments' <?php echo $skinny_delete_all_spam_comments; ?> onclick='database_optimization_settings(this.id);' /><span id="align"> Delete all Spam Comments</span></label>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <label id="dblabel"><input type='checkbox' name='delete_all_comments_in_trash' id='delete_all_comments_in_trash' value='delete_all_comments_in_trash'  <?php echo $skinny_delete_all_comments_trash; ?> onclick='database_optimization_settings(this.id);'  /><span id="align"> Delete all Comments in trash</span></label>
                        </td>
                        <td>
                        <label id="dblabel"><input type='checkbox' name='delete_all_unapproved_comments' id='delete_all_unapproved_comments' value='delete_all_unapproved_comments'  <?php echo $skinny_delete_all_unapproved_comments; ?> onclick='database_optimization_settings(this.id);' /><span id="align"> Delete all Unapproved Comments</span></label>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <label id="dblabel"><input type='checkbox' name='delete_all_pingback_commments' id='delete_all_pingback_commments' value='delete_all_pingback_commments'  <?php echo $skinny_delete_all_pingback_comments; ?> onclick='database_optimization_settings(this.id);' /><span id="align"> Delete all Pingback Comments</span></label>
                        </td>
                        <td>
			<label id="dblabel"><input type='checkbox' name='delete_all_trackback_comments' id='delete_all_trackback_comments' value='delete_all_trackback_comments'  <?php echo $skinny_delete_all_trackback_comments; ?> onclick='database_optimization_settings(this.id);' /> <span id="align"> Delete all Trackback Comments</span></label>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                                <div style="float:right;padding:17px;margin-top:-2px;">
                                        <input id="database_optimization" class="action btn btn-warning" type="button" onclick="databaseoptimization();" value="Run DB Optimizer" name="database_optimization">
                                </div>
                                <div id="optimizelog" style="margin-top:35px;display:none;">
                                        <h4>Database Optimization Log</h4>
                                        <div id="optimizationlog" class="optimizerlog">
                                                <div id="log" class="log">
                                                        <p style="margin:15px;color:red;" id="align">NO LOGS YET NOW.</p>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
		<!--div-8-->
                <div id="section8" class="securityperformance" style="display:none;">
                        <div class="title">
                                <h3>Security and Performance</h3>
                        </div>
                        <div id="data" class="databorder security-perfoemance" >
                        <table class="securityfeatures">
                        <tr><td>
                        <h3 id="innertitle">Allow authors/editors to import</h3>
                        <label>This enables authors/editors to import.</label></td><td>
                        <label id="allowimport" class="<?php echo $skinnyData['authorimport']; ?>" ><input type='radio' name='enable_plugin_access_for_author' id="enableimport" class="importauthor" value='enable_plugin_access_for_author' <?php echo $skinnyData['enable_plugin_access_for_author']; ?> style="display:none" onclick="authorimportsetting(this.id, this.name);"  />Check</label>
                        <label id="donallowimport" class="<?php echo $skinnyData['noauthorimport']; ?>" > <input type='radio' name='enable_plugin_access_for_author' class="importauthor" style="display:none" onclick="authorimportsetting(this.id, this.name);" >Uncheck</label>
                        </td></tr>
                        </table>
                        <table class="table table-striped">
                        <tr><th colspan="3" >
                        <h3 id="innertitle">Minimum required php.ini values (Ini configured values)</h3>
                        </th></tr>
                        <tr><th>
                        <label>Variables</label>
                        </th><th class='ini-configured-values'>
                        <label>System values</label>
                        </th><th class='min-requirement-values'>
                        <label>Minimum Requirements</label>
                        </th></tr>
                        <tr><td>post_max_size </td><td class='ini-configured-values'><?php echo ini_get('post_max_size') ?></td><td class='min-requirement-values'>10M</td></tr>
                        <tr><td>auto_append_file</td><td class='ini-configured-values'>- <?php echo ini_get('auto_append_file') ?></td><td class='min-requirement-values'>-</td></tr>
                        <tr><td>auto_prepend_file </td><td class='ini-configured-values'>- <?php echo ini_get('auto_prepend_file') ?></td><td class='min-requirement-values'>-</td></tr>
                        <tr><td>upload_max_filesize </td><td class='ini-configured-values'><?php echo ini_get('upload_max_filesize') ?></td><td class='min-requirement-values'>2M</td></tr>
                        <tr><td>file_uploads </td><td class='ini-configured-values'><?php echo ini_get('file_uploads') ?></td><td class='min-requirement-values'>1</td></tr>
			<tr><td>allow_url_fopen </td><td class='ini-configured-values'><?php echo ini_get('allow_url_fopen') ?></td><td class='min-requirement-values'>1</td></tr>
                        <tr><td>max_execution_time </td><td class='ini-configured-values'><?php echo ini_get('max_execution_time') ?></td><td class='min-requirement-values'>3000</td></tr>
                        <tr><td>max_input_time </td><td class='ini-configured-values'><?php echo ini_get('max_input_time') ?></td><td class='min-requirement-values'>3000</td></tr>
                        <tr><td>max_input_vars </td><td class='ini-configured-values'><?php echo ini_get('max_input_vars') ?></td><td class='min-requirement-values'>3000</td></tr>
                        <tr><td>memory_limit </td><td class='ini-configured-values'><?php echo ini_get('memory_limit') ?></td><td class='min-requirement-values'>99M</td></tr>
                        </table>
                        <h3 id="innertitle" colspan="2" >Required to enable/disable Loaders, Extentions and modules:</h3>
                        <table class="table table-striped">
                        <?php $loaders_extensions = get_loaded_extensions();?>
			<?php if(function_exists('apache_get_modules')){
					$mod_security = apache_get_modules(); 
			}?>	
                        <tr><td>IonCube Loader </td><td><?php if(in_array('ionCube Loader', $loaders_extensions)) {
                                        echo '<label style="color:green;">Yes</label>';
                                } else {
                                        echo '<label style="color:red;">No</label>';
                                } ?> </td><td></td></tr>
                        <tr><td>PDO </td><td><?php if(in_array('PDO', $loaders_extensions)) {
                                        echo '<label style="color:green;">Yes</label>';
                                } else {
                                        echo '<label style="color:red;">No</label>';
                                } ?></td><td></td></tr>
                        <tr><td>Curl </td><td><?php if(in_array('curl', $loaders_extensions)) {
                                        echo '<label style="color:green;">Yes</label>';
                                } else {
                                        echo '<label style="color:red;">No</label>';
                                } ?></td><td></td></tr>
			<tr><td>Mod Security </td><td><?php if(isset($mod_security) && in_array('mod_security.c', $mod_security)) {
                                        echo '<label style="color:green;">Yes</label>';
                                } else {
                                        echo '<label style="color:red;">No</label>';
                                } ?></td><td>
					<div style='float:left'>
						<a href="#" class="tooltip">
							<img src="<?php echo WP_CONST_ULTIMATE_CSV_IMP_DIR; ?>images/help.png" style="margin-left:-74px;"/>
							<span style="margin-left:20px;margin-top:-10px;width:150px;">
								<img class="callout" src="<?php echo WP_CONST_ULTIMATE_CSV_IMP_DIR; ?>images/callout.gif"/>
								<strong>htaccess settings:</strong>
								<p>Locate the .htaccess file in Apache web root,if not create a new file named .htaccess and add the following:</p>
<b><?php echo '<IfModule mod_security.c>';?> SecFilterEngine Off SecFilterScanPOST Off <?php echo ' </IfModule>';?></b>

							</span>
						</a>
					</div>
				    </td></tr>
                        </table>
                        <h3 id="innertitle" colspan="2" >Debug Information:</h3>
                        <table class="table table-striped">
                        <tr><td class='debug-info-name'>WordPress Version</td><td><?php echo $wp_version; ?></td><td></td></tr>
                        <tr><td class='debug-info-name'>PHP Version</td><td><?php echo phpversion(); ?></td><td></td></tr>
                        <tr><td class='debug-info-name'>MySQL Version</td><td><?php echo $wpdb->db_version(); ?></td><td></td></tr>
                        <tr><td class='debug-info-name'>Server SoftWare</td><td><?php echo $_SERVER[ 'SERVER_SOFTWARE' ]; ?></td><td></td></tr>			       <tr><td class='debug-info-name'>Your User Agent</td><td><?php echo $_SERVER['HTTP_USER_AGENT']; ?></td><td></td></tr>
                        <tr><td class='debug-info-name'>WPDB Prefix</td><td><?php echo $wpdb->prefix; ?></td><td></td></tr>
                        <tr><td class='debug-info-name'>WP Multisite Mode</td><td><?php if ( is_multisite() ) { echo '<label style="color:green;">Enabled</label>'; } else { echo '<label style="color:red;">Disabled</label>'; } ?> </td><td></td></tr>
                        <tr><td class='debug-info-name'>WP Memory Limit</td><td><?php echo (int) ini_get('memory_limit'); ?></td><td></td></tr>
                        </table>
                        </div>
                </div>
		<div id="section9" class="documentation" style="display:none;">
                        <div class="title">
                                <h3>Documentation</h3>
                        </div>
                        <div id="data">
                                <div id="video">
                                        <iframe width="560" height="315" src="//www.youtube.com/embed/FhTUXE5zk0o?list=PL2k3Ck1bFtbRli9VdJaqwtzTSzzkOrH4j" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <div id="relatedpages">
                                        <h2 id="doctitle">Smackcoders Guidelines </h2 >
					<p> <a href="https://www.smackcoders.com/blog/category/web-development-news/" target="_blank"> Development News </a> </p>
					<p> <a href="http://www.wpultimatecsvimporter.com/" target="_blank"> Whats New? </a> </p>
					<p> <a href="http://wiki.smackcoders.com/WP_Ultimate_CSV_Importer_Pro" target="_blank"> Documentation </a> </p>
					<p> <a href="https://www.smackcoders.com/blog/csv-importer-a-simple-and-easy-csv-importer-tutorial.html" target="_blank"> Tutorials </a> </p>
					<p> <a href="http://www.youtube.com/user/smackcoders/channels" target="_blank"> Youtube Channel </a> </p>
					<p> <a href="https://www.smackcoders.com/store/products-46/wordpress.html" target="_blank"> Other Plugins </a> </p>
				</div>
                        </div>
                </div>
<!--conbar-->
	 </div>
</div>
</form>
</div>





