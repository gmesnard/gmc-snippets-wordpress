<?php
/*
 * Plugin Name: ROOTS Taxonomy Search 
 * Plugin URI: http://demotms.wp-roots.com/
 * Description: Create search forms for custom post types.
 * Version: 1.0
 * Author: ROOTS 
 * Author URI: http://www.wp-roots.com/
 */

/*
 * create the tables
 */

register_activation_hook ( __FILE__, 'create_rots_tax_tbl' );
function create_rots_tax_tbl() {
	global $wpdb;
	require_once (ABSPATH . 'wp-admin/includes/upgrade.php');	
	/*
	 * the forms
	 */
	$table_name = $wpdb->prefix . "its_forms";
	if ($wpdb->get_var ( "show tables like '$table_name'" ) != $table_name) {		$sql = "CREATE TABLE IF NOT EXISTS `$table_name` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`name` varchar(255) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=latin1;";		dbDelta ( $sql );
	}
	
	/*
	 * the meta
	 */
	$table_name = $wpdb->prefix . "its_formmeta";
	if ($wpdb->get_var ( "show tables like '$table_name'" ) != $table_name) {
		$sql = "CREATE TABLE IF NOT EXISTS `$table_name` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `form_id` int(11) NOT NULL,
			  `meta_name` varchar(255) NOT NULL,
			  `meta_value` varchar(255) NOT NULL,
			  `meta_row` int(11) NOT NULL,
			  `meta_col` int(11) NOT NULL,
			  `meta_position` int(11) NOT NULL,
			  PRIMARY KEY (`id`)
			) ENGINE=InnoDB  DEFAULT CHARSET=latin1;";
		dbDelta ( $sql );
	}
	
}

function get_user_role($uid) {
	global $wpdb;
	$role = $wpdb->get_var("SELECT meta_value FROM {$wpdb->usermeta} WHERE meta_key = 'wp_capabilities' AND user_id = {$uid}");
	if(!$role) return 'non-user';
	$rarr = unserialize($role);
	$roles = is_array($rarr) ? array_keys($rarr) : array('non-user');
	return $roles[0];
}

function add_capability() {
	$current_user = get_current_user_id();
	$role = get_user_role($current_user);
	if ($role != 'non-user') {
		$role = get_role( $role );	
		$role->add_cap( 'rots_submenu' );
	}
	
}
add_action( 'admin_init', 'add_capability');

add_action ( 'admin_menu', 'i_tax_search_menu' );
function i_tax_search_menu() {
	$icon_menu = plugin_dir_url ( __FILE__ ) . 'files/img/r_menu.jpg';
	add_menu_page ( 'Taxonomy Search', 'Taxonomy Search', 'nosuchcapability', 'i_tax_search_plugin', '', $icon_menu );
	add_submenu_page ( 'i_tax_search_plugin', 'Manage', 'Manage', 'rots_submenu', 'i_tax_search_manage', 'i_tax_search_manage' );
	add_submenu_page ( 'i_tax_search_plugin', 'Add New', 'Add New', 'rots_submenu', 'i_tax_search_add_new', 'i_tax_search_add_new' );
}
// MANAGE
function i_tax_search_manage() {
	$dir = plugin_dir_path ( __FILE__ );
	include ($dir . 'includes/manage.php');
}
// ADD/Edit
function i_tax_search_add_new() {
	$dir = plugin_dir_path ( __FILE__ );
	wp_enqueue_script ( 'its_functions', plugins_url ( 'rots-taxonomy-search/files/js/functions.js' ), array (), null );
	wp_localize_script ( 'its_functions', 'ajaxobject', array ('ajaxurl' => admin_url ( 'admin-ajax.php' ) ) );
	wp_enqueue_style ( 'theme1', 'files/css/admin_themes/theme1.css', array (), null );
	wp_enqueue_style ( 'theme2', 'files/css/admin_themes/theme2.css', array (), null );
	wp_enqueue_style ( 'theme3', 'files/css/admin_themes/theme3.css', array (), null );
	wp_enqueue_style ( 'theme4', 'files/css/admin_themes/theme4.css', array (), null );
	wp_enqueue_style ( 'theme5', 'files/css/admin_themes/theme5.css', array (), null );
	wp_enqueue_style ( 'theme6', 'files/css/admin_themes/theme6.css', array (), null );
	wp_enqueue_style ( 'theme7', 'files/css/admin_themes/theme7.css', array (), null );
	wp_enqueue_style ( 'theme8', 'files/css/admin_themes/theme8.css', array (), null );
	include ($dir . 'includes/add_edit.php');
}
class ROTSTaxonomySearch extends WP_Widget {
	function ROTSTaxonomySearch() {
		$name = 'ROTS Taxonomy Search';
		parent::__construct ( false, $name );
	}
	function widget($args, $instance) {
		if (isset ( $instance ['form_id'] ) && $instance ['form_id'] != '')
			echo do_shortcode ( "[rots_taxonomy_search id={$instance['form_id']}]" );
	}
	function update($new_instance, $old_instance) {
		if (! get_option ( 'i_tax_search_form' ))
			add_option ( 'i_tax_search_form', $new_instance ['form_id'] );
		else
			update_option ( 'i_tax_search_form', $new_instance ['form_id'] );
		$instance = $old_instance;
		$instance ['form_id'] = $new_instance ['form_id'];
		return $instance;
	}
	function form($instance) {
		$dir = plugin_dir_path ( __FILE__ );
		include ($dir . 'includes/widget_form.php');
	}
}
function regist_ROTSTaxonomySearch() {
	register_widget ( 'ROTSTaxonomySearch' );
}
add_action ( 'widgets_init', 'regist_ROTSTaxonomySearch' );

add_action('wp_enqueue_scripts', 'roots_front_end_scripts');
function roots_front_end_scripts(){

}


add_action ( "admin_enqueue_scripts", 'i_tax_search_header' );
function i_tax_search_header() {
	$plugin_pages = array ('i_tax_search_add_new', 'i_tax_search_manage' );
	$dir_path = plugin_dir_url ( __FILE__ );
	if (isset ($_GET ['page']) && in_array ( $_GET ['page'], $plugin_pages )) {
		// styles
		wp_enqueue_style ( 'style', $dir_path . 'files/css/style.css', array (), null );
		wp_enqueue_style ( 'general_style', $dir_path . 'files/css/general_style.css', array (), null );
		wp_enqueue_style ( 'bootstrap', $dir_path . 'files/css/bootstrap.css', array (), null );
		wp_enqueue_style ( 'jquery-ui', $dir_path . 'files/css/jquery-ui.css', array (), null );
		wp_enqueue_style ( 'jquery.ui.theme', $dir_path . 'files/css/jquery.ui.theme.css', array (), null );
		// scripts
		wp_enqueue_script ( 'jquery', $dir_path . 'files/js/jquery.min.js', array (), null );
		wp_enqueue_script ( 'jquery.ui', $dir_path . 'files/js/jquery-ui.js', array (), null );
		wp_enqueue_script ( 'jquery.ui.widget.min', $dir_path . 'files/js/jquery.ui.widget.min.js', array (), null );
		wp_enqueue_script ( 'jquery.ui.mouse.min', $dir_path . 'files/js/jquery.ui.mouse.min.js', array (), null );
		wp_enqueue_script ( 'jquery.ui.slider', $dir_path . 'files/js/jquery.ui.slider.js', array (), null );
	}
}
function addMyScript() {
	$dir_path = plugin_dir_url ( __FILE__ );
	wp_enqueue_style ( 'general_style', $dir_path . 'files/css/general_style.css', array (), null );
}
add_action ( 'wp_head', 'addMyScript' );
add_shortcode ( 'rots_taxonomy_search', 'i_tax_search_shortcode' );
function i_tax_search_shortcode($id) { 
	$dir_path = plugin_dir_url ( __FILE__ );
	wp_enqueue_style ( 'style', $dir_path . 'files/css/style.css', array (), null ); 
	wp_enqueue_style ( 'jquery-ui', $dir_path . 'files/css/jquery-ui.css', array (), null );
	wp_enqueue_style ( 'general_style', $dir_path . 'files/css/general_style.css', array (), null );
	wp_enqueue_style ( 'theme1', $dir_path . 'files/css/themes/theme1.css', array (), null );
	wp_enqueue_style ( 'theme2', $dir_path . 'files/css/themes/theme2.css', array (), null );
	wp_enqueue_style ( 'theme3', $dir_path . 'files/css/themes/theme3.css', array (), null );
	wp_enqueue_style ( 'theme4', $dir_path . 'files/css/themes/theme4.css', array (), null );
	wp_enqueue_style ( 'theme5', $dir_path . 'files/css/themes/theme5.css', array (), null );
	wp_enqueue_style ( 'theme6', $dir_path . 'files/css/themes/theme6.css', array (), null );
	wp_enqueue_style ( 'theme7', $dir_path . 'files/css/themes/theme7.css', array (), null );
	wp_enqueue_style ( 'theme8', $dir_path . 'files/css/themes/theme8.css', array (), null );
	
	wp_enqueue_script ( 'jquery' );
	wp_enqueue_script ( 'jquery-min', $dir_path . 'files/js/jquery.min.js', array (), null );
	wp_enqueue_script ( 'jquery.ui.widget.min', $dir_path . 'files/js/jquery.ui.widget.min.js', array (), null );
	wp_enqueue_script ( 'jquery.ui.mouse.min', $dir_path . 'files/js/jquery.ui.mouse.min.js', array (), null );
	wp_enqueue_script ( 'jquery.ui.slider', $dir_path . 'files/js/jquery.ui.slider.js', array (), null );
	wp_enqueue_script ( 'functions', $dir_path . 'files/js/functions.js', array (), null );
	
	wp_enqueue_script ( 'its_functions', plugins_url ( 'rots-taxonomy-search/files/js/functions.js' ), array (), null );
	wp_localize_script ( 'its_functions', 'ajaxobject', array ('ajaxurl' => admin_url ( 'admin-ajax.php' ) ) );
	$dir = $dir_path = plugin_dir_path ( __FILE__ );
	$return_str = true;
	include ($dir . 'includes/shortcode.php');
	return $final_str;
}
/*
 * add function to change the search query
 */
function exclude_category($query) {
	if (! is_admin () && $query->is_main_query () && isset($_REQUEST ['its_search']) && $_REQUEST ['its_search']) {
		/*
		 * set the post type
		 */
		$search_template = urldecode($_REQUEST['search_template']);
		$tmpl_array = array('is_search', 'is_archive', 'is_category', 'is_tag', 'is_tax', 'is_post_type_archive', 'is_home');

		$query->is_search = $query->is_archive = $query->is_category =$query->is_tag =$query->is_tax =$query->is_post_type_archive =$query->is_home = ''; 
		
		switch($search_template) {
			case 'is_search': 
				$query->is_search = 1;
				break; 
			case 'is_archive': 
				$query->is_archive = 1;
				break;  
			case 'is_category':
				$query->is_category = 1;
				break;  
			case 'is_tag': 
				$query->is_tag = 1;
				break;  
			case 'is_tax':
				$query->is_tax = 1;
				break;  
			case 'is_post_type_archive':
				$query->is_post_type_archive = 1;
				break;  
			case 'is_home':
				$query->is_home = 1;
				break; 
		}
		
		
		
		
		$query->set ( 'post_type', $_REQUEST ['post_type'] );
		/*
		 * brouse through taxonomies
		 */
		$tax = array();
		foreach ( $_REQUEST as $k => $v ) {
			if (strpos ( $k, 'tx_' ) === 0) {
				if (strpos($k, '[]')) {
					foreach($v as $key=>$value) {
						$tax [substr($k, 0, -2)] = $v;
					}
				} else {
					if ($v != - 1)
						$tax [$k] = $v;
				}
			}
		}
		$taxquery = array();
		if (sizeof ( $tax )) {
			foreach ( $tax as $k => $v ) {
				if (is_array($v)) {
						$taxquery[] = array(
								array(
										'taxonomy' => substr ( $k, 3 ),
										'field' => 'slug ',
										'terms' => $v,
										'operator' => 'IN'
										)
								);
				} else{
					$query->set ( substr ( $k, 3 ), $v );
				}
			}
		}
		
		$query->set('tax_query', $taxquery);
		/*
		 * brouse though custom fields
		 */
		$i = 0;
		$meta_array = array ();
		$meta_array ['relation'] = $_REQUEST ["logical_cond"] = 'and';
		foreach ( $_REQUEST as $k => $v ) {
			if (strpos ( $k, 'cf_' ) === 0) {
				if (is_array ( $_REQUEST [$k] )) {
					$meta_array [$i ++] = array ('key' => $custom_field, 'value' => $v, 'compare' => 'IN');
				} else {
					$custom_field = substr ( $k, 3 );
					if ($v != - 1) {
						$meta_array [$i ++] = array ('key' => $custom_field, 'value' => $v, 'compare' => '=' );
					}
				}
			}
		}
		/*
		 * browse for range sliders
		 */
		foreach ( $_REQUEST as $k => $v ) {
			if (strpos ( $k, 'range_cf_' ) === 0) {
				$range = $_REQUEST [$k];
				$range_array = explode ( '-', $range );
				$custom_field = substr ( $k, 9 );
				$meta_array [$i ++] = array ('key' => $custom_field, 'value' => array ($range_array [0], $range_array [1] ), 'compare' => 'BETWEEN', 'type' => 'NUMERIC' );
			}
		}
		/*
		 * browse for simple sliders
		 */
		foreach ( $_REQUEST as $k => $v ) {
			if (strpos ( $k, 'max_cf_' ) === 0) {
				$range = $_REQUEST [$k];
				$custom_field = substr ( $k, 7 );
				$meta_array [$i ++] = array ('key' => $custom_field, 'value' => array ($range ), 'compare' => '>=', 'type' => 'NUMERIC' );
			}
		}
		$query->set ( 'meta_query', $meta_array );
	}
}
add_action ( 'pre_get_posts', 'exclude_category' );


/* set search template */
add_filter( 'template_include', 'its_search_page_template', 99 );
function its_search_page_template( $template ) {
	$new_template = $template;
	if(isset($_REQUEST['its_search']) && $_REQUEST['search_template']!=''){
		$tmpl_array = array('is_search', 'is_archive', 'is_category', 'is_tag', 'is_tax', 'is_post_type_archive', 'is_home');
		$tmpl = urldecode($_REQUEST['search_template']);
		if (!in_array($tmpl, $tmpl_array)) {
			$new_template = locate_template( $tmpl );
		}
		return $new_template;
	}
	else return $template;
}

/*
 * ajax call for children dropdown
 */
function its_get_select_term_children() {
	echo "<div class=\"" . $_REQUEST ['type'] . "-child\">";
	$theme = $_REQUEST['theme'];
	$hide_empty = $_REQUEST ['hide_empty'];
	$show_counts = $_REQUEST ['show_counts'];
	$taxonomy = $_REQUEST ['taxonomy'];
	$taxonomy [0] = $taxonomy [1] = $taxonomy [2] = '';
	$taxonomy = trim ( $taxonomy );
	$term = trim ( $_REQUEST ['term'] );
	@$idObj = get_term_by ( 'slug', $term, $taxonomy );
	@$idObj->term_id;
	if (!isset($idObj->term_id)) die ();
	@$args = array ('taxonomy' => $taxonomy, 'child_of' => $idObj->term_id, 'hide_empty' => $hide_empty );
	$categories = get_categories ( $args );
	$cat_array = null;
	if (sizeof ( $categories )) {
		foreach ( $categories as $k => $category ) {
			if ($category->parent == $idObj->term_id)
				$cat_array [] = $category;
		}
	}
	if (sizeof ( $cat_array )) {
		echo "<div class=\"" . $_REQUEST ['type'] . "-child\">";
		echo "<select onchange=\"updateForm(jQuery(this), 'select', " . ( int ) $hide_empty . ", " . ( int ) $show_counts . ", ".$theme.");\" name=\"tx_$taxonomy\" class=\"taxonomy_select_".$theme."\">";
		echo "<option value=\"-1\">Select All</option>";
		foreach ( $cat_array as $k => $category ) {
			echo "<option value=\"$category->slug\">$category->name";
			if ($show_counts == 1) {
				$subcats = get_categories ( array ('taxonomy' => $taxonomy, 'child_of' => $category->term_id, 'hide_empty' => $_REQUEST ['hide_empty'] ) );
				$sum = 0;
				if (sizeof ( $subcats )) {
					foreach ( $subcats as $subcat ) {
						$sum += $subcat->category_count;
					}
					echo "(" . $sum . ")";
				} else
					echo "(" . $category->category_count . ")";
			}
			echo "</option>";
		}
		echo "</select>";
		echo "</div>";
	}
	echo "</div>";
	die ();
}
add_action ( 'wp_ajax_its_get_select_term_children', 'its_get_select_term_children' );
add_action ( 'wp_ajax_nopriv_its_get_select_term_children', 'its_get_select_term_children' );
/*
 * ajax call for children radio
 */
function its_get_radio_term_children() {
	echo "<div class=\"" . $_REQUEST ['type'] . "-child\">";
	$hide_empty = $_REQUEST ['hide_empty'];
	$show_counts = $_REQUEST ['show_counts'] = 1;
	$taxonomy = $_REQUEST ['taxonomy'];
	$taxonomy [0] = $taxonomy [1] = $taxonomy [2] = '';
	$taxonomy = trim ( $taxonomy );
	$term = trim ( $_REQUEST ['term'] );
	$idObj = get_term_by ( 'slug', $term, $taxonomy );
	$idObj->term_id;
	$args = array ('taxonomy' => $taxonomy, 'child_of' => $idObj->term_id, 'hide_empty' => $hide_empty );
	$categories = get_categories ( $args );
	$cat_array = null;
	if (sizeof ( $categories )) {
		foreach ( $categories as $k => $category ) {
			if ($category->parent == $idObj->term_id)
				$cat_array [] = $category;
		}
	}
	if (sizeof ( $cat_array )) {
		echo "<div class=\"" . $_REQUEST ['type'] . "-child\">";
		echo "<ul>";
		foreach ( $cat_array as $k => $category ) {
			echo "<li><input type='radio' name='tx_" . $taxonomy . "' value=\"$category->slug\" onchange=\"updateForm(jQuery(this), 'radio', " . ( int ) $hide_empty . ", " . ( int ) $show_counts . ");\">" . $category->name;
			if ($show_counts == 1) {
				$subcats = get_categories ( array ('taxonomy' => $taxonomy, 'child_of' => $category->term_id, 'hide_empty' => $_REQUEST ['hide_empty'] ) );
				$sum = 0;
				if (sizeof ( $subcats )) {
					foreach ( $subcats as $subcat ) {
						$sum += $subcat->category_count;
					}
					echo "(" . $sum . ")";
				} else
					echo "(" . $category->category_count . ")";
			}
		}
		echo "</ul>";
		echo "</div>";
	}
	echo "</div>";
	die ();
}
add_action ( 'wp_ajax_its_get_radio_term_children', 'its_get_radio_term_children' );
add_action ( 'wp_ajax_nopriv_its_get_radio_term_children', 'its_get_radio_term_children' );
/*
 * ajax call for children checkbox
 */
function its_get_checkbox_term_children() {
	echo "<div class=\"" . $_REQUEST ['type'] . "-child\">";
	$hide_empty = $_REQUEST ['hide_empty'];
	$show_counts = $_REQUEST ['show_counts'] = 1;
	$taxonomy = $_REQUEST ['taxonomy'];
	$taxonomy [0] = $taxonomy [1] = $taxonomy [2] = '';
	$taxonomy = trim ( $taxonomy );
	$taxonomy = substr ( $taxonomy, 0, - 2 );
	$taxonomy = trim ( $taxonomy );
	$term = trim ( $_REQUEST ['term'] );
	$idObj = get_term_by ( 'slug', $term, $taxonomy );
	$idObj->term_id;
	$args = array ('taxonomy' => $taxonomy, 'child_of' => $idObj->term_id, 'hide_empty' => $hide_empty );
	$categories = get_categories ( $args );
	$cat_array = null;
	if (sizeof ( $categories )) {
		foreach ( $categories as $k => $category ) {
			if ($category->parent == $idObj->term_id)
				$cat_array [] = $category;
		}
	}
	if (sizeof ( $cat_array )) {
		echo "<div class=\"" . $_REQUEST ['type'] . "-child\">";
		echo "<ul>";
		foreach ( $cat_array as $k => $category ) {
			echo "<li><input type='checkbox' name='tx_" . $taxonomy . "[]' value=\"$category->slug\" onchange=\"updateForm(jQuery(this), 'checkbox', " . ( int ) $hide_empty . ", " . ( int ) $show_counts . ");\">" . $category->name;
			if ($show_counts == 1) {
				$subcats = get_categories ( array ('taxonomy' => $taxonomy, 'child_of' => $category->term_id, 'hide_empty' => $_REQUEST ['hide_empty'] ) );
				$sum = 0;
				if (sizeof ( $subcats )) {
					foreach ( $subcats as $subcat ) {
						$sum += $subcat->category_count;
					}
					echo "(" . $sum . ")";
				} else
					echo "(" . $category->category_count . ")";
			}
		}
		echo "</ul>";
		echo "</div>";
	}
	echo "</div>";
	die ();
}
add_action ( 'wp_ajax_its_get_checkbox_term_children', 'its_get_checkbox_term_children' );
add_action ( 'wp_ajax_nopriv_its_get_checkbox_term_children', 'its_get_checkbox_term_children' );
/**
 * ADMIN AJAX CALLS
 */
function its_add_row_form() {
	?>
<div class="row-fluid builder"
	style="border: 1px solid #ccc; padding: 5px; margin: 1px;">
	<input type="hidden" class="cell_size" name="cell_size[]" value="">
	<div class="row-menu">
		<div class="row-button"
			onclick="jQuery(this).parent().parent().find('.form').show('slow');">
			<i class="icon-th-large"></i>
		</div>
		<div class="row-button"
			onclick="jQuery(this).parent().parent().remove();">
			<i class="icon-remove"></i>
		</div>
	</div>
	<div class="form">
		<div class="col-type">
			<div class="col12" onclick="changeColumns(this, '12');"></div>
		</div>
		<div class="col-type">
			<div class="col8-4" onclick="changeColumns(this, '8:4');"></div>
		</div>
		<div class="col-type">
			<div class="col6-6" onclick="changeColumns(this, '6:6');"></div>
		</div>
		<div class="col-type">
			<div class="col4-8" onclick="changeColumns(this, '4:8');"></div>
		</div>
		<div class="col-type">
			<div class="col4-4-4" onclick="changeColumns(this, '4:4:4');"></div>
		</div>
		<div class="col-type">
			<div class="col3-6-3" onclick="changeColumns(this, '3:6:3');"></div>
		</div>
	</div>
	<div class="columns"></div>
</div>
<?php
	die ();
}
add_action ( 'wp_ajax_its_add_row_form', 'its_add_row_form' );
function its_add_columns() {
	$cols = $_POST ['cols'];
	$cols = explode ( ":", $_POST ['cols'] );
	$i = 0;
	foreach ( $cols as $col ) {
		$i ++?>
<div class="span<?php echo $col; ?>" style="border: 1px dotted #ccc;">
	<div class="tax_result" style="min-height: 50px; border: 1px dotted;"></div>
	<div class="tax_form"></div>

	<div class="add_column plus" onclick="loadTaxonomyForm(this);">
		<i class="icon-plus"></i> Taxonomy
	</div>
	<div class="add_column minus" onclick="destroyTaxonomyForm(this);"
		style="display: none;">
		<i class="icon-minus"></i> Taxonomy
	</div>
</div>
<?php
	}
	die ();
}
add_action ( 'wp_ajax_its_add_columns', 'its_add_columns' );
function its_add_taxonomy_form() {
	$post_type = $_REQUEST ['post_type'];
	$args = array ('object_type' => array ($post_type ) );
	$output = 'objects';
	$operator = 'or'; 
	$taxonomies = get_taxonomies ( $args, $output, $operator );
	global $wpdb;
	$custom_fields = $wpdb->get_results ("SELECT distinct(a.meta_key) FROM  " . $wpdb->prefix . "postmeta as a, " . $wpdb->prefix . "posts as b 
			where 
			b.ID = a.post_id AND 
			b.post_type='" . $post_type . "'");
	?>
<ul class="tabs tabs-inline tabs-top">
	<li class="active"
		onclick="jQuery(this).parent().children().toggleClass('active');jQuery(this).parent().parent().children('.taxonomy_form').slideToggle();">
		<a data-toggle="tab"><i class="icon-inbox"></i> Taxonomy</a>
	</li>
	<li
		onclick="jQuery(this).parent().children().toggleClass('active');jQuery(this).parent().parent().children('.taxonomy_form').slideToggle();">
		<a data-toggle="tab"><i class="icon-inbox"></i> Button</a>
	</li>
</ul>
<div class="taxonomy_form" style="height:0">
<form class="tax_form_add" onsubmit="addTaxonomy(this); return false;" method="POST">
</form>
</div>
<div class="taxonomy_form">
	<span>Custom fields will only be available in this form builder once there are posts with them assigned.</span>
	<form class="tax_form_add" onsubmit="addTaxonomy(this); return false;" method="POST">
		<div class="form-horizontal"> 
			<div class="control-group" style="margin-bottom: 23px;">
				<label class="control-label tax-label">Taxonomy:</label>
				<div class="controls">
					<select name="tax_name" class="tax-name">
						<optgroup label="Taxonomies">
						<?php
						foreach ( $taxonomies as $taxonomy ) {
						?>
							<option value="t_<?php echo $taxonomy->name; ?>"><?php echo $taxonomy->label; ?></option>
						<?php
						}
						?>
						</optgroup>
						<optgroup label="Custom Fields">
						<?php
						if ($custom_fields) {
						foreach ( $custom_fields as $k => $v ) {
						?>
							<option value="cf_<?php echo $v->meta_key; ?>"><?php echo $v->meta_key; ?></option>
						<?php
						}
						}
						?>
						</optgroup>
					</select>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">Display Style:</label>
				<div class="controls">
					<select name="display_style"
						onchange="updateTaxForm(jQuery(this));">
						<option>Select</option>
						<option>Radio</option>
						<option>Checkbox</option>
						<option>Simple Slider</option>
						<option>Range Slider</option>
						<!-- <option>Search Field</option>  -->
					</select>
				</div>
			</div>
			<div class="simple_slider" style="display: none;">
				<div class="control-group">
					<label class="control-label" style="vertical-align: baseline;">Min
						Value</label>
					<div class="controls">
						<input type="checkbox" name="slider_min_value_default" value="-1" />
						0 or <input type="text" name="slider_min_value"
							style="max-width: 120px;" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" style="vertical-align: baseline;">Max
						Value</label>
					<div class="controls">
						<input type="checkbox" name="slider_max_value_default" value="-1" />
						auto-determine or <input type="text" name="slider_max_value"
							style="max-width: 120px;" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" style="vertical-align: baseline;">Step</label>
					<div class="controls">
						<input type="checkbox" name="slider_step_default" value="-1" />
						auto scale or <input type="text" name="slider_step"
							style="max-width: 120px;" />
					</div>
				</div>
			</div>

			<div class="range_slider" style="display: none;">
				<div class="control-group">
					<label class="control-label" style="vertical-align: baseline;">Min
						Value</label>
					<div class="controls">
						<input type="checkbox" name="slider_min_value_default" value="-1" />
						auto-determine or <input type="text" name="slider_min_value"
							style="max-width: 120px;" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" style="vertical-align: baseline;">Max
						Value</label>
					<div class="controls">
						<input type="checkbox" name="slider_max_value_default" value="-1" />
						auto-determine or <input type="text" name="slider_max_value"
							style="max-width: 120px;" />
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" style="vertical-align: baseline;">Step</label>
					<div class="controls">
						<input type="checkbox" name="slider_step_default" value="-1" />
						auto scale or <input type="text" name="slider_step"
							style="max-width: 120px;" />
					</div>
				</div>
			</div>

			<div class="control-group">
				<label class="control-label" style="vertical-align: baseline;">Custom
					Label:</label>
				<div class="controls">
					<input type="text" name="custom_label" style="max-width: 120px;" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" style="vertical-align: baseline;">Show
					Counts</label>
				<div class="controls">
					<input type="checkbox" name="show_counts" value="1" />
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" style="vertical-align: baseline;">Hide
					Empty Results</label>
				<div class="controls">
					<input type="checkbox" name="hide_empty" value="1" />
				</div>
			</div>
			<input type="submit" value="Add+" class="save">

		</div>
	</form>
</div>
<div class="taxonomy_form form-horizontal" style="display: none;">
	<form method="post" class="tax_form_add" onsubmit="addTaxonomy(this); return false;">
		<div class="control-group">
			<label class="control-label" style="vertical-align: baseline;">Button
				Type</label>
			<div class="controls" style="padding: 5px 0 0 10px;">
				<div style="padding-bottom: 10px;">
					<input type="radio" name="button_type" value="submit"
						style="border-radius: 100% !important; margin: -4px 4px 0 0;" /> <span
						style="color: #477db5; font-weight: bold;">Submit</span>
				</div>
				<div>
					<input type="radio" name="button_type" value="reset"
						style="border-radius: 100% !important; margin: -4px 4px 0 0;" /> <span
						style="color: #477db5; font-weight: bold;">Reset</span>
				</div>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label" style="vertical-align: baseline;">Button
				Label: </label>
			<div class="controls">
				<input type="text" name="button_label" />
			</div>
		</div>
		<input type="submit" value="Add+" class="save">
	</form>
</div>
<?php
	die ();
}
add_action ( 'wp_ajax_its_add_taxonomy_form', 'its_add_taxonomy_form' );
function its_add_taxonomy() {
	$dir = plugin_dir_path ( __FILE__ );
	include ($dir . 'inc/add_taxonomy.php');
	die ();
}
add_action ( 'wp_ajax_its_add_taxonomy', 'its_add_taxonomy' );
function its_save_admin_form() {
	$json = array ('success' => false, 'result' => $_POST ,'form_id' => $_POST['form_id'] );
	$json ['success'] = true;
	global $wpdb;
	if ($_POST['form_id'] != -1) {
		$wpdb->query ( "UPDATE " . $wpdb->prefix . "its_forms set name = '" . $_POST ['real_form_name'] . "' WHERE id = '".$_POST['form_id']."'");
		$wpdb->query ( "DELETE FROM " . $wpdb->prefix . "its_formmeta WHERE form_id = '".$_POST['form_id']."'");
		$form_id = $_POST['form_id'];
	} else {
	$wpdb->query ( "INSERT INTO " . $wpdb->prefix . "its_forms (name) VALUES ('" . $_POST ['real_form_name'] . "')");
	$form_id = $wpdb->insert_id;
	$json['form_id'] = $form_id; 
	}
	
// 	if ($form_id == 31 || $form_id == 30 ) die();
	
	$wpdb->query ("INSERT INTO " . $wpdb->prefix . "its_formmeta (form_id, meta_name, meta_value) VALUES ('$form_id', 'row_count', '" . count ( $_POST ['meta_taxonomy'] ) . "')");
	
/*filter_theme*/
	$wpdb->query ( "INSERT INTO " . $wpdb->prefix . "its_formmeta (form_id, meta_name, meta_value) VALUES ('$form_id', 'filter_theme', '" . ( $_POST ['filter_theme'] ) . "')");
/* logical_cond*/
	$wpdb->query ( "INSERT INTO " . $wpdb->prefix . "its_formmeta (form_id, meta_name, meta_value) VALUES ('$form_id', 'logical_cond', '" . ( $_POST ['logical_cond'] ) . "')");
/*post_type	product*/
	$wpdb->query ( "INSERT INTO " . $wpdb->prefix . "its_formmeta (form_id, meta_name, meta_value) VALUES ('$form_id', 'post_type', '" . ( $_POST ['post_type'] ) . "')");
/*real_form_autosearch*/
	$wpdb->query ( "INSERT INTO " . $wpdb->prefix . "its_formmeta (form_id, meta_name, meta_value) VALUES ('$form_id', 'autosearch', '" . ( $_POST ['real_form_autosearch'] ) . "')");
/*search_template*/
	$wpdb->query ( "INSERT INTO " . $wpdb->prefix . "its_formmeta (form_id, meta_name, meta_value) VALUES ('$form_id', 'search_template', '" . ( $_POST ['search_template'] ) . "')");
	
	for($i = 0; $i < count ( $_POST ['meta_taxonomy'] ); $i ++) {
		$current_row = $i + 1;
		$colls_count = count ( $_POST ['meta_taxonomy'] [$current_row] );
		$wpdb->query ( "INSERT INTO " . $wpdb->prefix . "its_formmeta (form_id, meta_name, meta_value, meta_row) VALUES ('$form_id', 'coll_count', '" . $colls_count . "', '$current_row')");
		for($j = 0; $j < $colls_count; $j ++) {
			$current_col = $j + 1;
			$tax_count = count ( $_POST ['meta_taxonomy'] [$current_row] [$current_col] );
			for($k = 0; $k < $tax_count; $k ++) {
				$current_tax = $k + 1;
				foreach ( $_POST ['meta_taxonomy'] [$current_row] [$current_col] [$current_tax] as $key => $value ) {
					$wpdb->query ( "INSERT INTO " . $wpdb->prefix . "its_formmeta (form_id, meta_name, meta_value, meta_row, meta_col, meta_position) VALUES ('$form_id', '$key', '$value', '$current_row','$current_col','$current_tax')");
				}
			}
		}
	}
	foreach ( $_POST ['cell_size'] as $k => $v ) {
		$current_index = $k + 1;
		$wpdb->query ( "INSERT INTO " . $wpdb->prefix . "its_formmeta (form_id, meta_name, meta_value, meta_row) VALUES ('$form_id', 'cell_size', '$v', '$current_index')");
	}
	echo json_encode($json);
	die ();
}
add_action ( 'wp_ajax_its_save_admin_form', 'its_save_admin_form' );
