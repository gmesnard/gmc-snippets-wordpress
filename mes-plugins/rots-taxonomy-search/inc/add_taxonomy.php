<?php

$data = $_POST ['data'];
$data_array = explode ( '&', $data );
foreach ( $data_array as $k => $v ) {
	$split = explode ( '=', $v );
	unset ( $data_array [$k] );
	$data_array [$split [0]] = $split [1];
}
if (isset ( $data_array ['display_style'] ))
	$display_style = $data_array ['display_style'];
$tax_name = $hide_empty = $show_counts = '';
if (isset ( $data_array ['tax_name'] ))
	$tax_name = $data_array ['tax_name'];
if (isset ( $data_array ['hide_empty'] ))
	$hide_empty = $data_array ['hide_empty'];
if (isset ( $data_array ['show_counts '] ))
	$show_counts = $data_array ['show_counts'];
if (strpos ( $tax_name, 't_' ) === 0) {
	$tax_name [0] = '';
	$tax_name [1] = '';
	$meta_type = "taxonomy";
}
@$data_array['custom_label'] = urldecode ( $data_array ['custom_label'] );
if (strpos ( $tax_name, 'cf_' ) === 0) {
	$tax_name [0] = '';
	$tax_name [1] = '';
	$tax_name [2] = '';
	$meta_type = "custom field"; 
}
$tax_name = trim ( $tax_name );
$taxonomy = get_taxonomy ( $tax_name );
$all_categories = get_categories ( array ('taxonomy' => $tax_name, 'hide_empty' => $hide_empty ) );
function display_form_field($data_array) {
	$tax_name = $hide_empty = $show_counts = '0';
	if (isset ( $data_array ['display_style'] ))
		$display_style = $data_array ['display_style'];
	$tax_name = $hide_empty = '';
	if (isset ( $data_array ['tax_name'] ))
		$tax_name = $data_array ['tax_name'];
	if (isset ( $data_array ['hide_empty'] ))
		$hide_empty = $data_array ['hide_empty'];
	if (isset ( $data_array ['show_counts'] ))
		$show_counts = $data_array ['show_counts'];
	if (strpos ( $tax_name, 't_' ) === 0) {
		$tax_name [0] = '';
		$tax_name [1] = '';
		$meta_type = "taxonomy";
	}
	if (strpos ( $tax_name, 'cf_' ) === 0) {
		$tax_name [0] = '';
		$tax_name [1] = '';
		$tax_name [2] = '';
		$meta_type = "custom field";
	}
	$tax_name = trim ( $tax_name );
	$taxonomy = get_taxonomy ( $tax_name );
	if (isset ( $data_array ['slider_step_default'] ) && $data_array ['slider_step_default'])
		$slider_step = $data_array ['slider_step_default'];
	else
		$slider_step = $data_array ['slider_step'];
	if (isset ( $data_array ['slider_min_value_default'] ) && $data_array ['slider_min_value_default'])
		$slider_min_value = $data_array ['slider_min_value_default'];
	else
		$slider_min_value = $data_array ['slider_min_value'];
	if (isset ( $data_array ['slider_max_value_default'] ) && $data_array ['slider_max_value_default'])
		$slider_max_value = $data_array ['slider_max_value_default'];
	else
		$slider_max_value = $data_array ['slider_max_value'];
	switch ($display_style) {
		case 'Select' :
			echo "<select class='taxonomy_select'>";
			echo "<option>Option 1</option>";
			echo "<option>Option 2</option>";
			echo "<option>Option 3</option>";
			echo "</select>";
			break;
		case 'Search+Field' :
			echo "<input type='text' class='taxonomy_input'>";
			break;
		case 'Checkbox' :
			echo '<div class="checkbox_container"><ul>';
			echo "<li><input type='checkbox'>Option 1</li>";
			echo "<li><input type='checkbox'>Option 2</li>";
			echo "<li><input type='checkbox'>Option 3</li>";
			echo '</ul></div>';
			break;
		case 'Radio' :
			echo '<div class="checkbox_container"><ul>';
			echo "<li><input type='radio'>Option 1</li>";
			echo "<li><input type='radio'>Option 2</li>";
			echo "<li><input type='radio'>Option 3</li>";
			echo '</ul></div>';
			break;
		case 'Simple+Slider' :
			echo '
				<div class="controls">
					<div class="slider" data-step="1" data-min="0" data-max="100">
				<div class="amount"></div>
				<div class="slide"></div>
				</div>';
			break;
		case 'Range+Slider' :
			echo '
				<div class="controls">
				<div class="slider" data-step="1" data-min="0" data-max="100" data-range="false" data-rangestart="15" data-rangestop="65">
				<div class="amount"></div>
				<div class="slide"></div>
				</div>';
			break;
	}
	echo "\n
	
		<input type='hidden' name= 'meta_taxonomy_item_type' value='$meta_type' />
		<input type='hidden' name= 'meta_taxonomy_tax_name' value='" . $data_array ['tax_name'] . "' />
		<input type='hidden' name= 'meta_taxonomy_display_style' value='" . $data_array ['display_style'] . "' />
		<input type='hidden' name= 'meta_taxonomy_custom_label' value='" . $data_array ['custom_label'] . "' />";
	
	if ($display_style != "Search+Field") {
		echo "
			<input type='hidden' name= 'meta_taxonomy_hide_empty' value='" . $hide_empty . "' />
			<input type='hidden' name= 'meta_taxonomy_show_counts' value= '".$show_counts."'/>";
	}
	if ($display_style == "Simple+Slider" || $display_style == "Range+Slider") {
		echo "
			<input type='hidden' name='meta_taxonomy_slider_min_value'  value='" . $slider_min_value . "' />
			<input type='hidden' name='meta_taxonomy_slider_max_value'  value='" . $slider_max_value . "' />
			<input type='hidden' name='meta_taxonomy_slider_step'  value='" . $slider_step . "' />";
	}
	echo "</script>";
}
echo '<div class="individual_taxonomy">';
echo '<br />';
if (isset ( $data_array ['button_type'] ) && strlen ( $data_array ['button_type'] ) > 2) {
	echo '<button class="button-submit">' . $data_array ['button_label'] . '</button>
		<i class="icon-move"></i><i class="icon-remove" onclick="jQuery(this).parent().remove();"></i>';
	echo "	<input type='hidden' name= 'meta_taxonomy_item_type' value='" . $data_array ['button_type'] . "' />
		<input type='hidden' name= 'meta_taxonomy_label' value='" . $data_array ['button_label'] . "' />";
} else {
	if (isset ( $data_array ['custom_label'] ) && strlen ( trim ( $data_array ['custom_label'] ) ) > 0) {
		$label = $data_array ['custom_label'];
	} else {
		$label = $taxonomy->labels->name;
	}
	echo '
		<span class="tax_label">' . $label . '</span>';
	display_form_field ( $data_array );
	echo '<i class="icon-move"></i><i class="icon-remove" onclick="jQuery(this).parent().remove();"></i>';
}
echo '</div>';