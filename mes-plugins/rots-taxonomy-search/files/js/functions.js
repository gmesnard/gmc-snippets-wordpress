function sortableItems() {
	{
		jQuery(".tax_result").sortable({
			connectWith : ".tax_result",
			handle : ".icon-move",
			cancel : ".portlet-toggle",
			placeholder : "portlet-placeholder ui-corner-all"
		});

		jQuery(".individual_taxonomy").addClass(
				"ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
				.find(".icon-move").addClass("ui-widget-header ui-corner-all")

		jQuery(".portlet-toggle").click(function() {
			var icon = jQuery(this);
			icon.toggleClass("ui-icon-minusthick ui-icon-plusthick");
			icon.closest(".portlet").find(".portlet-content").toggle();
		});
	}
}

function activateSliders() {
	if (jQuery('.slider').length > 0) {
		jQuery(".slider")
				.each(
						function() {
							var jQueryel = jQuery(this);
							var min = parseInt(jQueryel.attr('data-min')), max = parseInt(jQueryel
									.attr('data-max')), step = parseInt(jQueryel
									.attr('data-step')), range = jQueryel
									.attr('data-range'), rangestart = parseInt(jQueryel
									.attr('data-rangestart')), rangestop = parseInt(jQueryel
									.attr('data-rangestop'));

							var opt = {
								min : min,
								max : max,
								step : step,
								slide : function(event, ui) {
									jQueryel.find('.amount').html(ui.value);
								}
							};

							if (range !== undefined) {
								opt.range = true;
								opt.values = [ rangestart, rangestop ];
								opt.slide = function(event, ui) {
									jQueryel.find('.amount')
											.html(
													ui.values[0] + " - "
															+ ui.values[1]);
									jQueryel.find(".amount_min").html(
											ui.values[0] + "jQuery");
									jQueryel.find(".amount_max").html(
											ui.values[1] + "jQuery");
								};
							}

							jQueryel.slider(opt);
							if (range !== undefined) {
								var val = jQueryel.slider('values');
								jQueryel.find('.amount').html(
										val[0] + ' - ' + val[1]);
								jQueryel.find(".amount_min").html(
										val[0] + "jQuery");
								jQueryel.find(".amount_max").html(
										val[1] + "jQuery");
							} else {
								jQueryel.find('.amount').html(
										jQueryel.slider('value'));
							}
						});
	}
}
activateSliders();

function selectAllC(from, where) {
	if (jQuery(from).is(':checked')) {
		jQuery(where).prop('checked', true);
	} else {
		jQuery(where).prop('checked', false);
	}
}

function updateForm(field, type, hide_empty, show_counts, theme) {
	var animatedLoading = '<div class="loading"></div>';
	jQuery(field).after(animatedLoading);
	jQuery.ajax({
		type : "POST",
		url : ajaxobject.ajaxurl,
		data : {
			action : 'its_get_' + type + '_term_children',
			taxonomy : field.attr('name'),
			type : type,
			term : field.val(),
			hide_empty : hide_empty,
			show_counts : show_counts,
			theme: theme
		},
		success : function(results) {
			field.next('.loading').remove();
			if (type == 'select') {
				field.next('.' + type + '-child').remove();
				jQuery(field).after(results);
			}
			if (type == 'radio') {
				field.parent().parent().find('.' + type + '-child').remove();
				jQuery(field).parent().append(results);
			}
			if (type == 'checkbox') {
				if (field.is(':checked')) {
					field.parent().parent().find('.' + type + '-child')
							.remove();
					jQuery(field).parent().append(results);
				} else {
					field.parent().parent().find('.' + type + '-child')
							.remove();
				}
			}
		}
	});
}

/* AJAX actions */
/* Add ROW */
jQuery("#add_row").click(function() {
	jQuery.ajax({
		url : ajaxobject.ajaxurl,
		data : {
			action : 'its_add_row_form'
		},
		success : function(results) {
			jQuery("#visual_builder").append(results);
		}
	});
});

/* Add COLUMN */
function changeColumns(button, format) {
	jQuery('.col-type').removeClass('selected');
	jQuery(button).parent().addClass('selected');
	addColumns(button, format);
	jQuery(button).parent().parent().hide('slow');

}
function addColumns(button, format) {

	jQuery.ajax({
		type : "POST",
		url : ajaxobject.ajaxurl,
		data : {
			action : 'its_add_columns',
			cols : format
		},
		success : function(results) {
			jQuery(button).parent().parent().parent().find('.columns').html(
					results);
			jQuery(button).parent().parent().parent().find('.cell_size').val(
					format);
		}
	});
}

/* Add TAXONOMY FORM */

function loadTaxonomyForm(button) {
	jQuery(button).parent().find('.add_column').show();
	jQuery(button).hide();
	jQuery.ajax({
		type : "POST",
		url : ajaxobject.ajaxurl + '?action=its_add_taxonomy_form&post_type='
				+ jQuery('#post_type').val(),
		success : function(results) {
			jQuery(button).parent().find('.tax_form').html(results);
		}
	});
}

function destroyTaxonomyForm(button) {
	jQuery(button).parent().find('.add_column').show();
	jQuery(button).hide();
	jQuery(button).parent().find('.tax_form').html('');
}

function updateTaxForm(field) {
	var container = field.parent().parent().parent();
	switch (field.val()) {
	case "Simple Slider":
		container.children(".range_slider").hide();
		container.children(".simple_slider").show();
		break;
	case "Range Slider":
		container.children(".simple_slider").hide();
		container.children(".range_slider").show();
		break;
	default:
		container.children(".simple_slider").hide();
		container.children(".range_slider").hide();
	}
}

function addTaxonomy(form) {
	var form = jQuery(form), content = form.serialize();

	jQuery(form).parent().parent().parent().find('.add_column').hide();
	jQuery(form).parent().parent().parent().find('.plus').show();
	jQuery
			.ajax({
				type : "POST",
				url : ajaxobject.ajaxurl,
				data : {
					action : 'its_add_taxonomy',
					dataType : 'json',
					data : content
				},
				success : function(results) {
					form.parent().parent().parent().find('.tax_result').append(
							results);
					form.parent().find('.tax_form').html('');
					activateSliders();
					sortableItems();
				}
			});

	return false;
}

jQuery("#real_builder_form").submit(
		function() {
			jQuery('.tax_form').html('');
			jQuery('.minus').hide();
			jQuery('.plus').show();
			/* set form index values */
			var allRows = jQuery('#real_builder_form').find(
					'.row-fluid .builder');
			var i = 0;
			/* each row */
			allRows.each(function() {
				i++;
				var columnsContainer = jQuery(this).children('.columns');
				var allColumns = columnsContainer.children('div');
				/* each column */
				var j = 0;
				allColumns.each(function() {
					j++;
					var allTax = jQuery(this).find('.individual_taxonomy');
					var k = 0;
					allTax.each(function() {
						k++;
						var nameArray = [ 'item_type', 'tax_name',
								'display_style', 'custom_label', 'hide_empty',
								'show_counts', 'label', 'slider_min_value',
								'slider_max_value', 'slider_step' ];
						for (a = 0; a < nameArray.length; a++) {
							var meta = jQuery(this).find(
									'input[name=meta_taxonomy_' + nameArray[a]
											+ ']');
							meta.attr('name', 'meta_taxonomy[' + i + '][' + j
									+ '][' + k + '][' + nameArray[a] + ']');
						}
					});

				});
			});
			var form = jQuery(this), content = form.serialize();
			var submitButton = form.find(':submit');
			submitButton.addClass('its_spinner');
			jQuery.ajax({
				type : "POST",
				url : ajaxobject.ajaxurl + '?action=its_save_admin_form',
				data : content,
				dataType : 'json',
				success : function(results) {
					submitButton.removeClass('its_spinner');
					jQuery('.form_id').val(results.form_id);

				}
			});
			return false;
		});

/* slider counter updater */
jQuery(document).ready(function() {
	jQuery('.slider').each(function() {
		jQuery(this).slider({
			change : function() {
				var new_value = 0;
				if (jQuery(this).hasClass('simple_slider')) {
					var value = jQuery(this).slider("value");
					new_value = value;
				} else {
					var lower = jQuery(this).slider("values", 0);
					var upper = jQuery(this).slider("values", 1);
					new_value = lower + '-' + upper;
				}
				jQuery(this).parent().children(':hidden').val(new_value);
			}
		});

	});

});

/*reset form on RESET*/
function resetForm(resetButton) {
	var form =  resetButton.form;
	jQuery(form).find('.select-child').remove();
	jQuery(form).find('.radio-child').remove();
	jQuery(form).find('.checkbox-child').remove();
}