<?php
global $wpdb;
$forms = $wpdb->get_results("SELECT id,name FROM {$wpdb->prefix}its_forms ORDER BY id ASC;");
if(isset($forms) && count($forms)>0){

	    $current_instance_id = explode('-', $this->id);
		$instance_no = $current_instance_id[1];

?>
    Select Search Form:
    <select name="<?php echo $this->get_field_name( 'form_id' ); ?>" id="select_seach_form_<?php echo $instance_no;?>">
        <?php
        foreach($forms as $form){
            $selected = '';
            if($instance['form_id']==$form->id){
                $selected = 'selected="selected" ';
                $widget_title = $form->name;
            }
        ?>
            <option value="<?php echo $form->id;?>" <?php echo $selected;?>><?php echo $form->name;?></option>
        <?php
          } //end of foreach
        ?>
    </select>
<?php
  if(isset($widget_title)){
      ?>
           <script>
                jQuery(document).ready(function(){
                    id = jQuery('div[id*="indeedtaxonomysearch-<?php echo $instance_no;?>"]').attr('id');
                    the_id = '#'+id+' .widget-title h4';
                    currentTitle = jQuery(the_id).html();
                    jQuery(the_id).html(currentTitle+' <?php echo $widget_title;?>');
                    console.log(currentTitle);
                });
                jQuery('#select_seach_form_<?php echo $instance_no;?>').change(function(){
                    id = jQuery('div[id*="indeedtaxonomysearch-<?php echo $instance_no;?>"]').attr('id');
                    the_id = '#'+id+' .widget-title h4';
                    jQuery(the_id).html("Taxonomy Search");
                });
           </script>
      <?php
  }
}//end of if
?>
