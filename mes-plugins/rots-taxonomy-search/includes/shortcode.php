<?php
$final_str = '';
global $wpdb;
$blog_url = get_bloginfo ( 'url' );
$dir_path = $blog_url . '/wp-content/plugins/rots-taxonomy-search/';
$random_id = rand ( 0, 10000 );
extract ( shortcode_atts ( array ('id' => '' ), $id, 'rots_taxonomy_search' ) );
$i_tax_search_id = $id;
if (isset ( $i_tax_search_id ) && $i_tax_search_id != '') {
	$metas = $wpdb->get_results ( "SELECT *
			FROM {$wpdb->prefix}its_formmeta
			WHERE {$wpdb->prefix}its_formmeta.form_id = $i_tax_search_id order by meta_row" );
	$form = $wpdb->get_results ( "SELECT *
					FROM {$wpdb->prefix}its_forms
					WHERE {$wpdb->prefix}its_forms.id = $i_tax_search_id" );
	/*
	 * Get Form Properties
	 */
	foreach ( $metas as $meta ) {
		if ($meta->meta_name == 'search_template') {
			$search_template = $meta->meta_value;
		}
		if ($meta->meta_name == 'logical_cond') {
			$logical_cond = $meta->meta_value;
		}
		if ($meta->meta_name == 'autosearch') {
			$autosearch = $meta->meta_value;
		}
		if ($meta->meta_name == 'filter_theme') {
			$theme = $meta->meta_value;
		}
		if ($meta->meta_name == 'post_type') {
			$post_type = $meta->meta_value;
		}
	}
	/*
	 * get the rows
	 */
	$rows = $wpdb->get_results ( "SELECT * FROM {$wpdb->prefix}its_formmeta
		WHERE {$wpdb->prefix}its_formmeta.form_id = $i_tax_search_id and meta_name = 'row_count'" );
	$rows = $rows [0]->meta_value;
	/*
	 * Start building the form
	 */
	$theme_number = $theme [strlen ( $theme ) - 5];
	$final_str .= "\n<div id=\"i_tax_search_form_$id\" style=\"border:1px dotted #ccc;\">";
	$final_str .= "\n<div class=\"search_form_title_$theme_number\">" . $form [0]->name . "</div>";
	$final_str .= "\n<form id=\"its_form_$random_id\" action=\"\" method=\"GET\" role=\"search\">
	<input type=\"hidden\" name=\"its_search\" value=\"$i_tax_search_id\" />
	<input type=\"hidden\" name=\"logical_cond\" value=\"$logical_cond\" />
	
	<input type=\"hidden\" name=\"search_template\" value=\"$search_template\" />
	<input type=\"hidden\" name=\"post_type\" value=\"$post_type\" />";
	for($row = 1; $row <= $rows; $row ++) {
		/*
		 * start the row
		 */
		$final_str .= "\n<div class=\"row-fluid\">";
		/*
		 * get the colls for each row
		 */
		$colls = $wpdb->get_results ( "SELECT * FROM {$wpdb->prefix}its_formmeta WHERE {$wpdb->prefix}its_formmeta.form_id = $i_tax_search_id and meta_name = 'coll_count' and meta_row='$row'" );
		$colls = $colls [0]->meta_value;
		$cell_size = $wpdb->get_results ( "SELECT * FROM {$wpdb->prefix}its_formmeta WHERE {$wpdb->prefix}its_formmeta.form_id = $i_tax_search_id and meta_name = 'cell_size' and meta_row='$row'" );
		$cell_size = $cell_size [0]->meta_value;
		$cell_size = explode ( ':', $cell_size );
		/*
		 * start the column
		 */
		foreach ( $cell_size as $k => $coll ) {
			$current_cell = ($k + 1);
			$final_str .= "\n<div class=\"span$coll\" >\n\t
			<div class=\"tax_result\">";
			/*
			 * get all fields inside a column
			 */
			$fieldCount = $wpdb->get_results ( "SELECT distinct(meta_position) FROM {$wpdb->prefix}its_formmeta WHERE 
				{$wpdb->prefix}its_formmeta.form_id = $i_tax_search_id and
				meta_row = $row AND
				meta_col =  $current_cell AND 
				meta_position not in (-1, 0)" );
			for($field_counter = 1; $field_counter <= count ( $fieldCount ); $field_counter ++) {
				$field = $wpdb->get_results ( "SELECT *
						FROM {$wpdb->prefix}its_formmeta
						WHERE
						{$wpdb->prefix}its_formmeta.form_id = $i_tax_search_id and
						meta_row = $row AND
						meta_col = $current_cell AND meta_position = $field_counter" );
				$show_counts = $button_label = $item_type = $tax_name = $display_style = $custom_label = $hide_empty = $slider_min_value = $slider_max_value = $slider_step = null;
				foreach ( $field as $field_property ) {
					switch ($field_property->meta_name) {
						case 'item_type' :
							$item_type = $field_property->meta_value;
							break;
						case 'tax_name' :
							$tax_name = trim ( $field_property->meta_value );
							break;
						case 'display_style' :
							$display_style = $field_property->meta_value;
							break;
						case 'custom_label' :
							$custom_label = $field_property->meta_value;
							break;
						case 'hide_empty' :
							$hide_empty = $field_property->meta_value;
							break;
						case 'slider_min_value' :
							$slider_min_value = $field_property->meta_value;
							break;
						case 'slider_max_value' :
							$slider_max_value = $field_property->meta_value;
							break;
						case 'slider_step' :
							$slider_step = $field_property->meta_value;
							break;
						case 'label' :
							$button_label = $field_property->meta_value;
							break;
						case 'show_counts' :
							$show_counts = $field_property->meta_value;
							break;
					}
				}
				
				if ($item_type == 'taxonomy') {
					$tax_name [0] = '';
					$tax_name [1] = '';
					$tax_name = trim ( $tax_name );
					if (strlen ( $custom_label ) > 0) {
						$label = $custom_label;
					} else {
						$taxonomy = get_taxonomy ( trim ( $tax_name ) );
						$label = $taxonomy->labels->name;
					}
				} else {
					$label = $custom_label;
				}
				$final_str .= "<div class=\"individual_taxonomy_$theme_number\">";
				/*
				 * Display Input Reset / Submit
				 */
				if ($item_type == 'submit' || $item_type == 'reset') {
					$final_str .= "<input type=\"$item_type\" value=\"$button_label\" class=\"button-submit_$theme_number\"";
					if ($item_type == 'reset')
						$final_str .= 'onclick="resetForm(this);"';
					$final_str .= " />";
				}
				/*
				 * Display Taxonomy
				 */
				if ($item_type == "taxonomy") {
					$args = array ('taxonomy' => $tax_name, 'hide_empty' => $hide_empty );
					$all_categories = get_categories ( $args );
					$final_str .= "<span class=\"tax_label_$theme_number\">$label</span>";
					/*
					 * display select
					 */
					switch ($display_style) {
						case 'Select' :
							$final_str .= "<select onchange=\"updateForm(jQuery(this), 'select', " . ( int ) $hide_empty . ", " . ( int ) $show_counts . ", ". $theme_number .");\" name=\"tx_$taxonomy->query_var\" class=\"taxonomy_select_$theme_number\">";
							$final_str .= "<option value=\"-1\">Select All</option>";
							foreach ( $all_categories as $category ) {
								if ($category->parent == 0) {
									$final_str .= "<option value=\"$category->slug\">$category->name";
									if ($show_counts)
										$final_str .= "(" . $category->category_count . ")</option>";
								}
							}
							$final_str .= "</select>";
							break;
						case 'Search Field' :
							$final_str .= '<input type="text" class="taxonomy_input_' . $theme_number . '" name="' . $taxonomy->query_var . '" />';
							break;
						case 'Checkbox' :
							$final_str .= '<div class="checkbox_container_' . $theme_number . '"><ul>';
							foreach ( $all_categories as $category ) {
								if ($category->parent == 0) {
									$final_str .= "<li><input type='checkbox' name='tx_" . $taxonomy->query_var . "[]' value=\"$category->slug\" onchange=\"updateForm(jQuery(this), 'checkbox', " . ( int ) $hide_empty . ", " . ( int ) $show_counts . ", ". $theme_number .");\">" . $category->name;
									if ($show_counts)
										$final_str .= "(" . $category->category_count . ")</option>";
									$final_str .= "</li>";
								}
							}
							$final_str .= '</ul></div>';
							break;
						case 'Radio' :
							$final_str .= '<div class="radio_container_' . $theme_number . '"><ul>';
							foreach ( $all_categories as $category ) {
								if ($category->parent == 0) {
									$final_str .= "<li><input type='radio' name='tx_" . $taxonomy->query_var . "' value=\"$category->slug\" onchange=\"updateForm(jQuery(this), 'radio', " . ( int ) $hide_empty . ", " . ( int ) $show_counts . ", ". $theme_number .");\">" . $category->name;
									if ($show_counts)
										$final_str .= "(" . $category->category_count . ")</option>";
									$final_str .= "</li>";
								}
							}
							$final_str .= '</ul></div>';
							break;
						case 'Simple+Slider' :
							$final_str .= '
				<div class="controls_' . $theme_number . '">
				<div class="slider_' . $theme_number . '" data-step="' . $slider_step . '" data-min="' . $slider_min_value . '" data-max="' . $slider_max_value . '" data-range="false" data-rangestart="' . $slider_min_value . '" data-rangestop="' . $slider_max_value . '">
				<div class="amount_' . $theme_number . '"></div>
				<div class="slide"></div>
				</div>
				</div>';
							break;
						case 'Range+Slider' :
							$final_str .= '
				<div class="controls_' . $theme_number . '">
				<div class="slider_' . $theme_number . '" data-step="' . $slider_step . '" data-min="' . $slider_min_value . '" data-max="' . $slider_max_value . '" data-range="true" data-rangestart="' . $slider_min_value . '" data-rangestop="' . $slider_max_value . '">
				<div class="amount_' . $theme_number . '"></div>
				<div class="slide"></div>
				</div>
				</div>';
							break;
						default :
							break;
					}
				}
				/*
				 * Display Custom Fields
				 */
				if ($slider_step == - 1) {
				}
				if ($slider_max_value == - 1) {
				}
				if ($slider_min_value == - 1) {
				}
				if ($item_type == "custom field") {
					$all_categories = $wpdb->get_results ( "SELECT distinct(meta_value)
							FROM {$wpdb->prefix}postmeta
							WHERE
							{$wpdb->prefix}postmeta.meta_key= '$tax_name' order by meta_value" );
					$final_str .= "<span class=\"tax_label_$theme_number\">$label</span>";
					/*
					 * display select
					 */
					
					$tax_name = substr ( $tax_name, 3 );
					$cf_values = $wpdb->get_results ( "SELECT a.meta_value as slider_value FROM {$wpdb->prefix}postmeta as a, {$wpdb->prefix}posts as b
					WHERE
					a.meta_key = '$tax_name' AND
					b.post_type = '$post_type' 
					" );

					
					foreach($cf_values as $cf_value) {
						$cf_vals[] = $cf_value->slider_value;
					}
					sort($cf_vals);
					$min = $cf_vals[0];
					rsort($cf_vals);
					$max = $cf_vals[0];
					
					$max_tmp = $max;
					if ($slider_step == - 1) {
						while ( ($max_tmp /= 100) > 1 ) {
							$slider_step *= 10;
						}
						$slider_step *= - 1;
					}
					$post_type;
					if ($slider_min_value == - 1) {
						$slider_min_value = $min;
					}
					if ($slider_max_value == - 1) {
						$slider_max_value = $max;
					}
					switch ($display_style) {
						case 'Select' :
							$all_meta_values = $wpdb->get_results ( "
									SELECT distinct(a.meta_value) as value, count(a.meta_value) as count FROM  " . $wpdb->prefix . "postmeta as a, " . $wpdb->prefix . "posts as b 
									where 
									b.ID = a.post_id AND 
									b.post_type='" . $post_type . "' AND 
									a.meta_key = '" . trim ( $tax_name ) . "'
									GROUP BY
									a.meta_value
									" );
							$final_str .= "<select name=\"cf_" . trim ( $tax_name ) . "\" class=\"taxonomy_select_$theme_number\">";
							$final_str .= "<option value=\"-1\">Select All</option>";
							if (sizeof ( $all_meta_values )) {
								foreach ( $all_meta_values as $meta_value ) {
									$final_str .= "<option value=\"$meta_value->value\">$meta_value->value";
									if ($show_counts == 1)
										$final_str .= '(' . $meta_value->count . ')';
									$final_str .= "</option>";
								}
							}
							$final_str .= "</select>";
							break;
						case 'Search+Field' :
							$final_str .= '<input type="text" class="taxonomy_input_' . $theme_number . '" name="cf_' . trim ( $tax_name ) . '" />';
							break;
						case 'Checkbox' :
							$all_meta_values = $wpdb->get_results ( "
									SELECT distinct(a.meta_value) as value, count(a.meta_value) as count FROM  " . $wpdb->prefix . "postmeta as a, " . $wpdb->prefix . "posts as b
									where
									b.ID = a.post_id AND
									b.post_type='" . $post_type . "' AND
									a.meta_key = '" . trim ( $tax_name ) . "'
									GROUP BY
									a.meta_value
									" );
							$final_str .= '<div class="checkbox_container_' . $theme_number . '"><ul>';
							if (sizeof ( $all_meta_values )) {
								foreach ( $all_meta_values as $meta_value ) {
									$final_str .= "<li><input type='checkbox' name='x_cf_" . trim ( $tax_name ) . "[]' value=\"$meta_value->value\" onchange=\"\">" . $meta_value->value;
									if ($show_counts == 1)
										$final_str .= "(" . $meta_value->count . ")";
									$final_str .= "</li>";
								}
							}
							$final_str .= '</ul></div>';
							break;
						case 'Radio' :
							$all_meta_values = $wpdb->get_results ( "
									SELECT distinct(a.meta_value) as value, count(a.meta_value) as count FROM  " . $wpdb->prefix . "postmeta as a, " . $wpdb->prefix . "posts as b
									where
									b.ID = a.post_id AND
									b.post_type='" . $post_type . "' AND
									a.meta_key = '" . trim ( $tax_name ) . "'
									GROUP BY
									a.meta_value
									" );
							$final_str .= '<div class="radio_container_' . $theme_number . '"><ul>';
							if (sizeof ( $all_meta_values )) {
								foreach ( $all_meta_values as $meta_value ) {
									$final_str .= "<li><input type='radio' name='cf_" . trim ( $tax_name ) . "' value=\"$meta_value->value\" onchange=\"\">" . $meta_value->value;
									if ($show_counts == 1)
										$final_str .= "(" . $meta_value->count . ")";
									$final_str .= "</li>";
								}
							}
							$final_str .= '</ul></div>';
							break;
						case 'Simple+Slider' :
							$final_str .= '
					<div class="controls_' . $theme_number . '">
						<div class="slider_' . $theme_number . ' simple_slider" data-step="' . $slider_step . '" data-min="' . $slider_min_value . '" data-max="' . $slider_max_value . '">
							<div class="amount_' . $theme_number . '"></div>
							<div class="slide"></div>
						</div>
						<input type="hidden" class="slider_min" name="max_cf_' . trim ( $tax_name ) . '"value="' . ( int ) $slider_min_value . '"/>
					</div>';
							break;
						case 'Range+Slider' :
							$final_str .= '
					<div class="controls_' . $theme_number . '">
						<div class="slider" id="slider_' . trim ( $tax_name ) . '" data-step="' . ( int ) $slider_step . '" data-min="' . ( int ) $slider_min_value . '" data-max="' . $slider_max_value . '" data-range="true" data-rangestart="' . ( int ) $slider_min_value . '" data-rangestop="' . ( int ) $slider_max_value . '">						<div class="amount slider_amount"></div>						<div class="slide"></div>
					</div>					
					<input type="hidden" name="range_cf_' . trim ( $tax_name ) . '"value="' . ( int ) $slider_min_value . '-' . ( int ) $slider_max_value . '"/>
					</div>';
							break;
						default :
							break;
					}
				}
				/*
				 * END DISPLAY
				 */
				$final_str .= "</div>";
			}
			$final_str .= "\n\t</div>\n</div>";
		}
		/*
		 * close each column
		 */

				/* close each row */
					
				$final_str .= "\n</div>";
	}
	$final_str .= "\n</form>";
	$final_str .= "\n</div>";
} else {
	exit ();
}