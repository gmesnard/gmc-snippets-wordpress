<?php 
$doc_root = urldecode($_REQUEST['doc_root']);
require_once($doc_root."/wp-load.php");

if(isset($_REQUEST['display_style'])) $display_style = $_REQUEST['display_style'];
$tax_name = $hide_empty = '';
if(isset($_REQUEST['tax_name'])) $tax_name = $_REQUEST['tax_name'];
if(isset($_REQUEST['hide_empty'])) $hide_empty = $_REQUEST['hide_empty'];
if(isset($_REQUEST['show_counts '])) $show_counts  = $_REQUEST['show_counts'];

if (strpos($tax_name, 't_') === 0 ) {
	$tax_name[0] = ''; $tax_name[1] = '';
	$meta_type="taxonomy";
}

if (strpos($tax_name, 'cf_') === 0 ) {
	$tax_name[0] = ''; $tax_name[1] = ''; $tax_name[2] = '';
	$meta_type="custom field";
}

if (strpos($tax_name, 'acf_') === 0 ) {
	$tax_name[0] = ''; $tax_name[1] = ''; $tax_name[2] = ''; $tax_name[3] = '';
	$meta_type="advanced custom field";
}
$tax_name  = trim ($tax_name);

$taxonomy = get_taxonomy($tax_name); 
// print_r($taxonomy);

$all_categories = get_categories( array('taxonomy' => $tax_name, 'hide_empty'=> $hide_empty));



function display_form_field ($type = 'Select', $name = '', $categories = array()) {
	global $tax_name, $meta_type, $hide_empty, $show_counts, $taxonomy;
	if (isset($_REQUEST['slider_step_default']) && $_REQUEST['slider_step_default']) $slider_step = $_REQUEST['slider_step_default']; else $slider_step = $_REQUEST['slider_step'];
	if (isset($_REQUEST['slider_min_value_default']) && $_REQUEST['slider_min_value_default']) $slider_min_value = $_REQUEST['slider_min_value_default']; else $slider_min_value= $_REQUEST['slider_min_value'];
	if (isset($_REQUEST['slider_max_value_default']) && $_REQUEST['slider_max_value_default']) $slider_max_value = $_REQUEST['slider_max_value_default']; else $slider_max_value = $_REQUEST['slider_max_value'];
	
	switch ($type) {
		case 'Select':
			echo "<select name='$name' class='taxonomy_select'>";
			foreach($categories as $category) {
				echo "<option value=\"$category->term_id\">$category->name</option>";
			} 
			echo "</select>";
			break;
		case 'Search Field':
				echo "<input type='text' name='".$name."' class='taxonomy_input'>";
			break;
		case 'Checkbox':
				echo '<div class="checkbox_container"><ul>';
				foreach($categories as $category) {
					echo "<li><input type='checkbox' name='".$name."[]' value=\"$category->term_id\">$category->name</li>";
				}
				echo '</ul></div>';
				break;
		case 'Radio':
			echo '<div class="checkbox_container"><ul>';
			foreach($categories as $category) {
				echo "<li><input type='radio' name='".$name."' value=\"$category->term_id\">$category->name</li>";
			}
			echo '</ul></div>';
			break;
		case 'Simple Slider':
			echo '
			<div class="controls">
				<div class="slider" data-step="'.$slider_step.'" data-min="'.$slider_min_value.'" data-max="'.$slider_max_value.'" data-range="false" data-rangestart="'.$slider_min_value.'" data-rangestop="'.$slider_max_value.'">
					<div class="amount"></div>
					<div class="slide"></div>	
				</div>
			</div>';
			break;	
		case 'Range Slider':
			echo '
			<div class="controls">
				<div class="slider" data-step="'.$slider_step.'" data-min="'.$slider_min_value.'" data-max="'.$slider_max_value.'" data-range="true" data-rangestart="'.$slider_min_value.'" data-rangestop="'.$slider_max_value.'">
					<div class="amount"></div>
					<div class="slide"></div>	
				</div>
			</div>';
			break;
	}

	echo "\n
	
	<input type='hidden' name= 'meta_taxonomy[][][][item_type]' value='$meta_type' />
	<input type='hidden' name= 'meta_taxonomy[][][][tax_name]' value='".$tax_name."' />
	<input type='hidden' name= 'meta_taxonomy[][][][display_style]' value='".$_REQUEST['display_style']."' />
	<input type='hidden' name= 'meta_taxonomy[][][][custom_label]' value='".$_REQUEST['custom_label']."' />";
	
	if ($type != "Search Field") {
		echo "
		<input type='hidden' name= 'meta_taxonomy[][][][hide_empty]' value='".$hide_empty."' />
		<input type='hidden' name= 'meta_taxonomy[][][][show_counts]' value= '".$show_counts."'/>";
	
		
	}
	if ($type == "Simple Slider" || $type == "Range Slider"){
		echo "
		<input type='hidden' name='meta_taxonomy[][][][slider_min_value]'  value='".$slider_min_value."' />
		<input type='hidden' name='meta_taxonomy[][][][slider_max_value]'  value='".$slider_max_value."' />
		<input type='hidden' name='meta_taxonomy[][][][slider_step]'  value='".$slider_step."' />";	
	}
	echo "</script>";
	

	
	
}

echo '<div class="individual_taxonomy">';

echo "<script>counter_".$_REQUEST['grid_row']."_".$_REQUEST['grid_column']."++;</script>";
if (isset($_REQUEST['button_type']) && strlen($_REQUEST['button_type']) > 2) {
echo '<input type="'.$_REQUEST['button_type'].'" value="'.$_REQUEST['button_label'].'" class="button-submit" />
	<i class="icon-remove" onclick="jQuery(this).parent().remove();"></i>';
echo "	<input type='hidden' name= 'meta_taxonomy[][][][item_type]' value='".$_REQUEST['button_type']."' />
		<input type='hidden' name= 'meta_taxonomy[][][][label]' value='".$_REQUEST['button_label']."' />";
	
} else {
	if (isset ($_REQUEST['custom_label']) && strlen(trim($_REQUEST['custom_label'])) > 0) {
		$label = $_REQUEST['custom_label'];
	} else {
		$label = $taxonomy->labels->name;
	}
	echo '
		<span class="tax_label">'.$label.'</span>';
	display_form_field( $display_style, $tax_name, $all_categories);
	
	echo '<i class="icon-remove" onclick="jQuery(this).parent().remove();"></i>';
	
}
echo '</div>';
// print_r($_REQUEST);
?>
