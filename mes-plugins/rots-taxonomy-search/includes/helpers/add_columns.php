<?php 
$doc_root = urldecode($_REQUEST['doc_root']);
$post_type = $_REQUEST['custom_post_type'];

require_once($doc_root."/wp-load.php");
$dir_path = plugin_dir_url ( __FILE__ );

query_posts (array( 'post_type' => $post_type));
the_post();
$post_id = get_the_id();
$custom_fields = get_post_custom_keys($post_id);
$acf_fields = get_fields();

// print_r($acf_fields);

$cols = explode(":", $_REQUEST['cols']);

$args=array(
		'object_type' => array($post_type)
);

$output = 'objects'; // or objects
$operator = 'or'; // 'and' or 'or'
$taxonomies=get_taxonomies($args,$output,$operator);
$i = 0;
foreach($cols as $col) {
	$i++
	?>
	<div class="span<?php echo $col; ?>" style="border:1px dotted #ccc;">
	<script>
		var counter_<?php echo $_REQUEST['row']; ?>_<?php echo $i; ?> = 0;
		jQuery('<input>').attr({
			type: 'hidden',
			name: 'meta_grid[<?php echo $_REQUEST['row'];?>][<?php echo $i;?>]',
			value: '<?php echo $col; ?>'
			}).appendTo('#real_builder_form');
	</script>
	
	<div class="tax_result"></div>
	<div class="tax_form" style="display: none;">
			<ul class="tabs tabs-inline tabs-top">
				<li class="active" onclick="jQuery(this).parent().children().toggleClass('active');jQuery('#form_tabs_<?php echo $_REQUEST['row']; ?>_<?php echo $i; ?>').children().slideToggle();">
					<a data-toggle="tab"><i class="icon-inbox"></i> Taxonomy</a>
				</li>
				<li onclick="jQuery(this).parent().children().toggleClass('active');jQuery('#form_tabs_<?php echo $_REQUEST['row']; ?>_<?php echo $i; ?>').children().slideToggle();">
					<a data-toggle="tab"><i class="icon-inbox"></i> Button</a>
				</li>
			</ul>
			<div id="form_tabs_<?php echo $_REQUEST['row']; ?>_<?php echo $i; ?>" class="tab-content padding tab-content-inline tab-content-bottom">
				<div>
					<form method="post" class="tax_form_add" id="tax_form_add_<?php echo $_REQUEST['row']; ?>_<?php echo $i; ?>">
					<div class="form-horizontal">
						<input type="hidden" value="<?php echo $_REQUEST['row']; ?>" name="grid_row" />					 
						<input type="hidden" value="<?php echo $i; ?>" name="grid_column" />
						
						<div class="control-group" style="margin-bottom:23px;">
							<label  class="control-label tax-label">Taxonomy:</label> 
							<div class="controls">
							<select name="tax_name" class="tax-name">
								<optgroup label="Taxonomies">
								<?php foreach($taxonomies as $taxonomy) {
									?><option value="t_<?php echo $taxonomy->name; ?>"><?php echo $taxonomy->label; ?></option><?php
								}?>
								</optgroup>
								<optgroup label="Custom Fields">
								<?php if($custom_fields) {
									foreach($custom_fields as $k=>$v) {
										?><option value="cf_<?php echo $v; ?>"><?php echo $v; ?></option><?php
									}
								}?>
								</optgroup>
								<optgroup label="Advanced Custom Fields">
								<?php 
								if($acf_fields) {
									foreach($acf_fields as $k=>$v) {
										$field = get_field_object($k);
										?><option value="acf_<?php echo $field['name']; ?>"><?php echo $field['label']; ?></option><?php
																	}
								}
								?>
								</optgroup>
							</select>
							</div>
						</div>
						<div class="control-group">
							<label class="control-label">Display Style:</label>
							<div class="controls">
							<select name="display_style" onchange= "updateForm(jQuery(this));">
								<option>Select</option>
								<option>Radio</option>
								<option>Checkbox</option>
								<option>Simple Slider</option>
								<option>Range Slider</option>
								<option>Search Field</option>
							</select>
							</div>
						</div>
						<div class="simple_slider" style="display:none;">
							<div class="control-group">
								<label class="control-label" style="vertical-align:baseline;">Min Value</label>
								<div class="controls">
									<input type="checkbox" name="slider_min_value_default" value="-1" /> 0 or 
									<input type="text" name="slider_min_value" style="max-width:120px;" />
								</div>
							</div>
							<div class="control-group">	
								<label class="control-label" style="vertical-align:baseline;">Max Value</label>
								<div class="controls">
									<input type="checkbox" name="slider_max_value_default" value="-1" /> auto-determine or 
									<input type="text" name="slider_max_value" style="max-width:120px;" />
								</div>
							</div>	
							<div class="control-group">	
								<label class="control-label" style="vertical-align:baseline;">Step</label>
								<div class="controls">
									<input type="checkbox" name="slider_step_default" value="-1" /> auto scale or 
									<input type="text" name="slider_step" style="max-width:120px;" />
								</div>
							</div>
						</div>
						
						<div class="range_slider"style="display:none;">
							<div class="control-group">
								<label class="control-label" style="vertical-align:baseline;">Min Value</label>
								<div class="controls">
									<input type="checkbox" name="slider_min_value_default" value="-1" /> auto-determine or 
									<input type="text" name="slider_min_value" style="max-width:120px;" />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" style="vertical-align:baseline;">Max Value</label>
								<div class="controls">
									<input type="checkbox" name="slider_max_value_default" value="-1" /> auto-determine or 
									<input type="text" name="slider_max_value" style="max-width:120px;" />
								</div>
							</div>
							<div class="control-group">	
								<label class="control-label" style="vertical-align:baseline;">Step</label>
								<div class="controls">
									<input type="checkbox" name="slider_step_default" value="-1" /> auto scale or 
									<input type="text" name="slider_step" style="max-width:120px;" />
								</div>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" style="vertical-align:baseline;">Custom Label:</label>
							<div class="controls">
								<input type="text" name="custom_label" style="max-width:120px;" />
							</div>
						</div>
						<div class="control-group">
							<label  class="control-label" style="vertical-align:baseline;">Show Counts</label>
							<div class="controls">
							<input type="checkbox" name="show_counts" value="1" />
							</div>
						</div>
						<div class="control-group">
							<label class="control-label" style="vertical-align:baseline;">Hide Empty Results</label>
							<div class="controls">
							<input type="checkbox" name="hide_empty" value="1" />
							</div>
						</div>
						<input type="submit" value="Add+" class="save">
						
					</div>
				 </form>
				 </div>
				<div id="button_form_<?php echo $_REQUEST['row']; ?>_<?php echo $i; ?>" style="display: none;">
				<div class="form-horizontal">
					<form method="post" id="tax_form_add_button_<?php echo $_REQUEST['row']; ?>_<?php echo $i; ?>">
						<input type="hidden" value="<?php echo $_REQUEST['row']; ?>" name="grid_row" />					 
						<input type="hidden" value="<?php echo $i; ?>" name="grid_column" />
						<div class="control-group">
							<label  class="control-label" style="vertical-align:baseline;">Button Type</label>
							<div class="controls" style="padding: 5px 0 0 10px;">
							   <div style="padding-bottom:10px;">
									<input type="radio" name="button_type" value="submit" style="border-radius: 100% !important; margin: -4px 4px 0 0;" />
									<span style="color: #477db5; font-weight:bold;">Submit</span>
							   </div>
							   <div>
									<input type="radio" name="button_type" value="reset" style="border-radius: 100% !important; margin: -4px 4px 0 0;" /> 
									<span style="color: #477db5; font-weight:bold;">Reset</span>
							   </div>
							</div>
						</div>
						<div class="control-group">
							<label  class="control-label" style="vertical-align:baseline;">Button Label: </label>
							<div class="controls">
								<input type="text" name="button_label" />
							</div>	
						</div>
						<input type="submit" value="Add+" class="save">
					</form>
					</div> 
				</div>
			</div>
		</div>
		
	<div class="add_column" onclick="jQuery(this).parent().children('.tax_form').toggle();"><i class="icon-plus"></i> Taxonomy</div>																	
	</div>
<script>
jQuery("#tax_form_add_<?php echo $_REQUEST['row']; ?>_<?php echo $i; ?>").on ('submit', function () {
	var form = jQuery(this),
	content = form.serialize();
	jQuery.ajax ( {
			url :'<?php echo $dir_path;?>process_taxonomy.php',
			dataType: 'json',
			type: 'post',
			data : content,
			success: function (data) {
				var value = { 
						doc_root: '<?php echo urlencode_deep($doc_root); ?>', 
						tax_name : data.tax_name, 
						display_style: data.display_style, 
						hide_empty : data.hide_empty, 
						button_label : data.button_label, 
						button_type : data.button_type,
						grid_row : data.grid_row,
						grid_column : 	data.grid_column,
						slider_min_value_default : 	data.slider_min_value_default,
						slider_min_value : 	data.slider_min_value,
						slider_max_value_default : 	data.slider_max_value_default,
						slider_max_value : 	data.slider_max_value,
						slider_step_default : 	data.slider_step_default,
						slider_step : 	data.slider_step,
						custom_label : 	data.custom_label,
						show_counts : 	data.show_counts,
						hide_empty : 	data.hide_empty
						};
					
			    jQuery.get("<?php echo $dir_path;?>add_taxonomy.php", value, function(data) {
				    console.log ('test');
			    	form.parent().parent().parent().parent().children('.tax_result').append(data);
			    });
				}
		});
	jQuery(".tax_form").hide();
	return false;
})

jQuery("#tax_form_add_button_<?php echo $_REQUEST['row']; ?>_<?php echo $i; ?>").on ('submit', function () {
var form = jQuery(this),
content = form.serialize();
jQuery.ajax ( {
		url :'<?php echo $dir_path;?>process_taxonomy.php',
		dataType: 'json',
		type: 'post',
		data : content,
		success: function (data) {
			var value = { 
					doc_root: '<?php echo urlencode_deep($doc_root); ?>', 
					tax_name : data.tax_name, 
					display_style: data.display_style, 
					hide_empty : data.hide_empty, 
					button_label : data.button_label, 
					button_type : data.button_type,
					grid_row : data.grid_row,
					grid_column : 	data.grid_column,
					slider_min_value_default : 	data.slider_min_value_default,
					slider_min_value : 	data.slider_min_value,
					slider_max_value_default : 	data.slider_max_value_default,
					slider_max_value : 	data.slider_max_value,
					slider_step_default : 	data.slider_step_default,
					slider_step : 	data.slider_step,
					custom_label : 	data.custom_label,
					show_counts : 	data.show_counts,
					hide_empty : 	data.hide_empty
					};
				
		    jQuery.get("<?php echo $dir_path;?>add_taxonomy.php", value, function(data) {
			    console.log ('test_1');
		    	form.parent().parent().parent().parent().parent().children('.tax_result').append(data);
		    });
			}
	});
jQuery(".tax_form").hide();
return false;
})
</script>
	<?php 
	
}
?>

<script>

function updateForm(field) {
	var container = field.parent().parent().parent(); 
	switch (field.val()) {
		case "Simple Slider" :
			container.children(".range_slider").hide();
			container.children(".simple_slider").show();
			break;
		case "Range Slider" :
			container.children(".simple_slider").hide();
			container.children(".range_slider").show();
			break;
		default:
			container.children(".simple_slider").hide();
			container.children(".range_slider").hide();
		}
}
</script>