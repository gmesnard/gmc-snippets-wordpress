<?php 
$doc_root = urldecode($_REQUEST['doc_root']);
$post_type = $_REQUEST['custom_post_type'];
require_once($doc_root."/wp-load.php");
$dir_path = plugin_dir_url ( __FILE__ );
$files = strstr($dir_path,'/includes',true).'/files/';
?>
<div class="row-fluid builder" id="row_<?php echo $_REQUEST['var1'];?>" style="border: 1px solid #ccc; padding: 5px; margin: 1px;">
 <div class="row-menu">
 	<div class="row-button" onclick="jQuery(this).parents('#row_<?php echo $_REQUEST['var1'];?>').children('.form').toggle();"><i class="icon-th-large"></i></div>
	<div class="row-button" onclick="jQuery('#row_<?php echo $_REQUEST['var1'];?>').remove();"><i class="icon-remove"></i></div>
 </div>
<form class="form" style="" method="post">
<input type="hidden" name="colls" value="" />
<div class="col-type">
 <div class="col12" onclick="jQuery('.col-type').removeClass('selected'); jQuery(this).parent().addClass('selected'); jQuery(this).parent().parent().children('input[type=hidden]').val('12'); jQuery(this).parent().submit();"></div>
</div>
<div class="col-type">
<div class="col8-4" onclick="jQuery('.col-type').removeClass('selected'); jQuery(this).parent().addClass('selected'); jQuery(this).parent().parent().children('input[type=hidden]').val('8:4'); jQuery(this).parent().submit();"></div>
</div>
<div class="col-type">
 <div class="col6-6" onclick="jQuery('.col-type').removeClass('selected'); jQuery(this).parent().addClass('selected'); jQuery(this).parent().parent().children('input[type=hidden]').val('6:6'); jQuery(this).parent().submit();"></div>
</div>
<div class="col-type">
<div class="col4-8" onclick="jQuery('.col-type').removeClass('selected'); jQuery(this).parent().addClass('selected'); jQuery(this).parent().parent().children('input[type=hidden]').val('4:8'); jQuery(this).parent().submit();"></div>
</div>
<div class="col-type">
<div class="col4-4-4" onclick="jQuery('.col-type').removeClass('selected'); jQuery(this).parent().addClass('selected'); jQuery(this).parent().parent().children('input[type=hidden]').val('4:4:4'); jQuery(this).parent().submit();"></div>
</div>
<div class="col-type">
 <div class="col3-6-3" onclick="jQuery('.col-type').removeClass('selected'); jQuery(this).parent().addClass('selected'); jQuery(this).parent().parent().children('input[type=hidden]').val('3:6:3'); jQuery(this).parent().submit();"></div>
</div>
</form>
<div class="columns"></div>
</div>


<script>
var row_post_type= '<?php echo $post_type; ?>';
jQuery(".form").on ('submit', function () {
	var form = jQuery(this),
		content = form.serialize();

	jQuery.ajax ( {
			url :'<?php echo $dir_path;?>process_columns.php',
			dataType: 'json',
			type: 'post',
			data : content,
			success: function (data) {
				
				form.parent().children('.columns').load("<?php echo $dir_path;?>add_columns.php?doc_root=<?php echo urlencode_deep($doc_root); ?>&cols="+data.columns+"&custom_post_type="+row_post_type+"&row="+<?php echo $_REQUEST['var1']; ?>);
				}
		});
	form.hide();
	return false;
});
</script>