<?php 

// if (isset ($_REQUEST['form_id'])) {
// 	if ($_REQUEST['form_id'] == 30 || $_REQUEST['form_id'] == 31 ) die();
// }

function display_form_field($data_array) {
	if (isset ( $data_array ['display_style'] ))
		$display_style = $data_array ['display_style'];
	$tax_name = $hide_empty = '';
	if (isset ( $data_array ['tax_name'] ))
		$tax_name = $data_array ['tax_name'];
	if (isset ( $data_array ['hide_empty'] ))
		$hide_empty = $data_array ['hide_empty'];
	if (isset ( $data_array ['show_counts '] ))
		$show_counts = $data_array ['show_counts'];
	if (strpos ( $tax_name, 't_' ) === 0) {
		$tax_name [0] = '';
		$tax_name [1] = '';
		$meta_type = "taxonomy";
	}
	if (strpos ( $tax_name, 'cf_' ) === 0) {
		$tax_name [0] = '';
		$tax_name [1] = '';
		$tax_name [2] = '';
		$meta_type = "custom field";
	}
	$tax_name = trim ( $tax_name );
	$taxonomy = get_taxonomy ( $tax_name );
	if (isset ( $data_array ['slider_step_default'] ) && $data_array ['slider_step_default'])
		$slider_step = $data_array ['slider_step_default'];
	else
		$slider_step = $data_array ['slider_step'];
	if (isset ( $data_array ['slider_min_value_default'] ) && $data_array ['slider_min_value_default'])
		$slider_min_value = $data_array ['slider_min_value_default'];
	else
		$slider_min_value = $data_array ['slider_min_value'];
	if (isset ( $data_array ['slider_max_value_default'] ) && $data_array ['slider_max_value_default'])
		$slider_max_value = $data_array ['slider_max_value_default'];
	else
		$slider_max_value = $data_array ['slider_max_value'];
	switch ($display_style) {
		case 'Select' :
			echo "<select class='taxonomy_select'>";
			echo "<option>Option 1</option>";
			echo "<option>Option 2</option>";
			echo "<option>Option 3</option>";
			echo "</select>";
			break;
		case 'Search+Field' :
			echo "<input type='text' class='taxonomy_input'>";
			break;
		case 'Checkbox' :
			echo '<div class="checkbox_container"><ul>';
			echo "<li><input type='checkbox'>Option 1</li>";
			echo "<li><input type='checkbox'>Option 2</li>";
			echo "<li><input type='checkbox'>Option 3</li>";
			echo '</ul></div>';
			break;
		case 'Radio' :
			echo '<div class="checkbox_container"><ul>';
			echo "<li><input type='radio'>Option 1</li>";
			echo "<li><input type='radio'>Option 2</li>";
			echo "<li><input type='radio'>Option 3</li>";
			echo '</ul></div>';
			break;
		case 'Simple+Slider' :
			echo '
			<div class="controls">
			<div class="slider" data-step="1" data-min="0" data-max="100">
			<div class="amount"></div>
			<div class="slide"></div>
			</div>';
			break;
		case 'Range+Slider' :
			echo '
			<div class="controls">
			<div class="slider" data-step="1" data-min="0" data-max="100" data-range="false" data-rangestart="15" data-rangestop="65">
			<div class="amount"></div>
			<div class="slide"></div>
			</div>';
			break;
	}
	echo "\n

	<input type='hidden' name= 'meta_taxonomy_item_type' value='$meta_type' />
	<input type='hidden' name= 'meta_taxonomy_tax_name' value='" . $data_array ['tax_name'] . "' />
	<input type='hidden' name= 'meta_taxonomy_display_style' value='" . $data_array ['display_style'] . "' />
	<input type='hidden' name= 'meta_taxonomy_custom_label' value='" . $data_array ['custom_label'] . "' />";
	if ($display_style != "Search+Field") {
	echo "
	<input type='hidden' name= 'meta_taxonomy_hide_empty' value='" . $hide_empty . "' />
	<input type='hidden' name= 'meta_taxonomy_show_counts' value= '" . $data_array['show_counts']. "'/>";
	}
	if ($display_style == "Simple+Slider" || $display_style == "Range+Slider") {
	echo "
	<input type='hidden' name='meta_taxonomy_slider_min_value'  value='" . $slider_min_value . "' />
	<input type='hidden' name='meta_taxonomy_slider_max_value'  value='" . $slider_max_value . "' />
	<input type='hidden' name='meta_taxonomy_slider_step'  value='" . $slider_step . "' />";
	}
	echo "</script>";
	}

global $wpdb;

$doc_root = str_replace("/wp-admin/admin.php", '', $_SERVER['SCRIPT_FILENAME']);
$filter_theme = 'theme8.css';
$search_template = $logical_cond = $autosearch = $form_post_type = $row_count = $form = $metas = null;
if (isset($_REQUEST['form_id'])) {
		$i_tax_search_id = $_REQUEST['form_id'];
		$metas = $wpdb->get_results ( "SELECT *
			FROM {$wpdb->prefix}its_formmeta
			WHERE {$wpdb->prefix}its_formmeta.form_id = $i_tax_search_id order by meta_row" );
		$form = $wpdb->get_results ( "SELECT *
			FROM {$wpdb->prefix}its_forms
			WHERE {$wpdb->prefix}its_forms.id = $i_tax_search_id" ); 
		foreach($metas as $meta) {
			if ($meta->meta_name == 'filter_theme') $filter_theme = $meta->meta_value;
			if ($meta->meta_name == 'search_template') $search_template = $meta->meta_value;
			if ($meta->meta_name == 'logical_cond') $logical_cond = $meta->meta_value;
			if ($meta->meta_name == 'autosearch') $autosearch = $meta->meta_value;
			if ($meta->meta_name == 'post_type') $form_post_type = $meta->meta_value;
			if ($meta->meta_name == 'row_count') $row_count = $meta->meta_value;
			if ($meta->meta_name == 'cell_size') $cell_size[$meta->meta_row] = $meta->meta_value;
						
		}
	}
	else 
		$i_tax_search_id= -1;
?>

<link id="mainStylesheet" href="<?php echo plugins_url(); ?>/rots-taxonomy-search/files/css/admin_themes/<?php echo $filter_theme; ?>" type="text/css" rel="stylesheet">
<div id="main">

<form method="post" id="real_builder_form">
			<input type="hidden" name="form_id" class="form_id" value="<?php echo $i_tax_search_id;?>" />
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h1><?php if ($i_tax_search_id == -1) echo "Add "; else echo "Edit "?>Taxonomy Search Form</h1>
					</div>
				</div>
				
				
				<div class="row-fluid" >
					<div class="span12">
						<div class="box">
							<div class="box-title">
								<h3>
									<i class="icon-cogs"></i>
									Settings
								</h3>
								<div class="actions">
									<a onclick="jQuery('#settings_form').slideToggle();"  class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
								</div>
							</div>
							<div class="box-content" style="" id="settings_form">
							<div class="form-horizontal">
								<div class="control-group">
										<label for="textfield" class="control-label">Filter Name</label>
										<div class="controls">
											<input type="text" name="textfield" id="textfield" class="filed-tax-name" placeholder="Text input" class="input-xlarge" onkeyup="jQuery('#real_form_name').val(jQuery(this).val());" value="<?php if ($i_tax_search_id != -1) echo $form[0]->name; ?>">
										</div>
									</div>
								<div class="control-group">
										<label for="textfield" class="control-label">Filter Theme:</label>
										<div class="controls">
										<select name="filter_theme" id="filter_theme" onchange="jQuery('#real_form_theme').val(jQuery(this).val());">
											<?php 
											$dir = $doc_root."/wp-content/plugins/rots-taxonomy-search/files/css/admin_themes";
											
									
											if (is_dir($dir)){
												if ($dh = opendir($dir)){
													while (($file = readdir($dh)) !== false){
														$files_array[] = $file;
													}
													closedir($dh);
												}
											}
											sort($files_array);
											foreach($files_array as $file) {
												if (strlen($file) > 2) {
													if ($filter_theme == $file) $selected = ' selected="selected" '; else $selected = '';
													echo "<option $selected>" . $file . "</option>";
												}
											}
											?>
										</select>
										</div>
									</div>
								<div class="control-group">
										<label for="textfield" class="control-label">Filter Result Template:</label>
										<div class="controls">
										  <select id="search_template" name="search_template" >
												<option <?php if ($search_template == 'is_search') echo ' selected="selected"'; ?> value="is_search"> Search Results </option>
												<option <?php if ($search_template == 'is_archive') echo ' selected="selected"'; ?> value="is_archive"> Archive </option>
												<option <?php if ($search_template == 'is_category') echo ' selected="selected"'; ?> value="is_category"> Category </option>
												<option <?php if ($search_template == 'is_tag') echo ' selected="selected"'; ?> value="is_tag"> Tag Archive </option>
												<option <?php if ($search_template == 'is_tax') echo ' selected="selected"'; ?> value="is_tax"> Taxonomy Archive </option>
												<option <?php if ($search_template == 'is_post_type_archive') echo ' selected="selected"'; ?> value="is_post_type_archive"> Post Type Archive </option>
												<option <?php if ($search_template == 'is_home') echo ' selected="selected"'; ?> value="is_home"> Blog Home Page </option>
                                            <?php
                                                $templates = get_page_templates();
                                                if(isset($templates) && count($templates)>0){
                                                     foreach($templates as $template_name => $template_page){
                                                     	if ($template_page == $search_template) $selected = ' selected="selected" '; else $selected = '';
                                                        ?>
												<option value="<?php echo $template_page;?>" <?php echo $selected;?> ><?php echo $template_name;?></option>
                                                        <?php
                                                     }
                                                }
                                            ?>
                                        </select>
										
										</div>
									</div>
									
								<div class="control-group">
									<label  class="control-label" style="vertical-align:baseline;">Logical Condition</label>
									<div class="controls">
									<select name="logical_cond" onchange="jQuery('#real_form_logical_cond').val(jQuery(this).val());">
										<option <?php if ($logical_cond == 'AND') echo 'selected="selected"';?>>AND</option>
										<option <?php if ($logical_cond == 'OR') echo 'selected="selected"';?>>OR</option>
									</select>
									</div>
								</div>	
<!-- 																
								<div class="control-group">
										<label for="textfield" class="control-label">Auto Search (Activate)</label>
										<div class="controls">
											<select onchange="jQuery('#real_form_autosearch').val(jQuery(this).val());" >
											<option value='0' <?php if ($autosearch == 0) echo 'selected="selected"';?>>No</option>
											<option value='1' <?php if ($autosearch == 0) echo 'selected="selected"';?>>Yes</option>
											</select>
										</div>
									</div>
 -->									
								</div>				
							</div>
						</div>
					</div>
				</div>
						
				<div class="row-fluid" >
					<div class="span12">
						<div class="box box-bordered">
							<div class="box-title" style="background: #ededed;">
								<h3>
									<i class="icon-tags"></i>
									Select Post Type
								</h3>
								<div class="actions">
									<a onclick="jQuery('#content_type_box').slideToggle();"  class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
								</div>
							</div>
							<div class="box-content" style="" id="content_type_box">
							<div class="form-horizontal">
								<div class="control-group">
										<label for="textfield" class="control-label">Custom Post Type</label>
										<div class="controls">
											<select name="post_type" id="post_type" onchange="jQuery('#real_form_post_type').val(jQuery(this).val());jQuery('#visual_builder').children('.builder').remove();">
											<?php 
											$all_post_types = get_post_types (array('public' => true));
											 
											foreach($all_post_types as $post_type) {
												if ($form_post_type == $post_type) $selected=' selected="selected"'; else $selected='';
												?>
												<option value="<?php echo $post_type; ?>" <?php echo $selected?>><?php echo ucfirst($post_type); ?></option>
												<?php
											}
											?>
											</select>
										</div>
									</div>
								</div>	
								
												
								
							</div>
						</div>
					</div>
				</div>
						
				<div class="row-fluid">
					<div class="span12">
							<div class="box box-bordered">
								<div class="box-title" style="background: #ededed;">
									<h3>
										<i class="icon-pencil"></i>
										Visual Builder
									</h3>
									<div class="actions">
										<a onclick="jQuery('#visual_builder_box').slideToggle();"  class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
									</div>
								</div>
								<div class="box-content" style="" id="visual_builder_box">
									
									<div id="visual_builder">
									<?php 
									if ($i_tax_search_id) {
										for($i = 1; $i<=$row_count; $i++) {
											?>
											<div class="row-fluid builder" style="border: 1px solid #ccc; padding: 5px; margin: 1px;">
												<input type="hidden" class="cell_size" name="cell_size[]" value="<?php echo $cell_size[$i];?>">
												<div class="row-menu">
													<div class="row-button" onclick="jQuery(this).parent().parent().find('.form').show('slow');">
														<i class="icon-th-large"></i>
													</div>
													<div class="row-button" onclick="jQuery(this).parent().parent().remove();">
														<i class="icon-remove"></i>
													</div>
												</div>
												<div class="form" style="display: none;">
													<div class="col-type" <?php if ($cell_size[$i] == '12') echo 'selected'; ?>>
														<div class="col12" onclick="changeColumns(this, '12');"></div>
													</div>
													<div class="col-type" <?php if ($cell_size[$i] == '8:4') echo 'selected'; ?>>
														<div class="col8-4" onclick="changeColumns(this, '8:4');"></div>
													</div>
													<div class="col-type  <?php if ($cell_size[$i] == '6:6') echo 'selected'; ?>">
														<div class="col6-6" onclick="changeColumns(this, '6:6');"></div>
													</div>
													<div class="col-type"  <?php if ($cell_size[$i] == '4:8') echo 'selected'; ?>>
														<div class="col4-8" onclick="changeColumns(this, '4:8');"></div>
													</div>
													<div class="col-type"  <?php if ($cell_size[$i] == '4:4:4') echo 'selected'; ?>>
														<div class="col4-4-4" onclick="changeColumns(this, '4:4:4');"></div>
													</div>
													<div class="col-type"  <?php if ($cell_size[$i] == '3:6:3') echo 'selected'; ?>>
														<div class="col3-6-3" onclick="changeColumns(this, '3:6:3');"></div>
													</div>
												</div>
												<div class="columns">
												<?php 
												$cell_size_array = explode(':', $cell_size[$i]);
												foreach($cell_size_array as $j=>$size) {
													//$i = row; $coll = coll; $k= order;
													$coll = $j+1;
													$tax_count = $wpdb->get_results ( "SELECT distinct({$wpdb->prefix}its_formmeta.meta_position) as position
															FROM {$wpdb->prefix}its_formmeta
															WHERE 
															{$wpdb->prefix}its_formmeta.form_id = $i_tax_search_id AND 
															{$wpdb->prefix}its_formmeta.meta_row = $i AND
															{$wpdb->prefix}its_formmeta.meta_col = $coll
															order by {$wpdb->prefix}its_formmeta.meta_position" );				
												?>
													<div class="span<?php echo $size?>" style="border: 1px dotted #ccc;">
														<div class="tax_result ui-sortable" style="min-height: 50px; border: 1px dotted;">

<?php 
if (count($tax_count)) {
	foreach($tax_count as $k) {
		$data_array = array(
				'item_type'=>null,
				'tax_name'=>null,
				'display_style'=>null,
				'custom_label'=>null,
				'hide_empty'=>null,
				'show_counts'=>null,
				'label'=>null,
				'slider_min_value'=>null,
				'slider_max_value'=>null,
				'slider_step'=>null,
				);
		$data_obj_array = $wpdb->get_results ( "SELECT *
		FROM {$wpdb->prefix}its_formmeta
		WHERE
		{$wpdb->prefix}its_formmeta.form_id = $i_tax_search_id AND
		{$wpdb->prefix}its_formmeta.meta_row = $i AND
		{$wpdb->prefix}its_formmeta.meta_col = $coll AND 
		{$wpdb->prefix}its_formmeta.meta_position = $k->position" );
		
		foreach($data_obj_array as $obj) {
			$data_array[$obj->meta_name] = $obj->meta_value;
 		}

		if (isset ( $data_array ['display_style'] ))
			$display_style = $data_array ['display_style'];
		$tax_name = $hide_empty = '';
		if (isset ( $data_array ['tax_name'] ))
			$tax_name = $data_array ['tax_name'];
		if (isset ( $data_array ['hide_empty'] ))
			$hide_empty = $data_array ['hide_empty'];
		if (isset ( $data_array ['show_counts '] ))
			$show_counts = $data_array ['show_counts'];
		if (strpos ( $tax_name, 't_' ) === 0) {
			$tax_name [0] = '';
			$tax_name [1] = '';
			$meta_type = "taxonomy";
		}
		$data_array ['custom_label'] = urldecode ( $data_array ['custom_label'] );
		if (strpos ( $tax_name, 'cf_' ) === 0) {
			$tax_name [0] = '';
			$tax_name [1] = '';
			$tax_name [2] = '';
			$meta_type = "custom field";
		}
		$tax_name = trim ( $tax_name );
		$taxonomy = get_taxonomy ( $tax_name );
		$all_categories = get_categories ( array ('taxonomy' => $tax_name, 'hide_empty' => $hide_empty ) );
			echo '<div class="individual_taxonomy">';
			echo '<br />';
			if (isset ( $data_array ['item_type'] ) && ( $data_array ['item_type'] == 'submit' ||$data_array ['item_type'] == 'reset' ) ) {
					echo '<button class="button-submit">' . $data_array ['label'] . '</button>
					<i class="icon-move"></i><i class="icon-remove" onclick="jQuery(this).parent().remove();"></i>';
					echo "	<input type='hidden' name= 'meta_taxonomy_item_type' value='" . $data_array ['item_type'] . "' />
					<input type='hidden' name= 'meta_taxonomy_label' value='" . $data_array ['label'] . "' />";
			} else {
			if (isset ( $data_array ['custom_label'] ) && strlen ( trim ( $data_array ['custom_label'] ) ) > 0) {
			$label = $data_array ['custom_label'];
			} else {
			$label = $taxonomy->labels->name;
			}
			echo '
			<span class="tax_label">' . $label . '</span>';
			display_form_field ( $data_array );
			echo '<i class="icon-move"></i><i class="icon-remove" onclick="jQuery(this).parent().remove();"></i>';
			}
			echo '</div>';
	}
}
?>														
														</div>
														<div class="tax_form"></div>
													
														<div class="add_column plus" onclick="loadTaxonomyForm(this);">
															<i class="icon-plus"></i> Taxonomy
														</div>
														<div class="add_column minus" onclick="destroyTaxonomyForm(this);" style="display: none;">
															<i class="icon-minus"></i> Taxonomy
														</div>
													</div>
													<?php }?>
											</div>
											</div>
											<?php 
										}
									}
									?>
									</div>		
									<div id="add_row"><i class="icon-plus"></i> Section</div>
		
								</div>
							</div>
						</div>
				</div>		
			
							
			
			<div class="row-fluid" style="margin: 20px auto; text-align:center;"> 
			<!-- build the form -->
			
				<input type="hidden" name="root_dir" id="root_dir" value="<?php echo $doc_root; ?>" />
				<input type="hidden" name="real_form_name" id="real_form_name" value="<?php if ($i_tax_search_id != -1) echo $form[0]->name; ?>" />
				<input type="hidden" name="real_form_post_type" id="real_form_post_type" value="post" />
				<input type="hidden" name="real_form_theme" id="real_form_theme" value="theme1.css" />
				<input type="hidden" name="real_form_restul_template" id="real_form_restul_template" value="search.php" />
				<input type="hidden" name="real_form_logical_cond" id="real_form_logical_cond" value="AND" />
				<input type="hidden" name="real_form_autosearch" id="real_form_autosearch" value="<?php if ($autosearch == 0) echo 0; else echo 1;?>" />
				<input type="submit" value="save" class="save-button" />
			</div>	
			</form>
			<!-- build the form -->
			</div>
	
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script>							

jQuery(document).ready(function(){
	activateSliders();
	sortableItems();
        jQuery('#filter_theme').change(function(e){
            console.log('change theme');
            jQuery('#mainStylesheet').prop('href', '<?php echo plugins_url(); ?>/rots-taxonomy-search/files/css/admin_themes/'+jQuery(this).val());
        });
    });
</script>