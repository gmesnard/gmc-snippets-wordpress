<?php
/*************************DELETE****************************/

function deleteForm($the_id){
    //DELETE FORM FUNCTION
    global $wpdb;
    $wpdb->query("DELETE FROM {$wpdb->prefix}its_forms WHERE id=$the_id");
    $wpdb->query("DELETE FROM {$wpdb->prefix}its_formmeta WHERE form_id=$the_id");
}

//MULTIPLE ITEMS DELETE
if(isset($_POST['deleteButton'])){
      if(isset($_POST['deleteItem']) && is_array($_POST['deleteItem']) && count($_POST['deleteItem'])>0){
              	foreach($_POST['deleteItem'] as $the_id){
                    deleteForm($the_id);
        	}
        $succefully_delete = true;
      }
}
//SINGLE ITEM DELETE
if(isset($_POST['deleteID']) && $_POST['deleteID']!=''){
          deleteForm($_POST['deleteID']);
}

/***************************SELECT FORMS***************************/
global $wpdb;
$forms = $wpdb->get_results("SELECT id,name FROM {$wpdb->prefix}its_forms ORDER BY id ASC;");
$url = admin_url('admin.php');
?>
    <div class="wrap">
    <h2>ROTS Taxonomy Search
    <a href="<?php echo $url ?>?page=i_tax_search_add_new" class="add-new-h2">Add New</a>
    </h2>
<?php
if(isset($forms) && count($forms)>0){
    ?>
    <form action="" method="post" id="manage_templates" class="form_manage_tmp" style="min-width:900px; width:80%; float:left;">
            <input type="submit" value="Delete" class="button action top_bttn" name="deleteButton"/>
            <table class="wp-list-table widefat fixed posts icltable" cellspacing="0">
                    <thead>
            		    <tr>
                            <th class="manage-column column-cb check-column"><input type="checkbox" onClick="selectAllC(this, '.checkbox_delete');" /></th>
            			    <th class="manage-column column">Name:</th>
                            <th class="manage-column column ">Shortcode</th>
                            <th class="manage-column column">Edit</th>
                        </tr>
                    </thead>
          <?php
          $current_user = get_current_user_id();
          $role = get_user_role($current_user);
          
                foreach($forms as $form){
                    ?>
                         <tr>
                            <td><?php 
                            if ($form->id >= 1 && $form->id <= 12 && $role == 'demo_user') {
                            	echo "&nbsp;";
                            } else {
                            ?><input type="checkbox" class="checkbox_delete" name="deleteItem[]" value="<?php echo $form->id;?>"/>
                            <?php } ?>
                            </td>
                            <td><?php echo $form->name;?></td>
                            <td><?php echo "[rots_taxonomy_search id={$form->id}]";?></td>
                            <td style="padding-left:10px;">
                            <?php if ($form->id >= 1 && $form->id <= 12 && $role == 'demo_user') {echo "&nbsp;"; } else {?>
                                <a href="<?php echo $url;?>?page=i_tax_search_add_new&form_id=<?php echo $form->id;?>" title="Edit this item">
                                    <i class="icon-edit-e"></i>
                                </a>
                            <?php } ?>    
                            </td>
                         </tr>
                    <?php
                }
          ?>
                    <tfoot>
            		    <tr>
                            <th class="manage-column column-cb check-column"><input type="checkbox" onClick="selectAllC(this, '.checkbox_delete');" /></th>
            			    <th class="manage-column column" style="min-width:170px;">Name:</th>
                            <th class="manage-column column" style="min-width: 200px;">Shortcode</th>
                            <th class="manage-column column" style="min-width:60px;">Edit</th>
                        </tr>
                    </tfoot>
            </table>
            <input type="submit" value="Delete" class="button action bottom_bttn" name="deleteButton"/>
            <input type="hidden" name="deleteID" value="" id="hidden_tmp_delete" />
        </form>
    </div>
    <?php
}
else{
    ?>
         <p>
            No Forms Available!
         </p>
    <?php
}
?>