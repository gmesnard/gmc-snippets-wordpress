<?php
/*
	Plugin Name: French Typo
	Plugin URI: https://bitbucket.org/master_shiva/wp-french-typo
	Description: Applique les règles de la typographie française.
	Author: Gilles Marchand
	Version: 1.0.3
*/

function french_typo($text)
{
	$french_typo_static_characters = array('(c)', '(r)');
	$french_typo_static_replacements = array('&#169;', '&#174;');

	$french_typo_dynamique_characters = array('#\s?([?!:;%])(?!\w|//)#u', '/(&#?[a-zA-Z0-9]+)&#160;;/');
	$french_typo_dynamique_replacements = array('&#160;$1', '$1;');

	$textarr = preg_split('/(<.*>|\[.*\])/Us', $text, -1, PREG_SPLIT_DELIM_CAPTURE);
	$stop = count($textarr);

	$text = '';

	for($i = 0; $i < $stop; $i++)
	{ 
		$curl = $textarr[$i];

		if(!empty($curl) && '<' != $curl[0] && '[' != $curl[0])
		{
			$curl = str_replace($french_typo_static_characters, $french_typo_static_replacements, $curl);
			$curl = preg_replace($french_typo_dynamique_characters, $french_typo_dynamique_replacements, $curl);
		}

		$text .= $curl;
	}

	return $text;
}

add_filter('the_title', 'french_typo');
add_filter('the_content', 'french_typo');
?>
