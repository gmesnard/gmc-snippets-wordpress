# French Typo #

Ce plugin pour [WordPress](http://fr.wordpress.org/) applique les règles de la typographie française aux articles que vous publiez sur vos pages.

## Règles utilisées ##

### Espaces insécables ###

Une [espace insécable](http://fr.wikipedia.org/wiki/Espace_ins%C3%A9cable) est ajoutée avant les caractères `;`, `:`, `!`, `?` & `%`.

### Caractères spéciaux ###

Les caractères `(c)` et `(r)` sont remplacés par `©` et `®`.

## Installation ##

1. [Téléchargez](https://bitbucket.org/master_shiva/wp-french-typo/downloads) la dernière version de French Typo.
2. Décompressez le fichier `french-typo.php` et ajoutez-le dans votre répertoire `/wp-content/plugins/`.
3. Activez le plugin dans le menu `Extensions` (`Plugins`) de WordPress.